FROM openjdk:11-jdk
WORKDIR usr/src
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} usr/src/backend.jar
EXPOSE 7000
ENTRYPOINT ["java","-jar","usr/src/backend.jar"]