package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.TestData.INVOICE;
import static com.suppleshop.exercise.app.order.dto.InvoiceDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CompanyNameValidatorTest {

    private final CompanyNameValidator testObj = new CompanyNameValidator();

    @Test
    void shouldReturnTrueWhenCompanyNameIsDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(INVOICE);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCompanyNameIsBlank() {
        // given
        InvoiceDto invoiceWithBlankCompanyName = withCompanyName("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(invoiceWithBlankCompanyName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Company name is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCompanyNameIsNull() {
        // given
        InvoiceDto invoiceWithNullCompanyName = withCompanyName(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(invoiceWithNullCompanyName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Company name is not defined!");
    }

    @Test
    void shouldReturnTrueWhenInvoiceIsNotRequired() {
        // when
        final boolean invoiceIsNotRequired = testObj.isFulfilledBy(null);

        // then
        assertThat(invoiceIsNotRequired).isTrue();
    }

    private InvoiceDto withCompanyName(final String companyName) {
        if (INVOICE.getCompanyName().equals(companyName)) {
            return INVOICE;
        } else {
            return builder()
                    .companyAddress(INVOICE.getCompanyAddress())
                    .companyName(companyName)
                    .nip(INVOICE.getNip())
                    .build();
        }
    }
}
