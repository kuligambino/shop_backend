package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.TestData.INVOICE;
import static com.suppleshop.exercise.app.order.dto.InvoiceDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NipValidatorTest {

    @InjectMocks
    private NipValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenNipMatchesRegex() {
        // given
        when(environment.getProperty("nip.regex")).thenReturn("^[0-9]{10}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(INVOICE);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format", "12345", "1234567890123"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNipDoesntMatchRegex(final String wrongNipFormat) {
        // given
        when(environment.getProperty("nip.regex")).thenReturn("^[0-9]{10}$");
        InvoiceDto invoiceWithWrongNipFormat = withNip(wrongNipFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(invoiceWithWrongNipFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Nip has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNipIsBlank() {
        // given
        InvoiceDto invoiceWithNullNip = withNip("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(invoiceWithNullNip))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Nip is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNipIsNull() {
        // given
        InvoiceDto invoiceWithNullNip = withNip(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(invoiceWithNullNip))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Nip is not defined!");
    }

    @Test
    void shouldReturnTrueWhenInvoiceIsNotRequired() {
        // when
        final boolean notRequiredInvoice = testObj.isFulfilledBy(null);

        // then
        assertThat(notRequiredInvoice).isTrue();
    }

    private InvoiceDto withNip(final String nip) {
        if (INVOICE.getNip().equals(nip)) {
            return INVOICE;
        } else {
            return builder()
                    .companyAddress(INVOICE.getCompanyAddress())
                    .companyName(INVOICE.getCompanyName())
                    .nip(nip)
                    .build();
        }
    }
}
