package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.TestData.INVOICE;
import static com.suppleshop.exercise.app.order.dto.InvoiceDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CompanyAddressValidatorTest {

    private final CompanyAddressValidator testObj = new CompanyAddressValidator();

    @Test
    void shouldReturnTrueWhenCompanyAddressIsDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(INVOICE);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCompanyAddressIsBlank() {
        // given
        InvoiceDto contactWithBlankCompanyAddress = withCompanyAddress("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithBlankCompanyAddress))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Company address is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCompanyAddressIsNull() {
        // given
        InvoiceDto contactWithBlankCompanyAddress = withCompanyAddress(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithBlankCompanyAddress))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Company address is not defined!");
    }

    @Test
    void shouldReturnTrueWhenInvoiceIsNotRequired() {
        // when
        final boolean notRequiredInvoice = testObj.isFulfilledBy(null);

        // then
        assertThat(notRequiredInvoice).isTrue();
    }

    private InvoiceDto withCompanyAddress(final String companyAddress) {
        if (INVOICE.getCompanyAddress().equals(companyAddress)) {
            return INVOICE;
        } else {
            return builder()
                    .companyAddress(companyAddress)
                    .companyName(INVOICE.getCompanyAddress())
                    .nip(INVOICE.getNip())
                    .build();
        }
    }

}