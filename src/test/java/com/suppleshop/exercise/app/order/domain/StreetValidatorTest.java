package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class StreetValidatorTest {

    private final StreetValidator testObj = new StreetValidator();

    @Test
    void shouldReturnTrueWhenStreetIsDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenStreetIsBlank() {
        // given
        AddressDto addressWithBlankStreet= withStreet("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankStreet))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Street is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenStreetIsNull() {
        // given
        AddressDto addressWithNoDefinedStreet = withStreet(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNoDefinedStreet))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Street is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withStreet(final String street) {
        if (ADDRESS.getStreet().equals(street)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(street)
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
