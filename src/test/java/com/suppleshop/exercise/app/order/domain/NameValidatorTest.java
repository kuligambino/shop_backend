package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class NameValidatorTest {

    private final NameValidator testObj = new NameValidator();

    @Test
    void shouldReturnTrueWhenNamesAreDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenFirstNameIsBlank() {
        // given
        AddressDto addressWithBlankFirstName = withFirstName("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankFirstName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("First name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenLastNameIsBlank() {
        // given
        AddressDto addressWithBlankLastName = withLastName("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankLastName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Last name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenFirstNameIsNull() {
        // given
        AddressDto addressWithNullFirstName = withFirstName(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullFirstName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("First name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenLastNameIsNull() {
        // given
        AddressDto addressWithNullLastName = withLastName(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullLastName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Last name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }


    private AddressDto withFirstName(final String firstName) {
        if (ADDRESS.getFirstName().equals(firstName)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(firstName)
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }

    private AddressDto withLastName(final String lastName) {
        if (ADDRESS.getLastName().equals(lastName)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(lastName)
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
