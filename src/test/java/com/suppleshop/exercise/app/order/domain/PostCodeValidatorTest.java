package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PostCodeValidatorTest {

    @InjectMocks
    private PostCodeValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenPostCodeMatchesRegex() {
        // given
        when(environment.getProperty("postCode.regex")).thenReturn("^[0-9]{2}\\-[0-9]{3}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format", "123-456", "123-45", "12356"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPostCodeDoesntMatchRegex(final String wrongPostCodeFormat) {
        // given
        when(environment.getProperty("postCode.regex")).thenReturn("^[0-9]{2}\\-[0-9]{3}$");
        AddressDto addressWithWrongPostCodeFormat = withPostCode(wrongPostCodeFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithWrongPostCodeFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Post code has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPostCodeIsBlank() {
        // given
        AddressDto addressWithBlankPostCode = withPostCode("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankPostCode))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Post code is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPostCodeIsNull() {
        // given
        AddressDto addressWithNullPostCode = withPostCode(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullPostCode))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Post code is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withPostCode(final String postCode) {
        if (ADDRESS.getPostCode().equals(postCode)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(postCode)
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
