package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailValidatorTest {

    @InjectMocks
    private EmailValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenEmailMatchesRegex() {
        // given
        when(environment.getProperty("email.regex")).thenReturn("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"eloelogmail.com", "elo@gmailcom", "@elo.com", "elo@gmail.c", "elo@    "})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailDoesntMatchRegex(final String wrongEmailFormat) {
        // given
        when(environment.getProperty("email.regex")).thenReturn("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");
        AddressDto addressWithWrongEmailFormat = withEmail(wrongEmailFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithWrongEmailFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Email has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailIsBlank() {
        // given
        AddressDto addressWithBlankEmail = withEmail("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankEmail))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Email is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailIsNull() {
        // given
        AddressDto addressWithNullEmail = withEmail("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullEmail))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Email is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withEmail(final String email) {
        if (ADDRESS.getEmail().equals(email)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(email)
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
