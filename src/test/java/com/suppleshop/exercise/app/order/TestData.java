package com.suppleshop.exercise.app.order;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.order.dto.InvoiceDto;

import static com.suppleshop.exercise.app.order.dto.CreditCardDto.builder;
import static java.lang.String.valueOf;
import static java.time.LocalDate.now;

public class TestData {

    private static final String FIRST_NAME = "John";
    private static final String LAST_NAME = "Doe";
    private static final String STREET = "Grunwaldzka 10/1";
    private static final String CARD_NUMBER = "1234123412341234";
    private static final String CVV = "123";
    private static final String EXPIRATION_MONTH = "Czerwiec";
    private static final String EXPIRATION_YEAR = valueOf(now().getYear() + 1);
    private static final String COMPANY_NAME = "KKWJ";
    private static final String NIP = "1234567890";
    private static final String EMAIL = "john@doe.com";
    private static final String PHONE_NUMBER = "123-123-123";
    private static final String CITY = "Warszawa";
    private static final String COUNTRY = "Polska";
    private static final String POST_CODE = "59-111";
    private static final boolean IS_MAIN = false;

    private TestData() {
    }

    public static final CreditCardDto CREDIT_CARD = builder()
            .ownerFirstName(FIRST_NAME)
            .ownerLastName(LAST_NAME)
            .cardNumber(CARD_NUMBER)
            .cvv(CVV)
            .expirationMonth(EXPIRATION_MONTH)
            .expirationYear(EXPIRATION_YEAR)
            .build();

    public static final InvoiceDto INVOICE = InvoiceDto.builder()
            .companyAddress(STREET)
            .companyName(COMPANY_NAME)
            .nip(NIP)
            .build();

    public static final AddressDto ADDRESS = AddressDto.builder()
            .firstName(FIRST_NAME)
            .lastName(LAST_NAME)
            .email(EMAIL)
            .phoneNumber(PHONE_NUMBER)
            .street(STREET)
            .city(CITY)
            .country(COUNTRY)
            .postCode(POST_CODE)
            .isMain(IS_MAIN)
            .build();
}
