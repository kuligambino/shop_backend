package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PhoneNumberValidatorTest {

    @InjectMocks
    private PhoneNumberValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenPhoneNumberMatchesRegex() {
        // given
        when(environment.getProperty("phoneNumber.regex")).thenReturn("^[0-9]{3}-[0-9]{3}-[0-9]{3}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format", "123-456", "123-123-1234", "123"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPhoneNumberDoesntMatchRegex(final String wrongPhoneNumberFormat) {
        // given
        when(environment.getProperty("phoneNumber.regex")).thenReturn("^[0-9]{3}-[0-9]{3}-[0-9]{3}$");
        AddressDto addressWithWrongPhoneNumberFormat = withPhoneNumber(wrongPhoneNumberFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithWrongPhoneNumberFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Phone number has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPhoneNumberIsBlank() {
        // given
        AddressDto addressWithBlankPhoneNumber = withPhoneNumber("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankPhoneNumber))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Phone number is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenPhoneNumberIsNull() {
        // given
        AddressDto addressWithNullPhoneNumber = withPhoneNumber(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullPhoneNumber))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Phone number is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withPhoneNumber(final String phoneNumber) {
        if (ADDRESS.getPhoneNumber().equals(phoneNumber)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(phoneNumber)
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}