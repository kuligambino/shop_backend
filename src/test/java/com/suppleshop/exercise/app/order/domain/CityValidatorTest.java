package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CityValidatorTest {

    private final CityValidator testObj = new CityValidator();

    @Test
    void shouldReturnTrueWhenCityIsDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCityIsBlank() {
        // given
        AddressDto addressWithBlankCvv = withCity("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankCvv))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("City is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCityIsNull() {
        // given
        AddressDto addressWithNullCvv = withCity(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullCvv))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("City is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withCity(final String city) {
        if (ADDRESS.getCity().equals(city)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(city)
                    .country(ADDRESS.getCountry())
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
