package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.CreditCardFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static com.suppleshop.exercise.app.helper.TesterHelper.convertJsonToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class CreditCardDtoControllerTest {

    private static final String CREDIT_CARD_PATH = "credit_card.controller/creditCard.json";
    private static final String EMPTY_CREDIT_CARD_PATH = "credit_card.controller/emptyCreditCard.json";
    private static final String ENDPOINT_URL = "/credit-card";

    private MockMvc mvc;
    @InjectMocks
    private CreditCardController testObj;
    @Mock
    private CreditCardFacade creditCardService;

    @BeforeEach
    void setUp() {
        mvc = standaloneSetup(testObj).build();
    }

    @Test
    void shouldReturnStatusOkWhenCreditCardIsValid() throws Exception {
        // given
        final String jsonAsString = convertJsonToString(CREDIT_CARD_PATH);
        when(creditCardService.isValidated(any())).thenReturn(true);

        // when
        final MockHttpServletResponse response = mvc.perform(post(ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(OK.value());
    }

    @Test
    void shouldNotValidateCreditCard() throws Exception {
        // given
        final String jsonAsString = convertJsonToString(EMPTY_CREDIT_CARD_PATH);

        // when
        final MockHttpServletResponse response = mvc.perform(post(ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(CONFLICT.value());
    }
}
