package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.TestData.ADDRESS;
import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CountryValidatorTest {

    private final CountryValidator testObj = new CountryValidator();

    @Test
    void shouldReturnTrueWhenCountryIsDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(ADDRESS);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCountryIsBlank() {
        // given
        AddressDto addressWithBlankCountry = withCountry("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithBlankCountry))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Country is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCountryIsNull() {
        // given
        AddressDto addressWithNullCountry = withCountry(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(addressWithNullCountry))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Country is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenAddressIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Address must be defined!");
    }

    private AddressDto withCountry(final String country) {
        if (ADDRESS.getCountry().equals(country)) {
            return ADDRESS;
        } else {
            return builder()
                    .firstName(ADDRESS.getFirstName())
                    .lastName(ADDRESS.getLastName())
                    .email(ADDRESS.getEmail())
                    .phoneNumber(ADDRESS.getPhoneNumber())
                    .street(ADDRESS.getStreet())
                    .city(ADDRESS.getCity())
                    .country(country)
                    .postCode(ADDRESS.getPostCode())
                    .isMain(ADDRESS.isMain())
                    .build();
        }
    }
}
