package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.dto.CreditCardDto.builder;
import static com.suppleshop.exercise.app.order.TestData.CREDIT_CARD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NumberValidatorTest {

    @InjectMocks
    private NumberValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenNumberMatchesRegex() {
        // given
        when(environment.getProperty("creditCard.regex")).thenReturn("^[0-9]{4}[0-9]{4}[0-9]{4}[0-9]{4}$");

        // when
        boolean isValid = testObj.isFulfilledBy(CREDIT_CARD);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format", "32452345", "11111111111111111"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNumberDoesntMatchRegex(final String wrongCardNumberFormat) {
        // given
        when(environment.getProperty("creditCard.regex")).thenReturn("^[0-9]{4}[0-9]{4}[0-9]{4}[0-9]{4}$");

        // when
        CreditCardDto creditCardWithWrongNumberFormat = withCardNumber(wrongCardNumberFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithWrongNumberFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card number has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNumberIsBlank() {
        // when
        CreditCardDto creditCardWithBlankNumber = withCardNumber("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankNumber))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card number is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenNumberIsNull() {
        // when
        CreditCardDto creditCardWithNotDefinedNumber = withCardNumber(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNotDefinedNumber))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card number is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenACreditCardIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card is not defined!");
    }

    private CreditCardDto withCardNumber(final String cardNumber) {
        if (CREDIT_CARD.getCardNumber().equals(cardNumber)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(CREDIT_CARD.getOwnerFirstName())
                    .ownerLastName(CREDIT_CARD.getOwnerLastName())
                    .cardNumber(cardNumber)
                    .cvv(CREDIT_CARD.getCvv())
                    .expirationMonth(CREDIT_CARD.getExpirationMonth())
                    .expirationYear(CREDIT_CARD.getExpirationYear())
                    .build();
        }
    }
}