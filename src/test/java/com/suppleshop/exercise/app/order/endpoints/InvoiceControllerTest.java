package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.InvoiceFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static com.suppleshop.exercise.app.helper.TesterHelper.convertJsonToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class InvoiceControllerTest {

    private static final String INVOICE_PATH = "invoice.controller/invoice.json";
    private static final String ENDPOINT_URL = "/invoice";
    private static final String ORDER_ID_NAME = "orderId";
    private static final String ORDER_ID_VALUE = "orderId";

    @InjectMocks
    private InvoiceController testObj;
    @Mock
    private InvoiceFacade invoiceService;
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = standaloneSetup(testObj).build();
    }

    @Test
    void shouldReturnStatusOkWhenInvoiceIsValid() throws Exception {
        // given
        when(invoiceService.isValidated(any())).thenReturn(true);
        final String jsonAsAString = convertJsonToString(INVOICE_PATH);

        // when
        final MockHttpServletResponse response = mvc.perform(post(ENDPOINT_URL)
                .content(jsonAsAString)
                .contentType(APPLICATION_JSON)
                .queryParam(ORDER_ID_NAME, ORDER_ID_VALUE))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(OK.value());
    }
}
