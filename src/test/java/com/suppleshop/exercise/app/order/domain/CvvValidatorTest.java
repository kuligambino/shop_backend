package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.order.TestData.CREDIT_CARD;
import static com.suppleshop.exercise.app.order.dto.CreditCardDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CvvValidatorTest {

    @InjectMocks
    private CvvValidator testObj;
    @Mock
    private Environment environment;

    @Test
    void shouldReturnTrueWhenCvvMatchesRegex() {
        // given
        when(environment.getProperty("cvv.regex")).thenReturn("^[0-9]{3}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(CREDIT_CARD);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format", "1", "1234"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCvvDoesntMatchRegex(final String wrongCvvFormat) {
        // given
        when(environment.getProperty("cvv.regex")).thenReturn("^[0-9]{3}$");
        CreditCardDto creditCardWithWrongCvvFormat = withCvv(wrongCvvFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithWrongCvvFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Cvv has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCvvIsBlank() {
        // given
        CreditCardDto creditCardWithBlankCvv = withCvv("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankCvv))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Cvv is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenCvvIsNull() {
        // given
        CreditCardDto creditCardWithNullCvv = withCvv(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNullCvv))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Cvv is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenACreditCardIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card is not defined!");
    }

    private CreditCardDto withCvv(final String cvv) {
        if (CREDIT_CARD.getCvv().equals(cvv)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(CREDIT_CARD.getOwnerFirstName())
                    .ownerLastName(CREDIT_CARD.getOwnerLastName())
                    .cardNumber(CREDIT_CARD.getCardNumber())
                    .cvv(cvv)
                    .expirationMonth(CREDIT_CARD.getExpirationMonth())
                    .expirationYear(CREDIT_CARD.getExpirationYear())
                    .build();
        }
    }
}
