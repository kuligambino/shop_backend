package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.order.TestData.CREDIT_CARD;
import static com.suppleshop.exercise.app.order.dto.CreditCardDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class NameValidator1Test {

    private final NameValidator1 testObj = new NameValidator1();

    @Test
    void shouldReturnTrueWhenOwnersNamesAreDefined() {
        // when
        final boolean isValid = testObj.isFulfilledBy(CREDIT_CARD);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenOwnerFirstNameIsBlank() {
        // given
        CreditCardDto creditCardWithBlankFirstName = withOwnerFirstName("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankFirstName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage( "Owner first name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenOwnerLastNameIsBlank() {
        // given
        CreditCardDto creditCardWithBlankLastName = withOwnerLastName("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankLastName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Owner last name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenOwnerFirstNameIsNull() {
        // given
        CreditCardDto creditCardWithNullFirstName = withOwnerFirstName(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNullFirstName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Owner first name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenOwnerLastNameIsNull() {
        // given
        CreditCardDto creditCardWithNullLastName = withOwnerLastName(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNullLastName))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Owner last name must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenACreditCardIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card is not defined!");
    }

    private CreditCardDto withOwnerFirstName(final String ownerFirstName) {
        if (CREDIT_CARD.getOwnerFirstName().equals(ownerFirstName)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(ownerFirstName)
                    .ownerLastName(CREDIT_CARD.getOwnerLastName())
                    .cardNumber(CREDIT_CARD.getCardNumber())
                    .cvv(CREDIT_CARD.getCvv())
                    .expirationMonth(CREDIT_CARD.getExpirationMonth())
                    .expirationYear(CREDIT_CARD.getExpirationYear())
                    .build();
        }
    }

    private CreditCardDto withOwnerLastName(final String ownerLastName) {
        if (CREDIT_CARD.getOwnerLastName().equals(ownerLastName)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(CREDIT_CARD.getOwnerFirstName())
                    .ownerLastName(ownerLastName)
                    .cardNumber(CREDIT_CARD.getCardNumber())
                    .cvv(CREDIT_CARD.getCvv())
                    .expirationMonth(CREDIT_CARD.getExpirationMonth())
                    .expirationYear(CREDIT_CARD.getExpirationYear())
                    .build();
        }
    }
}
