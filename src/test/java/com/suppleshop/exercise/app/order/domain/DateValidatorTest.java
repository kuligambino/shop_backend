package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.suppleshop.exercise.app.order.dto.CreditCardDto.builder;
import static com.suppleshop.exercise.app.order.TestData.CREDIT_CARD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class DateValidatorTest {

    private final DateValidator testObj = new DateValidator();

    @DisplayName("Json with data looks like: {expMonth: zerwiec, expYear: 2022} - this is correct format. Date should be after now().")
    @Test
    void shouldReturnTrueWhenDateIsCorrectlyDefined() {
        // when
        boolean isValid = testObj.isFulfilledBy(CREDIT_CARD);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"5", "05", "13", "-1", "0"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenMonthIsInWrongFormat(final String wrongMonthFormat) {
        // given
        CreditCardDto creditCardWithWrongExpMonthFormat = withExpirationMonth(wrongMonthFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithWrongExpMonthFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Date has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenMonthIsBlank() {
        // given
        CreditCardDto creditCardWithBlankExpMonthFormat = withExpirationMonth("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankExpMonthFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Expiration month must be defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenMonthIsNull() {
        // given
        CreditCardDto creditCardWithNullExpMonthFormat = withExpirationMonth(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNullExpMonthFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Expiration month must be defined!");
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong format"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenYearIsInWrongFormat(final String wrongYearFormat) {
        // given
        CreditCardDto creditCardWithWrongExpYearFormat =withExpirationYear(wrongYearFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithWrongExpYearFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Date has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenYearIsBlank() {
        // given
        CreditCardDto creditCardWithBlankExpYearFormat = withExpirationYear("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithBlankExpYearFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Expiration year is not defined");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenYearIsNull() {
        // given
        CreditCardDto creditCardWithNullExpYearFormat = withExpirationYear(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithNullExpYearFormat))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Expiration year is not defined");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenDateIsInPast(){
        // given
        CreditCardDto creditCardWithExpYearInPast = withExpirationYear("2000");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(creditCardWithExpYearInPast))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Card is expired");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenACreditCardIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Credit card is not defined!");
    }

    private CreditCardDto withExpirationMonth(final String expirationMonth) {
        if (CREDIT_CARD.getExpirationMonth().equals(expirationMonth)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(CREDIT_CARD.getOwnerFirstName())
                    .ownerLastName(CREDIT_CARD.getOwnerLastName())
                    .cardNumber(CREDIT_CARD.getCardNumber())
                    .cvv(CREDIT_CARD.getCvv())
                    .expirationMonth(expirationMonth)
                    .expirationYear(CREDIT_CARD.getExpirationYear())
                    .build();
        }
    }

    private CreditCardDto withExpirationYear(final String expirationYear) {
        if (CREDIT_CARD.getExpirationYear().equals(expirationYear)) {
            return CREDIT_CARD;
        } else {
            return builder()
                    .ownerFirstName(CREDIT_CARD.getOwnerFirstName())
                    .ownerLastName(CREDIT_CARD.getOwnerLastName())
                    .cardNumber(CREDIT_CARD.getCardNumber())
                    .cvv(CREDIT_CARD.getCvv())
                    .expirationMonth(CREDIT_CARD.getExpirationMonth())
                    .expirationYear(expirationYear)
                    .build();
        }
    }
}