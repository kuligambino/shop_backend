package com.suppleshop.exercise.app.store_data.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.suppleshop.exercise.app.store_data.domain.StoreDataFacade;
import com.suppleshop.exercise.app.store_data.dto.StoreDataDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@ExtendWith(MockitoExtension.class)
class StoreDataDtoControllerTest {

    @InjectMocks
    StoreDataController testObj;
    @Mock
    StoreDataFacade storeDataFacade;
    private MockMvc mvc;
    private StoreDataDto storeData;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        storeData = new StoreDataDto();
        objectMapper = new ObjectMapper();
        mvc = standaloneSetup(testObj).build();
    }

    @Test
    void shouldReturnStoreData() throws Exception {
        when(storeDataFacade.initializeStoreData()).thenReturn(storeData);
        mvc.perform(get("/store-data"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(storeData)));
    }
}