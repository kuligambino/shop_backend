package com.suppleshop.exercise.app.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParser;
import com.suppleshop.exercise.app.shared.exception.ApiError;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static java.lang.ClassLoader.getSystemResource;

public class TesterHelper {

    public static String convertJsonToString(String path) throws FileNotFoundException {
        final FileReader jsonFile = new FileReader(getSystemResource(path).getFile());
        final Object json = new JsonParser().parse(jsonFile);
        return json.toString();
    }

    public static ApiError[] convertJsonToApiErrors(String json) throws JsonProcessingException {
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ApiError[].class);
    }
}
