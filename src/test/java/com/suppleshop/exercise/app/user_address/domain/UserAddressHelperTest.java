package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.order.domain.Address;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.user_address.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;

class UserAddressHelperTest {

    @Test
    void shouldUpdateAddressUser() {
        // when
        Address updatedUserAddress = UserAddressHelper.updateUserAddress(ADDRESS, USER_ADDRESS);

        // then
        assertThat(updatedUserAddress.getStreet()).isEqualTo(ADDRESS_USER_STREET);
        assertThat(updatedUserAddress.getCity()).isEqualTo(ADDRESS_USER_CITY);
        assertThat(updatedUserAddress.getPostCode()).isEqualTo(ADDRESS_USER_POST_CODE);
        assertThat(updatedUserAddress.getCountry()).isEqualTo(ADDRESS_USER_COUNTRY);
    }
}