package com.suppleshop.exercise.app.user_address;

import com.suppleshop.exercise.app.order.domain.Address;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import com.suppleshop.exercise.app.register.domain.User;

import static com.suppleshop.exercise.app.user_address.dto.AddressDto.addressUserDtoBuilder;
import static java.util.Set.of;

public class TestData {

    public static final Long ID = 1L;
    public static final String STREET = "Złota 1";
    public static final String CITY = "Warszawa";
    public static final String POST_CODE = "11-111";
    public static final String COUNTRY = "Polska";
    public static final String ADDRESS_USER_STREET = "Grunwaldzka 1";
    public static final String ADDRESS_USER_CITY = "Wroclaw";
    public static final String ADDRESS_USER_POST_CODE = "12-123";
    public static final String ADDRESS_USER_COUNTRY = "Polska";
    public final static String FIRST_NAME = "Krystian";
    public final static String LAST_NAME = "Kulig";
    public final static String EMAIL = "k@k";
    public final static String PHONE_NUMBER = "123456789";

    public static final AddressDto USER_ADDRESS = addressUserDtoBuilder()
            .id(ID)
            .street(ADDRESS_USER_STREET)
            .city(ADDRESS_USER_CITY)
            .postCode(ADDRESS_USER_POST_CODE)
            .country(ADDRESS_USER_COUNTRY)
            .build();

    public static final Address ADDRESS = createAddress(ID, STREET, CITY, POST_CODE, COUNTRY);
    public static final Address ADDRESS1 = createAddress(2L, "Ponaska 1", "Kraków", "13-121", "Polska");

    public static final User USER = createUser();

    private static Address createAddress(Long id, String street, String city, String postCode, String country) {
        final Address address = new Address();
        address.setId(id);
        address.setStreet(street);
        address.setCity(city);
        address.setPostCode(postCode);
        address.setCountry(country);
        return address;
    }

    private static User createUser() {
        final User user = new User();
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setPhoneNumber(PHONE_NUMBER);
        user.setAddresses(of(ADDRESS));
        return user;
    }
}
