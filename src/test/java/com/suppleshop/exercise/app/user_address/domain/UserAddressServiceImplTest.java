package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.suppleshop.exercise.app.user_address.TestData.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserAddressServiceImplTest {

    @InjectMocks
    private UserAddressService testObj;
    @Mock
    private HttpRequestService httpRequestService;

    @Test
    void shouldGetAddressesWhenUserExists() {
        // given
        when(httpRequestService.getUser(any())).thenReturn(USER);

        // when
        Set<AddressDto> userAddresses = testObj.getAddresses(any());

        // then
        assertThat(userAddresses.size()).isEqualTo(1);
    }

    @Test
    void shouldNotGetAddressesWhenUserNotExists() {
        // given
        when(httpRequestService.getUser(any())).thenThrow(new UsernameNotExistsException());

        // when
        Set<AddressDto> userAddresses = testObj.getAddresses(any());

        // then
        assertThat(userAddresses.isEmpty()).isTrue();
    }
}
