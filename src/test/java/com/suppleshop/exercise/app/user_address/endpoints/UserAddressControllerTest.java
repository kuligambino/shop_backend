package com.suppleshop.exercise.app.user_address.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.suppleshop.exercise.app.user_address.domain.UserAddressFacade;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Set;

import static com.suppleshop.exercise.app.user_address.TestData.USER_ADDRESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class UserAddressControllerTest {

    private static final String GET_ADDRESSES_PATH = "/user-addresses";
    private static final String ADD_ADDRESS_PATH = "/add-user-address";

    private MockMvc mockMvc;
    @InjectMocks
    private UserAddressController testObj;
    @Mock
    private UserAddressFacade userAddressFacade;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        mockMvc = standaloneSetup(testObj).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void shouldReturnStatus200WithAddressesInBody() throws Exception {
        // given
        when(userAddressFacade.getAddresses(any())).thenReturn(Set.of(USER_ADDRESS));

        // when
        final MockHttpServletResponse response = mockMvc.perform(get(GET_ADDRESSES_PATH)
                .accept(APPLICATION_JSON))
                .andReturn()
                .getResponse();

        final Set<AddressDto> addresses = convertContentToObject(response.getContentAsString());

        // then
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(addresses.size()).isEqualTo(1);
    }

    @Test
    void shouldReturnStatus201WhenAddNewAddress() throws Exception {
        // given
        when(userAddressFacade.addAddressToUser(eq(USER_ADDRESS), any())).thenReturn(USER_ADDRESS);
        String content = convertObjectToString(USER_ADDRESS);

        // when
        final MockHttpServletResponse response = mockMvc.perform(post(ADD_ADDRESS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .content(content))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(CREATED.value());
    }

    private String convertObjectToString(AddressDto addressUserDto) throws JsonProcessingException {
        return objectMapper.writeValueAsString(addressUserDto);
    }

    private Set<AddressDto> convertContentToObject(String json) throws IOException {
        return objectMapper.readValue(json, new TypeReference<>() {
        });
    }
}