package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.order.domain.Address;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static com.suppleshop.exercise.app.user_address.TestData.*;
import static com.suppleshop.exercise.app.user_address.domain.UserAddressMapper.*;
import static org.assertj.core.api.Assertions.assertThat;

class UserAddressMapperTest {

    @Test
    void shouldMapAddressToAddressUserDto() {
        // when
        final AddressDto result = addressToAddressUserDto(ADDRESS1);

        // then
        assertThat(result.getId()).isEqualTo(2L);
        assertThat(result.getStreet()).isEqualTo("Ponaska 1");
        assertThat(result.getCity()).isEqualTo("Kraków");
        assertThat(result.getPostCode()).isEqualTo("13-121");
        assertThat(result.getCountry()).isEqualTo("Polska");
    }

    @Test
    void shouldMapAddressesToAddressesUserDto() {
        // when
        final Set<AddressDto> result = toAddressUserCollectionDto(Set.of(ADDRESS));

        // then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.contains(addressToAddressUserDto(ADDRESS))).isTrue();
    }

    @Test
    void shouldFillInformationAboutUserInAddress() {
        // when
        final Address result = addressUserToEntity(USER_ADDRESS, USER);

        // then
        assertThat(result.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(result.getLastName()).isEqualTo(LAST_NAME);
        assertThat(result.getEmail()).isEqualTo(EMAIL);
        assertThat(result.getPhoneNumber()).isEqualTo(PHONE_NUMBER);
    }
}
