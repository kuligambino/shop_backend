package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.ContactException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.suppleshop.exercise.app.contact.domain.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactServiceTest {

    @InjectMocks
    private ContactService testObj;
    @Mock
    private ContactRepository contactRepository;
    @Mock
    private ContactValidator contactValidator;

    @Test
    void shouldAddContactWhenIsValid() {
        // given
        when(contactValidator.isFulfilledBy(any())).thenReturn(true);
        when(contactRepository.save(any())).thenReturn(ContactMapper.toEntity(CONTACT_DTO));
        // when
        final ContactDto result = testObj.addContact(CONTACT_DTO);

        // then
        assertThat(result.getEmail()).isEqualTo(EMAIL_DTO);
        assertThat(result.getDetails()).isEqualTo(DETAILS_DTO);
        assertThat(result.getGrade()).isEqualTo(GRADE_DTO);
        assertThat(result.isRecaptcha()).isTrue();
    }

    @Test
    void shouldThrowContactExceptionWhenContactIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.addContact(null))
                .isInstanceOf(ContactException.class)
                .hasMessage("There are problems with form!");
    }

}
