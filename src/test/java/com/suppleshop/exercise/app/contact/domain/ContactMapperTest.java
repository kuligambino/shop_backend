package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.contact.domain.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;

class ContactMapperTest {

    @Test
    void shouldMapEntityToDto(){
        // when
        ContactDto result = ContactMapper.toDto(CONTACT);

        // then
        assertThat(result.getEmail()).isEqualTo(EMAIL);
        assertThat(result.getDetails()).isEqualTo(DETAILS);
        assertThat(result.getGrade()).isEqualTo(GRADE);
        assertThat(result.isRecaptcha()).isTrue();
    }

    @Test
    void shouldMapDtoToEntity(){
        // when
        Contact result = ContactMapper.toEntity(CONTACT_DTO);

        // then
        assertThat(result.getEmail()).isEqualTo(EMAIL_DTO);
        assertThat(result.getDetails()).isEqualTo(DETAILS_DTO);
        assertThat(result.getGrade()).isEqualTo(GRADE_DTO);
    }

}
