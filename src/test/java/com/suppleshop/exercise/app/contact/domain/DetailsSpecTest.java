package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.contact.domain.DetailsSpec.MIN_DETAILS_LENGTH;
import static com.suppleshop.exercise.app.contact.domain.DetailsSpec.MIN_DETAILS_MESSAGE;
import static com.suppleshop.exercise.app.contact.domain.TestData.CONTACT_DTO;
import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class DetailsSpecTest {

    private final DetailsSpec testObj = new DetailsSpec();

    @Test
    void shouldReturnTrueWhenDetailsAreValid() {
        // when
        final boolean isValid = testObj.isFulfilledBy(CONTACT_DTO);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenContactIsNull() {
        // when
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Contact is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenDetailsAreNull() {
        // given
        ContactDto contactWithNullDetails = withDetails(null);

        // when
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithNullDetails))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Details are not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenDetailsAreBlank() {
        // given
        ContactDto contactWithBlankDetails = withDetails("");

        // when
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithBlankDetails))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Details are not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenDetailsAreTooShort() {
        // given
        ContactDto contactWithDetailsShorterThan50Chars = withDetails("I should have at least " + MIN_DETAILS_LENGTH + " chars!");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithDetailsShorterThan50Chars))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage(MIN_DETAILS_MESSAGE);
    }

    private ContactDto withDetails(final String details) {
        if (CONTACT_DTO.getDetails().equals(details)) {
            return CONTACT_DTO;
        } else {
            return builder()
                    .email(CONTACT_DTO.getEmail())
                    .details(details)
                    .grade(CONTACT_DTO.getGrade())
                    .recaptcha(CONTACT_DTO.isRecaptcha())
                    .build();
        }
    }

}
