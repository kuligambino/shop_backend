package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.contact.domain.TestData.CONTACT_DTO;
import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailSpecTest {

    private static final String CONTACT_IS_NOT_DEFINED_MESSAGE = "Email is not defined!";

    @InjectMocks
    private EmailSpec testObj;
    @Mock
    private Environment env;

    @Test
    void shouldReturnTrueWhenEmailIsValidated() {
        // given
        when(env.getProperty("email.regex")).thenReturn("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");

        // when
        final boolean isValid = testObj.isFulfilledBy(CONTACT_DTO);

        // then
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"nomonkey.pl", "no@domain"})
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailDoesntMatchRegex(final String wrongEmailFormat) {
        // given
        when(env.getProperty("email.regex")).thenReturn("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");
        ContactDto contactWithWrongEmail = withEmail(wrongEmailFormat);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithWrongEmail))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Email has wrong format!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenContactIsNull() {
        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Contact is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailIsBlank() {
        // given
        ContactDto contactWithBlankEmail = withEmail("");

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithBlankEmail))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage(CONTACT_IS_NOT_DEFINED_MESSAGE);
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenEmailIsNull() {
        // given
        ContactDto contactWithNullEmail = withEmail(null);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithNullEmail))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage(CONTACT_IS_NOT_DEFINED_MESSAGE);
    }

    private ContactDto withEmail(final String email) {
        if (CONTACT_DTO.getEmail().equals(email)) {
            return CONTACT_DTO;
        } else {
            return builder()
                    .email(email)
                    .details(CONTACT_DTO.getDetails())
                    .grade(CONTACT_DTO.getGrade())
                    .recaptcha(CONTACT_DTO.isRecaptcha())
                    .build();
        }
    }

}
