package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.contact.domain.TestData.CONTACT;
import static com.suppleshop.exercise.app.contact.domain.TestData.CONTACT_DTO;
import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RecaptchaSpecTest {

    private final RecaptchaSpec testObj = new RecaptchaSpec();

    @Test
    void shouldReturnTrueWhenRecaptchaIsAccepted() {
        // when
        final boolean isValid = testObj.isFulfilledBy(CONTACT_DTO);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenContactIsNull() {
        // when
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Contact is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenRecaptchaIsNotApproved() {
        // given
        ContactDto contactWithNotAcceptedRecaptcha = withRecaptcha(false);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithNotAcceptedRecaptcha))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Recaptcha must be accepted!");
    }

    private ContactDto withRecaptcha(final boolean recaptcha) {
        if (CONTACT_DTO.isRecaptcha() == recaptcha) {
            return CONTACT_DTO;
        } else {
            return builder()
                    .email(CONTACT_DTO.getEmail())
                    .details(CONTACT_DTO.getDetails())
                    .grade(CONTACT.getGrade())
                    .recaptcha(recaptcha)
                    .build();
        }
    }

}
