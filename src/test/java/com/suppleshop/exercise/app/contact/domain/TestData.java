package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;

import java.time.LocalDateTime;

import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;

class TestData {

    public final static String EMAIL_DTO = "dto@dto.pl";
    public final static String DETAILS_DTO = "a".repeat(52);
    public final static int GRADE_DTO = 5;
    public final static String EMAIL = "entity@entity.pl";
    public final static String DETAILS = "entity";
    public final static int GRADE = 10;
    public final static boolean RECAPTCHA = true;
    public static final LocalDateTime SEND_DATA = now().truncatedTo(SECONDS);

    public static ContactDto CONTACT_DTO = builder()
            .email(EMAIL_DTO)
            .details(DETAILS_DTO)
            .grade(GRADE_DTO)
            .recaptcha(RECAPTCHA)
            .build();

    public static Contact CONTACT = createContact(EMAIL, DETAILS, GRADE, SEND_DATA);

    private static Contact createContact(final String email, final String details,
                                         final int grade, final LocalDateTime sendTime) {
        final Contact contact = new Contact();
        contact.setEmail(email);
        contact.setDetails(details);
        contact.setGrade(grade);
        contact.setSendDate(sendTime);
        return contact;
    }

}
