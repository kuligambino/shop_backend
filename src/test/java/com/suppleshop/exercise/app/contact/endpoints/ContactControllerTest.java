package com.suppleshop.exercise.app.contact.endpoints;

import com.suppleshop.exercise.app.contact.domain.ContactFacade;
import com.suppleshop.exercise.app.shared.exception.ApiExceptionHandler;
import com.suppleshop.exercise.app.shared.exception.ContactException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static com.suppleshop.exercise.app.helper.TesterHelper.convertJsonToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class ContactControllerTest {

    private static final String EMPTY_CONTACT_FORM_PATH = "contact/controller/emptyContactForm.json";
    private static final String CORRECT_CONTACT_FORM_PATH = "contact/controller/contactForm.json";
    private static final String ENDPOINT_URL = "/contact";

    private MockMvc mvc;

    @InjectMocks
    private ContactController testObj;
    @Mock
    private ContactFacade contactFacade;

    @BeforeEach
    void setUp() {
        mvc = standaloneSetup(testObj).
                setControllerAdvice(new ApiExceptionHandler())
                .build();
    }

    @Test
    void shouldReturnStatusCreatedWhenContactIsValid() throws Exception {
        // given
        final String jsonAsString = convertJsonToString(CORRECT_CONTACT_FORM_PATH);

        // when then
        final MockHttpServletResponse response = mvc.perform(post(ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        assertThat(response.getStatus()).isEqualTo(CREATED.value());
    }

    @Test
    void shouldReturnStatusBadRequestWhenContactIsNotValid() throws Exception {
        // given
        when(contactFacade.addContact(any())).thenThrow(new ContactException());
        String jsonAsString = convertJsonToString(EMPTY_CONTACT_FORM_PATH);

        // when
        final MockHttpServletResponse response = mvc.perform(post(ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
    }

}
