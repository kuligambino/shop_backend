package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.junit.jupiter.api.Test;

import static com.suppleshop.exercise.app.contact.domain.TestData.CONTACT_DTO;
import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class GradeSpecTest {

    private final GradeSpec testObj = new GradeSpec();

    @Test
    void shouldReturnTrueWhenGradeIsValid() {
        // when
        final boolean isValid = testObj.isFulfilledBy(CONTACT_DTO);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenContactIsNull() {
        // when
        assertThatThrownBy(() -> testObj.isFulfilledBy(null))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Contact is not defined!");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenGradeIsTooBig() {
        // given
        ContactDto contactWithGradeBiggerThan10 = withGrade(11);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithGradeBiggerThan10))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Grade can't be higher than 10");
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenGradeIsTooSmall() {
        // given
        ContactDto contactWithGradeLowerThan1 = withGrade(0);

        // when then
        assertThatThrownBy(() -> testObj.isFulfilledBy(contactWithGradeLowerThan1))
                .isInstanceOf(UnsatisfiedSpecificationException.class)
                .hasMessage("Grade can't be lower than 1");
    }

    private ContactDto withGrade(final int grade) {
        if (CONTACT_DTO.getGrade() == (grade)) {
            return CONTACT_DTO;
        } else {
            return builder()
                    .email(CONTACT_DTO.getEmail())
                    .details(CONTACT_DTO.getDetails())
                    .grade(grade)
                    .recaptcha(CONTACT_DTO.isRecaptcha())
                    .build();
        }
    }

}
