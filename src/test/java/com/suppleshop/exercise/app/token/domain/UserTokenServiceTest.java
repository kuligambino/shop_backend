package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.shared.exception.TokenNotExistException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.suppleshop.exercise.app.token.domain.TestData.*;
import static com.suppleshop.exercise.app.token.domain.TokenType.PASSWORD_RESET;
import static com.suppleshop.exercise.app.token.domain.TokenType.VERIFICATION;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserTokenServiceTest {

    @InjectMocks
    private UserTokenService testObj;
    @Mock
    private UserTokenRepository userTokenRepository;
    @Mock
    private TokenRepository tokenRepository;

    @Test
    void shouldGenerateVerificationToken() {
        // given
        when(userTokenRepository.save(any())).thenReturn(USER_TOKEN);

        // when
        final UserToken result = testObj.generateTokenByType(USER, VERIFICATION);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getTokenType().getType()).isEqualTo(VERIFICATION);
        assertThat(result.getUser()).isEqualTo(USER);
        assertThat(result.getUser().getTokens().size()).isPositive();
    }

    @Test
    void shouldGeneratePasswordResetToken() {
        // given
        when(userTokenRepository.save(any())).thenReturn(EXPIRED_USER_TOKEN);

        // when
        final UserToken result = testObj.generateTokenByType(USER, PASSWORD_RESET);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getTokenType().getType()).isEqualTo(PASSWORD_RESET);
        assertThat(result.getUser()).isEqualTo(USER);
        assertThat(result.getUser().getTokens().size()).isPositive();
    }

    @Test
    void shouldFindByToken() {
        // given
        when(userTokenRepository.findByToken(TOKEN_UUID)).thenReturn(Optional.of(USER_TOKEN));

        // when
        final UserToken result = testObj.findByToken(TOKEN_UUID);

        // then
        assertNotNull(result);
        assertThat(result.getToken()).isEqualTo(TOKEN_UUID);
        assertThat(result.getExpiryDate()).isEqualTo(EXPIRY_DATE);
        assertThat(result.isValid()).isEqualTo(IS_VALID);
        assertThat(result.getTokenType().getType()).isEqualTo(VERIFICATION);
    }

    @Test
    void shouldThrowTokenNotExistsException() {
        // given
        when(userTokenRepository.findByToken(TOKEN_UUID)).thenReturn(empty());

        // when then
        assertThatThrownBy(() -> testObj.findByToken(TOKEN_UUID))
                .isInstanceOf(TokenNotExistException.class)
                .hasMessage(TOKEN_NOT_EXISTS_EXCEPTION_MESSAGE);
    }

    @Test
    void shouldReturnTrueWhenTokenIsValid() {
        // when
        final boolean isValid = testObj.checkIfValid(USER_TOKEN);

        // then
        assertThat(isValid).isTrue();
    }

    @Test
    void shouldThrowUnsatisfiedSpecificationExceptionWhenTokenIsNotValid() {
        // when
        final boolean isValid = testObj.checkIfValid(EXPIRED_USER_TOKEN);

        // then
        assertThat(isValid).isFalse();
    }
}
