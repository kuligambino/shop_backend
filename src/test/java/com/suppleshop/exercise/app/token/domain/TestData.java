package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.token.domain.TokenType;
import com.suppleshop.exercise.app.token.domain.Token;
import com.suppleshop.exercise.app.token.domain.UserToken;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.suppleshop.exercise.app.token.domain.TokenType.PASSWORD_RESET;
import static com.suppleshop.exercise.app.token.domain.TokenType.VERIFICATION;
import static java.time.LocalDateTime.now;

class TestData {

    private TestData() {
    }

    private final static LocalDateTime EXPIRED = now().minusDays(2);
    private final static String FIRST_NAME = "Krystian";
    private final static String LAST_NAME = "Kulig";
    private final static String EMAIL = "k@k";
    private final static String PASSWORD = "Krystian!";
    private final static String PHONE_NUMBER = "123456789";
    private final static LocalDate BIRTH_DATE = LocalDate.of(2000, 10, 10);

    public final static String TOKEN_NOT_EXISTS_EXCEPTION_MESSAGE = "Taki token nie istnieję!";
    public final static String TOKEN_UUID = "abcd-abcd-abcd-abcd";
    public final static boolean IS_VALID = true;
    public final static boolean IS_VALID_1 = false;
    public final static LocalDateTime EXPIRY_DATE = now().plusDays(2);

    public static final User USER = createUser();

    public static final UserToken USER_TOKEN = createUserToken(TOKEN_UUID, EXPIRY_DATE, IS_VALID, VERIFICATION, USER);
    public static final UserToken EXPIRED_USER_TOKEN = createUserToken(TOKEN_UUID, EXPIRED, IS_VALID_1, PASSWORD_RESET, USER);

    private static User createUser() {
        final User user = new User();
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);
        user.setPhoneNumber(PHONE_NUMBER);
        user.setBirthDate(BIRTH_DATE);
        user.setEnabled(false);
        user.setAnonymous(true);
        return user;
    }

    private static UserToken createUserToken(final String tokenUUID, final LocalDateTime expiryDate,
                                             final boolean isValid, final TokenType tokenType, final User user) {
        final UserToken userToken = new UserToken();
        userToken.setToken(tokenUUID);
        userToken.setExpiryDate(expiryDate);
        userToken.setValid(isValid);
        userToken.setTokenType(createToken(tokenType));
        userToken.setUser(user);
        return userToken;
    }

    private static Token createToken(final TokenType tokenType) {
        final Token token = new Token();
        token.setType(tokenType);
        return token;
    }
}
