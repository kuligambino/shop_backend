package com.suppleshop.exercise.app.reset_password.endpoints;

import com.suppleshop.exercise.app.reset_password.domain.ResetPasswordFacade;
import com.suppleshop.exercise.app.shared.exception.ApiExceptionHandler;
import com.suppleshop.exercise.app.shared.exception.BusinessException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static com.suppleshop.exercise.app.helper.TesterHelper.convertJsonToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class ResetPasswordControllerTest {

    private static final String CORRECT_REQUEST_RESET_PASSWORD_PATH = "reset_password.endpoints/validRequestPasswordForm.json";
    private static final String INCORRECT_REQUEST_RESET_PASSWORD_PATH = "reset_password.endpoints/invalidRequestPasswordForm.json";

    @InjectMocks
    private ResetPasswordController testObj;
    @Mock
    private ResetPasswordFacade resetPasswordFacade;
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = standaloneSetup(testObj).
                setControllerAdvice(new ApiExceptionHandler())
                .build();
    }

    @Test
    void shouldReturn200WhenRequestResetPasswordFormIsValid() throws Exception {
        // given
        final String jsonAsString = convertJsonToString(CORRECT_REQUEST_RESET_PASSWORD_PATH);

        // when then
        final MockHttpServletResponse response = mvc.perform(post("/password/forget")
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        assertThat(response.getStatus()).isEqualTo(OK.value());
    }

    @Test
    void shouldReturn409WhenRequestResetPasswordFormIsNotValid() throws Exception {
        // given
        final String jsonAsString = convertJsonToString(INCORRECT_REQUEST_RESET_PASSWORD_PATH);
        when(resetPasswordFacade.sendEmailWithResetToken(any())).thenThrow(new BusinessException());

        // when then
        final MockHttpServletResponse response = mvc.perform(post("/password/forget")
                .contentType(APPLICATION_JSON)
                .content(jsonAsString))
                .andReturn()
                .getResponse();

        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
    }

}
