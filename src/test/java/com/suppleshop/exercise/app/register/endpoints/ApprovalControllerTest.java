package com.suppleshop.exercise.app.register.endpoints;

import com.suppleshop.exercise.app.register.domain.ApprovalFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class ApprovalControllerTest {

    private static final String ENDPOINT_URL = "/approvals";

    private MockMvc mvc;
    @InjectMocks
    private ApprovalController testObj;
    @Mock
    private ApprovalFacade approvalFacade;

    @BeforeEach
    void setUp() {
        mvc = standaloneSetup(testObj).build();
    }

    @Test
    void shouldReturnStatusOkAndGetApprovals() throws Exception {
        // when
        final MockHttpServletResponse response = mvc.perform(get(ENDPOINT_URL))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(OK.value());
    }
}
