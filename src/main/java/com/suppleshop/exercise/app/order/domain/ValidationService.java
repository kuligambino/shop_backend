package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import org.springframework.stereotype.Service;

@Service
class ValidationService<T> {

    private final OrderSpecificationFactory orderSpecificationFactory;

    ValidationService(OrderSpecificationFactory orderSpecificationFactory) {
        this.orderSpecificationFactory = orderSpecificationFactory;
    }

    public boolean isValidated(final T t) {
        if (t instanceof CreditCardDto) {
            return orderSpecificationFactory.defineCreditCardSpecification().isFulfilledBy((CreditCardDto) t);
        } else if (t instanceof InvoiceDto) {
            return orderSpecificationFactory.defineInvoiceSpecification().isFulfilledBy((InvoiceDto) t);
        } else if (t instanceof AddressDto) {
            return orderSpecificationFactory.defineAddressSpecification().isFulfilledBy((AddressDto) t);
        }
        return false;
    }
}
