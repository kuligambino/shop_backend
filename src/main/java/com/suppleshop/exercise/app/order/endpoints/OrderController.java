package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.OrderService;
import com.suppleshop.exercise.app.order.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

import static com.suppleshop.exercise.app.order.domain.OrderStatus.DONE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/order")
class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public ResponseEntity<OrderDto> getOrder(final HttpServletRequest request) {
        final OrderDto order = orderService.getCurrentOrder(request);
        return status(OK).body(order);
    }

    @PostMapping("/add")
    public ResponseEntity<OrderDto> addProductToOrder(
            final HttpServletRequest request,
            final @RequestParam Long productId,
            final @RequestParam(required = false, defaultValue = "1") int quantity) {
        final OrderDto order = orderService.addProductToOrder(productId, quantity, request);
        return status(OK).body(order);
    }

    @PatchMapping("/update")
    public ResponseEntity<OrderDto> updateOrder(final @RequestBody OrderDto orderDTO) {
        final OrderDto updatedOrder = orderService.updateOrder(orderDTO);
        return status(ACCEPTED).body(updatedOrder);
    }

    @PostMapping("place-order")
    public ResponseEntity<Void> placeOrder(final @RequestBody OrderDto order) {
        final boolean isPlaced = orderService.placeOrder(order);
        return isPlaced ? ok().build() : status(CONFLICT).build();
    }

    @GetMapping("status/{orderId}")
    public ResponseEntity<Boolean> isOrderDone(final @PathVariable String orderId) {
        return status(OK).body(orderService.getOrderStatus(orderId) == DONE);
    }

    @GetMapping("products/quantity")
    public ResponseEntity<Long> getCurrentQuantity(final HttpServletRequest request) {
        return status(OK).body(orderService.getCurrentQuantity(request));
    }
}
