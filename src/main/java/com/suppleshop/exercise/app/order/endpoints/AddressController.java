package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.AddressService;
import com.suppleshop.exercise.app.order.dto.AddressDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

@RestController
class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping("/addresses")
    public ResponseEntity<Set<AddressDto>> getUserAddresses(final HttpServletRequest request) {
        final Set<AddressDto> addresses = addressService.getAddresses(request);
        return status(OK).body(addresses);
    }

    @PostMapping("/add-address")
    public ResponseEntity<Set<AddressDto>> addAddress(final @RequestBody AddressDto address, final HttpServletRequest request) {
        Set<AddressDto> updatedAddresses = null;
        if (addressService.isValidated(address)) {
            updatedAddresses = addressService.addAddress(address, request);
        }
        return status(CREATED).body(updatedAddresses);
    }

    @PatchMapping("/edit-address")
    public ResponseEntity<AddressDto> editAddress(final @RequestBody AddressDto address) {
        if (addressService.isValidated(address)) {
            final AddressDto updatedAddress = addressService.editAddress(address);
            return status(OK).body(updatedAddress);
        } else {
            return status(NO_CONTENT).build();
        }
    }

    @DeleteMapping("/delete-address")
    public ResponseEntity<Void> deleteAddress(final @RequestParam Long addressId, final HttpServletRequest request) {
        if (addressId != null) {
            addressService.deleteAddress(addressId, request);
            return status(NO_CONTENT).build();
        } else {
            return status(BAD_REQUEST).build();
        }
    }

    @PatchMapping("/address")
    public ResponseEntity<AddressDto> setAddressToOrder(final @RequestBody AddressDto address, final @RequestParam String orderId) {
        AddressDto updatedAddress = null;
        if (addressService.isValidated(address)) {
            updatedAddress = addressService.setAddressToOrder(address, orderId);
        }
        return status(OK).body(updatedAddress);
    }
}
