package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.InvoiceFacade;
import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/invoice")
class InvoiceController {

    private final InvoiceFacade invoiceFacade;

    InvoiceController(InvoiceFacade invoiceFacade) {
        this.invoiceFacade = invoiceFacade;
    }

    @PostMapping
    public ResponseEntity<Boolean> validateInvoice(final @RequestBody InvoiceDto invoice) {
        if (invoice != null && invoiceFacade.isValidated(invoice)) {
            return status(OK).body(true);
        } else {
            return status(NO_CONTENT).body(false);
        }
    }
}
