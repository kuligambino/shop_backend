package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.security.domain.EncryptionFacade;
import com.suppleshop.exercise.app.shared.exception.OrderException;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Set;

import static com.suppleshop.exercise.app.order.domain.AddressHelper.updateAddress;
import static com.suppleshop.exercise.app.order.domain.AddressMapper.*;
import static java.util.Collections.emptySet;
import static java.util.Optional.empty;
import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
public class AddressService {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final EncryptionFacade encryptionFacade;
    private final OrderSpecificationFactory orderSpecificationFactory;
    private final HttpRequestService httpRequestService;

    public AddressService(OrderRepository orderRepository, UserRepository userRepository,
                          AddressRepository addressRepository, EncryptionFacade encryptionFacade,
                          OrderSpecificationFactory orderSpecificationFactory, HttpRequestService httpRequestService) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.encryptionFacade = encryptionFacade;
        this.orderSpecificationFactory = orderSpecificationFactory;
        this.httpRequestService = httpRequestService;
    }

    public boolean isValidated(final AddressDto address) {
        final AddressValidator specification = orderSpecificationFactory.defineAddressSpecification();
        return specification.isFulfilledBy(address);
    }

    public AddressDto setAddressToOrder(final AddressDto addressDto, final String orderId) {
        final Order order = getOrder(orderId);
        Optional<Address> addressOptional = empty();
        if (addressDto.getId() != null) {
            addressOptional = addressRepository.findById(addressDto.getId());
        }
        Address address = addressOptional
                .map(a -> updateAddress(a, addressDto))
                .orElseGet(() -> toEntity(addressDto));
        order.setShippingAddress(address);
        final Address updatedAddress = addressRepository.save(address);
        orderRepository.save(order);
        return toDto(updatedAddress);
    }

    public Set<AddressDto> getAddresses(final HttpServletRequest request) {
        try {
            final User user = httpRequestService.getUser(request);
            return toCollectionDto(user.getAddresses());
        } catch (UsernameNotExistsException ex) {
            return emptySet();
        }
    }

    public Set<AddressDto> addAddress(final AddressDto addressDto, final HttpServletRequest request) {
        final User user = httpRequestService.getUser(request);
        final Address address = toEntity(addressDto);
        addNewAddressToUser(address, user);
        return getAddresses(request);
    }

    public AddressDto editAddress(final AddressDto updatedAddress) {
        final Optional<Address> addressToUpdate = addressRepository.findById(updatedAddress.getId());
        return addressToUpdate.flatMap(address -> addressToUpdate)
                .map(addressFromDb -> updateAddress(addressFromDb, updatedAddress))
                .map(addressRepository::save)
                .map(AddressMapper::toDto)
                .orElseThrow();
    }

    public void deleteAddress(final Long addressId, final HttpServletRequest request) {
        final User user = httpRequestService.getUser(request);
        user.removeAddress(addressId);
        userRepository.save(user);
    }

    private Order getOrder(final String orderId) {
        if (!isEmpty(orderId)) {
            final Long decodedId = encryptionFacade.decodeId(orderId);
            return orderRepository.findById(decodedId).orElseThrow(OrderException::new);
        } else {
            throw new OrderException();
        }
    }

    private void addNewAddressToUser(final Address address, final User user) {
        final Set<Address> addresses = user.getAddresses();
        if (isFalse(addresses.contains(address))) {
            addresses.add(address);
            addressRepository.save(address);
            userRepository.save(user);
        }
    }
}
