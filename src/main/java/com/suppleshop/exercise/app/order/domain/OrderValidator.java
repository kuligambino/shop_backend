package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.OrderDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import org.springframework.stereotype.Component;

@Component
class OrderValidator implements BaseSpec<OrderDto> {

    private final OrderSpecificationFactory orderSpecificationFactory;

    OrderValidator(OrderSpecificationFactory orderSpecificationFactory) {
        this.orderSpecificationFactory = orderSpecificationFactory;
    }

    @Override
    public boolean isFulfilledBy(final OrderDto order) {
        if (order != null) {
            final AddressValidator addressValidator = orderSpecificationFactory.defineAddressSpecification();
            final CreditCardValidator creditCardValidator = orderSpecificationFactory.defineCreditCardSpecification();
            final InvoiceValidator invoiceValidator = orderSpecificationFactory.defineInvoiceSpecification();
            final boolean addressIsValidated = addressValidator.isFulfilledBy(order.getAddress());
            final boolean creditCardIsValidated = creditCardValidator.isFulfilledBy(order.getCreditCard());
            final boolean invoiceIsValidated = invoiceValidator.isFulfilledBy(order.getInvoice());
            final boolean regulationIsAccepted = order.isRegulationAccepted();
            return order.getInvoice() != null ?
                    addressIsValidated && creditCardIsValidated && regulationIsAccepted && invoiceIsValidated :
                    addressIsValidated && creditCardIsValidated && regulationIsAccepted;
        } else {
            return false;
        }
    }
}
