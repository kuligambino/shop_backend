package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;

class InvoiceMapper {

    private InvoiceMapper() {
    }

    public static Invoice toEntity(final InvoiceDto invoiceDto) {
        if(invoiceDto != null) {
            final Invoice invoice = new Invoice();
            invoice.setId(invoiceDto.getId());
            invoice.setCompanyName(invoiceDto.getCompanyName());
            invoice.setNip(invoiceDto.getNip());
            invoice.setCompanyAddress(invoiceDto.getCompanyAddress());
            return invoice;
        }
        return null;
    }
}
