package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.shared.exception.OrderException;
import com.suppleshop.exercise.app.products.domain.Product;
import com.suppleshop.exercise.app.register.domain.User;

import java.math.BigDecimal;
import java.util.List;

import static com.suppleshop.exercise.app.order.domain.OrderStatus.OPEN;
import static java.math.BigDecimal.ZERO;
import static java.time.LocalDate.now;

public class OrderHelper {

    private OrderHelper() {
    }

    public static Order getUserOpenedOrder(final User user) {
        return user.getOrders().stream()
                .filter(o -> o.getStatus() == OPEN)
                .findAny().orElseThrow(OrderException::new);
    }

    public static boolean hasUserAnyOpenedOrders(final User user) {
        return user != null && user
                .getOrders().stream()
                .anyMatch(o -> o.getStatus() == OPEN);
    }

    public static Order createNewOrderForUser(final User user) {
        final Order newOrder = new Order();
        if (user != null) {
            newOrder.setUser(user);
        }
        newOrder.setCreationDate(now());
        newOrder.setStatus(OPEN);
        return newOrder;
    }

    public static BigDecimal countOrderAmount(final List<Product> products) {
        return products.stream()
                .map(Product::getPrice)
                .reduce(ZERO, BigDecimal::add);
    }
}
