package com.suppleshop.exercise.app.order.dto;

public class AddressDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String street;
    private String city;
    private String postCode;
    private String country;
    private boolean isMain;

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getCountry() {
        return country;
    }

    public boolean isMain() {
        return isMain;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setMain(boolean main) {
        isMain = main;
    }

    public static AddressDtoBuilder builder() {
        return new AddressDtoBuilder();
    }

    public static final class AddressDtoBuilder {
        private Long id;
        private String firstName;
        private String lastName;
        private String email;
        private String phoneNumber;
        private String street;
        private String city;
        private String postCode;
        private String country;
        private boolean isMain;

        private AddressDtoBuilder() {
        }

        public AddressDtoBuilder id(final Long id) {
            this.id = id;
            return this;
        }

        public AddressDtoBuilder firstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AddressDtoBuilder lastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public AddressDtoBuilder email(final String email) {
            this.email = email;
            return this;
        }

        public AddressDtoBuilder phoneNumber(final String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public AddressDtoBuilder street(final String street) {
            this.street = street;
            return this;
        }

        public AddressDtoBuilder city(final String city) {
            this.city = city;
            return this;
        }

        public AddressDtoBuilder postCode(final String postCode) {
            this.postCode = postCode;
            return this;
        }

        public AddressDtoBuilder country(final String country) {
            this.country = country;
            return this;
        }

        public AddressDtoBuilder isMain(final boolean isMain) {
            this.isMain = isMain;
            return this;
        }

        public AddressDto build() {
            final AddressDto addressDto = new AddressDto();
            addressDto.phoneNumber = this.phoneNumber;
            addressDto.email = this.email;
            addressDto.id = this.id;
            addressDto.street = this.street;
            addressDto.lastName = this.lastName;
            addressDto.isMain = this.isMain;
            addressDto.postCode = this.postCode;
            addressDto.country = this.country;
            addressDto.city = this.city;
            addressDto.firstName = this.firstName;
            return addressDto;
        }
    }
}
