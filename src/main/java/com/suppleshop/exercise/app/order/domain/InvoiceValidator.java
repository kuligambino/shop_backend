package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

class InvoiceValidator implements BaseSpec<InvoiceDto> {

    private final List<BaseSpec<InvoiceDto>> specifications;

    public InvoiceValidator(List<BaseSpec<InvoiceDto>> specifications) {
        this.specifications = specifications;
    }

    @Override
    public boolean isFulfilledBy(InvoiceDto invoice) {
        return specifications.stream().allMatch(spec -> spec.isFulfilledBy(invoice));
    }
}

class NipValidator implements BaseSpec<InvoiceDto> {

    private final Environment env;

    public NipValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final InvoiceDto invoice) {
        final boolean invoiceIsNotDefined = invoice == null;

        if (invoiceIsNotDefined) {
            return true;
        }

        final String nip = invoice.getNip();
        final boolean nipIsNotDefined = isBlank(nip);

        if (nipIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Nip is not defined!");
        }

        final String nipRegex = requireNonNull(env.getProperty("nip.regex"));
        boolean nipHasWrongFormat = !nip.matches(nipRegex);

        if (nipHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Nip has wrong format!");
        }

        return true;

    }
}

class CompanyNameValidator implements BaseSpec<InvoiceDto> {

    @Override
    public boolean isFulfilledBy(final InvoiceDto invoice) {
        final boolean invoiceIsNotDefined = invoice == null;

        if (invoiceIsNotDefined) {
            return true;
        }

        final String companyName = invoice.getCompanyName();
        final boolean companyNameIsNotDefined = isBlank(companyName);

        if (companyNameIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Company name is not defined!");
        }

        return true;
    }
}

class CompanyAddressValidator implements BaseSpec<InvoiceDto> {

    @Override
    public boolean isFulfilledBy(final InvoiceDto invoice) {
        final boolean invoiceIsNotDefined = invoice == null;

        if (invoiceIsNotDefined) {
            return true;
        }

        final String companyAddress = invoice.getCompanyAddress();
        final boolean companyAddressIsNotDefined = isBlank(companyAddress);

        if (companyAddressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Company address is not defined!");
        }

        return true;
    }
}
