package com.suppleshop.exercise.app.order.domain;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static java.util.List.of;

@Component
class OrderSpecificationFactory {

    private final Environment env;

    OrderSpecificationFactory(Environment env) {
        this.env = env;
    }

    public InvoiceValidator defineInvoiceSpecification() {
        return new InvoiceValidator(of(
                new NipValidator(env),
                new CompanyNameValidator(),
                new CompanyAddressValidator()
        ));
    }

    public CreditCardValidator defineCreditCardSpecification() {
        return new CreditCardValidator(of(
                new NameValidator1(),
                new NumberValidator(env),
                new CvvValidator(env),
                new DateValidator())
        );
    }

    public AddressValidator defineAddressSpecification() {
        return new AddressValidator(of(
                new NameValidator(),
                new EmailValidator(env),
                new PhoneNumberValidator(env),
                new StreetValidator(),
                new CityValidator(),
                new PostCodeValidator(env),
                new CountryValidator()
        ));
    }
}
