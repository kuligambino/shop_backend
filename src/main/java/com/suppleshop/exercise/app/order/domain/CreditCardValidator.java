package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;

import java.time.YearMonth;
import java.util.Arrays;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.time.YearMonth.now;
import static java.time.YearMonth.of;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

class CreditCardValidator implements BaseSpec<CreditCardDto> {

    private final List<BaseSpec<CreditCardDto>> creditCardSpecs;

    public CreditCardValidator(List<BaseSpec<CreditCardDto>> creditCardSpecs) {
        this.creditCardSpecs = creditCardSpecs;
    }

    @Override
    public boolean isFulfilledBy(CreditCardDto creditCard) {
        return creditCardSpecs.stream().allMatch(spec -> spec.isFulfilledBy(creditCard));
    }
}

class NameValidator1 implements BaseSpec<CreditCardDto> {

    @Override
    public boolean isFulfilledBy(final CreditCardDto creditCard) {
        final boolean creditCardIsNotDefined = creditCard == null;

        if (creditCardIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Credit card is not defined!");
        }

        final String ownerFirstName = creditCard.getOwnerFirstName();
        final boolean ownerFirstNameIsNotDefined = isBlank(ownerFirstName);

        if (ownerFirstNameIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Owner first name must be defined!");
        }
        final String ownerLastName = creditCard.getOwnerLastName();
        final boolean ownerLastNameIsNotDefined = isBlank(ownerLastName);

        if (ownerLastNameIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Owner last name must be defined!");
        }
        return true;
    }
}

class NumberValidator implements BaseSpec<CreditCardDto> {

    private final Environment env;

    public NumberValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final CreditCardDto creditCard) {
        final boolean creditCardIsNotDefined = creditCard == null;

        if (creditCardIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Credit card is not defined!");
        }

        final String cardNumber = creditCard.getCardNumber();
        final boolean creditCardNumberIsNotDefined = isBlank(cardNumber);

        if (creditCardNumberIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Credit card number is not defined!");
        }

        final String creditCardNumberRegex = requireNonNull(env.getProperty("creditCard.regex"));
        final boolean creditCardNumberHasWrongFormat = !cardNumber.matches(creditCardNumberRegex);

        if (creditCardNumberHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Credit card number has wrong format!");
        }

        return true;
    }
}

class DateValidator implements BaseSpec<CreditCardDto> {

    @Override
    public boolean isFulfilledBy(final CreditCardDto creditCard) {
        final boolean creditCardIsNotDefined = creditCard == null;

        if (creditCardIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Credit card is not defined!");
        }

        final String expirationMonth = creditCard.getExpirationMonth();
        boolean monthIsNotDefined = isBlank(expirationMonth);

        if (monthIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Expiration month must be defined!");
        }

        final String expirationYear = creditCard.getExpirationYear();
        boolean yearIsNotDefined = isBlank(expirationYear);

        if (yearIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Expiration year is not defined");
        }

        final YearMonth expirationDate = formatStringDateIntoYearMonth(expirationMonth, expirationYear);
        boolean cardIsExpired = expirationDate.isBefore(now());

        if (cardIsExpired) {
            throw new UnsatisfiedSpecificationException("Card is expired");
        }

        return true;
    }

    private YearMonth formatStringDateIntoYearMonth(final String expirationMonth, final String expirationYear) {
        final int month;
        final int year;
        try {
            month = Month.getMonthNumber(expirationMonth);
            year = parseInt(expirationYear);
        } catch (IllegalArgumentException ex) {
            throw new UnsatisfiedSpecificationException("Date has wrong format!");
        }
        return of(year, month);
    }
}

class CvvValidator implements BaseSpec<CreditCardDto> {

    private final Environment env;

    public CvvValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final CreditCardDto creditCard) {
        final boolean creditCardIsNotDefined = creditCard == null;

        if (creditCardIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Credit card is not defined!");
        }

        final String cvv = creditCard.getCvv();
        final boolean cvvIsNotDefined = isBlank(cvv);

        if (cvvIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Cvv is not defined!");
        }

        final String cvvRegex = requireNonNull(env.getProperty("cvv.regex"));
        final boolean cvvHasWrongFormat = !cvv.matches(cvvRegex);

        if (cvvHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Cvv has wrong format!");
        }

        return true;
    }
}

enum Month {
    JANUARY(1, "Styczeń"),
    FEBRUARY(2, "Luty"),
    MARCH(3, "Marzec"),
    APRIL(4, "Kwiecień"),
    MAY(5, "Maj"),
    JUNE(6, "Czerwiec"),
    JULY(7, "Lipiec"),
    AUGUST(8, "Sierpień"),
    SEPTEMBER(9, "Wrzesień"),
    OCTOBER(10, "Październik"),
    NOVEMBER(11, "Listopad"),
    DECEMBER(12, "Grudzień");

    private final int position;
    private final String name;

    Month(int position, String name) {
        this.position = position;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    static int getMonthNumber(String monthName) {
        return Arrays.stream(Month.values())
                .filter(month -> month.getName().equals(monthName))
                .map(Month::getPosition)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
