package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

class AddressValidator implements BaseSpec<AddressDto> {

    private final List<BaseSpec<AddressDto>> addressSpecs;

    public AddressValidator(List<BaseSpec<AddressDto>> addressSpecs) {
        this.addressSpecs = addressSpecs;
    }

    @Override
    public boolean isFulfilledBy(AddressDto address) {
        return addressSpecs.stream().allMatch(spec -> spec.isFulfilledBy(address));
    }
}

class NameValidator implements BaseSpec<AddressDto> {

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String firstName = address.getFirstName();
        final boolean firstNameIsNotDefined = isBlank(firstName);

        if (firstNameIsNotDefined) {
            throw new UnsatisfiedSpecificationException("First name must be defined!");
        }

        final String lastName = address.getLastName();
        final boolean lastNameIsNotDefined = isBlank(lastName);

        if (lastNameIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Last name must be defined!");
        }

        return true;
    }
}

class EmailValidator implements BaseSpec<AddressDto> {

    private final Environment env;

    public EmailValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String email = address.getEmail();
        final boolean emailIsNotDefined = isBlank(email);

        if (emailIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Email is not defined!");
        }

        final String emailRegex = requireNonNull(env.getProperty("email.regex"));
        final boolean emailHasWrongFormat = !email.matches(emailRegex);

        if (emailHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Email has wrong format!");
        }
        return true;
    }
}

class PhoneNumberValidator implements BaseSpec<AddressDto> {

    private final Environment env;

    public PhoneNumberValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String phoneNumber = address.getPhoneNumber();
        final boolean phoneNumberIsNotDefined = isBlank(phoneNumber);

        if (phoneNumberIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Phone number is not defined!");
        }

        final String phoneNumberRegex = requireNonNull(env.getProperty("phoneNumber.regex"));
        final boolean phoneNumberHasWrongFormat = !phoneNumber.matches(phoneNumberRegex);

        if (phoneNumberHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Phone number has wrong format!");
        }

        return true;
    }
}

class CityValidator implements BaseSpec<AddressDto> {

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String city = address.getCity();
        final boolean cityIsNotDefined = isBlank(city);

        if (cityIsNotDefined) {
            throw new UnsatisfiedSpecificationException("City is not defined!");
        }

        return true;
    }
}

class StreetValidator implements BaseSpec<AddressDto> {

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String street = address.getStreet();
        final boolean cityIsNotDefined = isBlank(street);

        if (cityIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Street is not defined!");
        }

        return true;
    }
}

class PostCodeValidator implements BaseSpec<AddressDto> {

    private final Environment env;

    public PostCodeValidator(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String postCode = address.getPostCode();
        final boolean postCodeIsNotDefined = isBlank(postCode);

        if (postCodeIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Post code is not defined!");
        }

        final String postCodeRegex = requireNonNull(env.getProperty("postCode.regex"));
        final boolean postCodeHasWrongFormat = !postCode.matches(postCodeRegex);

        if (postCodeHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Post code has wrong format!");
        }

        return true;
    }
}

class CountryValidator implements BaseSpec<AddressDto> {

    @Override
    public boolean isFulfilledBy(final AddressDto address) {
        final boolean addressIsNotDefined = address == null;

        if (addressIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Address must be defined!");
        }

        final String country = address.getCountry();
        final boolean countryIsNotDefined = isBlank(country);

        if (countryIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Country is not defined!");
        }

        return true;
    }
}
