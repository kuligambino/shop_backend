package com.suppleshop.exercise.app.order.endpoints;

import com.suppleshop.exercise.app.order.domain.CreditCardFacade;
import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/credit-card")
class CreditCardController {

    private final CreditCardFacade creditCardFacade;

    CreditCardController(CreditCardFacade creditCardFacade) {
        this.creditCardFacade = creditCardFacade;
    }

    @PostMapping
    public ResponseEntity<Boolean> validateCreditCard(final @RequestBody CreditCardDto creditCardDto) {
        final boolean isValidated = creditCardFacade.isValidated(creditCardDto);
        return isValidated ? ok(true) : status(CONFLICT).body(false);
    }
}
