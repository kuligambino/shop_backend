package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;

class CreditCardMapper {

    private CreditCardMapper() {
    }

    public static CreditCard toEntity(final CreditCardDto creditCardDto) {
        final CreditCard creditCard = new CreditCard();
        creditCard.setId(creditCardDto.getId());
        creditCard.setOwnerFirstName(creditCardDto.getOwnerFirstName());
        creditCard.setOwnerLastName(creditCardDto.getOwnerLastName());
        creditCard.setCardNumber(creditCardDto.getCardNumber());
        creditCard.setExpirationYear(creditCardDto.getExpirationYear());
        creditCard.setExpirationMonth(creditCardDto.getExpirationMonth());
        creditCard.setCvv(creditCardDto.getCvv());
        return creditCard;
    }
}
