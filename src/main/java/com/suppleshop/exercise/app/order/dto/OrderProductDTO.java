package com.suppleshop.exercise.app.order.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class OrderProductDTO {

    private Long id;
    private String name;
    private int weight;
    private BigDecimal price;
    private String taste;
    private String categoryName;
    private String img;
    private int quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderProductDTO that = (OrderProductDTO) o;
        return weight == that.weight &&
                quantity == that.quantity &&
                Objects.equals(id, that.id) &&
                name.equals(that.name) &&
                price.equals(that.price) &&
                taste.equals(that.taste) &&
                categoryName.equals(that.categoryName) &&
                img.equals(that.img);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, weight, price, taste, categoryName, img, quantity);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        private String name;
        private int weight;
        private BigDecimal price;
        private String taste;
        private String categoryName;
        private String img;
        private int quantity;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder weight(int weight) {
            this.weight = weight;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder taste(String taste) {
            this.taste = taste;
            return this;
        }

        public Builder categoryName(String categoryName) {
            this.categoryName = categoryName;
            return this;
        }

        public Builder img(String img) {
            this.img = img;
            return this;
        }

        public Builder quantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public OrderProductDTO build() {
            OrderProductDTO orderProductDTO = new OrderProductDTO();
            orderProductDTO.id = this.id;
            orderProductDTO.name = this.name;
            orderProductDTO.weight = this.weight;
            orderProductDTO.price = this.price;
            orderProductDTO.taste = this.taste;
            orderProductDTO.categoryName = this.categoryName;
            orderProductDTO.img = this.img;
            orderProductDTO.quantity = this.quantity;
            return orderProductDTO;
        }
    }
}


