package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.CreditCardDto;
import org.springframework.stereotype.Component;

@Component
public class CreditCardFacade {

    private final ValidationService<CreditCardDto> validationService;

    CreditCardFacade(ValidationService<CreditCardDto> validationService) {
        this.validationService = validationService;
    }

    public boolean isValidated(CreditCardDto creditCard) {
        return validationService.isValidated(creditCard);
    }
}
