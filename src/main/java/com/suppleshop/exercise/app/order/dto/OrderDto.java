package com.suppleshop.exercise.app.order.dto;

import java.math.BigDecimal;
import java.util.List;

public class OrderDto {

    private String id;
    private List<OrderProductDTO> orderProducts;
    private BigDecimal amount;
    private AddressDto address;
    private CreditCardDto creditCard;
    private boolean regulationAccepted;
    private InvoiceDto invoice;

    public String getId() {
        return id;
    }

    public List<OrderProductDTO> getOrderProducts() {
        return orderProducts;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public AddressDto getAddress() {
        return address;
    }

    public CreditCardDto getCreditCard() {
        return creditCard;
    }

    public boolean isRegulationAccepted() {
        return regulationAccepted;
    }

    public InvoiceDto getInvoice() {
        return invoice;
    }

    public static OrderDtoBuilder builder() {
        return new OrderDtoBuilder();
    }

    public static final class OrderDtoBuilder {
        private String id;
        private List<OrderProductDTO> orderProducts;
        private BigDecimal amount;
        private AddressDto address;
        private CreditCardDto creditCard;
        private boolean regulationAccepted;
        private InvoiceDto invoice;

        public OrderDtoBuilder id(final String id) {
            this.id = id;
            return this;
        }

        public OrderDtoBuilder orderProducts(final List<OrderProductDTO> orderProducts) {
            this.orderProducts = orderProducts;
            return this;
        }

        public OrderDtoBuilder amount(final BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public OrderDtoBuilder address(final AddressDto address) {
            this.address = address;
            return this;
        }

        public OrderDtoBuilder creditCard(final CreditCardDto creditCard) {
            this.creditCard = creditCard;
            return this;
        }

        public OrderDtoBuilder regulationAccepted(final boolean regulationAccepted) {
            this.regulationAccepted = regulationAccepted;
            return this;
        }

        public OrderDtoBuilder invoice(final InvoiceDto invoice) {
            this.invoice = invoice;
            return this;
        }

        public OrderDto build() {
            final OrderDto order = new OrderDto();
            order.id = id;
            order.orderProducts = orderProducts;
            order.amount = amount;
            order.address = address;
            order.creditCard = creditCard;
            order.regulationAccepted = regulationAccepted;
            order.invoice = invoice;
            return order;
        }
    }
}
