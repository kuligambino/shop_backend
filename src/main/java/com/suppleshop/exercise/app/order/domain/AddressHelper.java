package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;

class AddressHelper {

    private AddressHelper() {
    }

    public static Address updateAddress(final Address address, final AddressDto addressDto) {
        address.setFirstName(addressDto.getFirstName());
        address.setLastName(addressDto.getLastName());
        address.setEmail(addressDto.getEmail());
        address.setPhoneNumber(addressDto.getPhoneNumber());
        address.setStreet(addressDto.getStreet());
        address.setCity(addressDto.getCity());
        address.setPostCode(addressDto.getPostCode());
        address.setCountry(addressDto.getCountry());
        return address;
    }
}
