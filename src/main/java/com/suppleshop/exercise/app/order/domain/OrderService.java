package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.email.domain.EmailFacade;
import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.order.dto.OrderDto;
import com.suppleshop.exercise.app.order.dto.OrderProductDTO;
import com.suppleshop.exercise.app.products.domain.Product;
import com.suppleshop.exercise.app.products.domain.ProductMapper;
import com.suppleshop.exercise.app.products.domain.ProductRepository;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.security.domain.EncryptionFacade;
import com.suppleshop.exercise.app.shared.exception.OrderException;
import com.suppleshop.exercise.app.shared.exception.ProductNotExistsException;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.suppleshop.exercise.app.order.domain.InvoiceMapper.toEntity;
import static com.suppleshop.exercise.app.order.domain.OrderHelper.*;
import static com.suppleshop.exercise.app.order.domain.OrderStatus.DONE;
import static java.time.LocalDate.now;
import static java.util.Collections.nCopies;
import static java.util.Map.of;

@Service
public class OrderService {

    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final ProductMapper productMapper;
    private final EncryptionFacade encryptionFacade;
    private final HttpRequestService httpRequestService;
    private final OrderValidator orderValidator;
    private final EmailFacade emailFacade;

    public OrderService(ProductRepository productRepository,
                        OrderRepository orderRepository,
                        OrderMapper orderMapper,
                        ProductMapper productMapper,
                        EncryptionFacade encryptionFacade,
                        HttpRequestService httpRequestService, OrderValidator orderValidator,
                        EmailFacade emailFacade) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productMapper = productMapper;
        this.encryptionFacade = encryptionFacade;
        this.httpRequestService = httpRequestService;
        this.orderValidator = orderValidator;
        this.emailFacade = emailFacade;
    }

    public OrderDto addProductToOrder(final Long productId, final int quantity, final HttpServletRequest request) {
        final User user = getUser(request);
        final Order order = getOrder(request, user);
        final Product product = getProduct(productId);
        final List<Product> newProducts = nCopies(quantity, product);
        order.getProducts().addAll(newProducts);
        final BigDecimal amount = countOrderAmount(order.getProducts());
        order.setAmount(amount);
        final Order savedOrder = orderRepository.save(order);
        return orderMapper.convertToOrderDTO(savedOrder);
    }

    public OrderDto getCurrentOrder(final HttpServletRequest request) {
        final User user = getUser(request);
        final Order order = getOrder(request, user);
        return orderMapper.convertToOrderDTO(order);
    }

    public OrderDto updateOrder(final OrderDto orderDTO) {
        final Long decodedOrderId = encryptionFacade.decodeId(orderDTO.getId());
        final Optional<Order> order = orderRepository.findById(decodedOrderId);
        if (order.isPresent()) {
            final List<Product> updatedProducts = productMapper.convertToProducts(orderDTO.getOrderProducts());
            final Order orderToUpdate = order.get();
            orderToUpdate.setProducts(updatedProducts);
            orderToUpdate.setAmount(countOrderAmount(updatedProducts));
            orderRepository.save(orderToUpdate);
            return orderMapper.convertToOrderDTO(orderToUpdate);
        } else {
            throw new OrderException();
        }
    }

    public boolean placeOrder(final OrderDto orderDTO) {
        final boolean isValidated = orderValidator.isFulfilledBy(orderDTO);
        if (isValidated) {
            final Long decodedOrderId = encryptionFacade.decodeId(orderDTO.getId());
            final Optional<Order> placedOrder = orderRepository.findById(decodedOrderId);
            if (placedOrder.isPresent()) {
                final Order order = placedOrder.get();
                sendConfirmationOrder(orderDTO);
                order.setStatus(DONE);
                order.setCompletionDate(now());
                order.setInvoice(toEntity(orderDTO.getInvoice()));
                orderRepository.save(order);
                return true;
            }
        }
        return false;
    }

    public OrderStatus getOrderStatus(String orderId) {
        if (orderId.equals("undefined")) {
            return null;
        }
        final Long decodedOrderId = encryptionFacade.decodeId(orderId);
        return orderRepository.findById(decodedOrderId)
                .map(Order::getStatus)
                .orElse(null);
    }

    public Long getCurrentQuantity(HttpServletRequest request) {
        OrderDto currentOrder = getCurrentOrder(request);
        return (long) currentOrder.getOrderProducts().stream()
                .map(OrderProductDTO::getQuantity)
                .reduce(0, Integer::sum);
    }

    private void sendConfirmationOrder(final OrderDto orderDTO) {
        final String email = orderDTO.getAddress().getEmail();
        final Map<String, Object> properties = of(
                "products", orderDTO.getOrderProducts(),
                "amount", orderDTO.getAmount(),
                "delivery", now().plusDays(3L),
                "address", orderDTO.getAddress());
        this.emailFacade.sendEmailWithContent(
                email,
                "Podsumownaie zakupów w KKWJ",
                properties,
                "orderSummary");
    }

    private Order getOrder(final HttpServletRequest request, final User user) {
        if (hasUserAnyOpenedOrders(user)) {
            return getUserOpenedOrder(user);
        } else {
            return findOrder(request, user);
        }
    }

    private Product getProduct(final Long productId) {
        return productRepository.findById(productId).orElseThrow(ProductNotExistsException::new);
    }

    private User getUser(final HttpServletRequest request) {
        try {
            return httpRequestService.getUser(request);
        } catch (UsernameNotExistsException ex) {
            return null;
        }
    }

    private Order findOrder(final HttpServletRequest request, final User user) {
        try {
            return httpRequestService.getOrder(request);
        } catch (OrderException ex) {
            return createNewOrderForUser(user);
        }
    }
}
