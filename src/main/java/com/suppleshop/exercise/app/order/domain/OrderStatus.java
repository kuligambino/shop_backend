package com.suppleshop.exercise.app.order.domain;

public enum OrderStatus {
    OPEN,
    IN_PROGRESS,
    DONE
}
