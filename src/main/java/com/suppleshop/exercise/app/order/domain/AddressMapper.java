package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;

import java.util.Set;

import static com.suppleshop.exercise.app.order.dto.AddressDto.builder;
import static java.util.stream.Collectors.toSet;

class AddressMapper {

    private AddressMapper() {
    }

    public static Address toEntity(final AddressDto addressDto) {
        final Address address = new Address();
        address.setId(addressDto.getId());
        address.setFirstName(addressDto.getFirstName());
        address.setLastName(addressDto.getLastName());
        address.setEmail(addressDto.getEmail());
        address.setPhoneNumber(addressDto.getPhoneNumber());
        address.setStreet(addressDto.getStreet());
        address.setCity(addressDto.getCity());
        address.setPostCode(addressDto.getPostCode());
        address.setCountry(addressDto.getCountry());
        address.setMain(addressDto.isMain());
        return address;
    }

    public static Set<AddressDto> toCollectionDto(Set<Address> addresses) {
        return addresses.stream()
                .map(AddressMapper::toDto)
                .collect(toSet());
    }

    public static AddressDto toDto(final Address address) {
        return builder()
                .id(address.getId())
                .firstName(address.getFirstName())
                .lastName(address.getLastName())
                .email(address.getEmail())
                .phoneNumber(address.getPhoneNumber())
                .street(address.getStreet())
                .city(address.getCity())
                .country(address.getCountry())
                .postCode(address.getPostCode())
                .isMain(address.isMain())
                .build();
    }
}
