package com.suppleshop.exercise.app.order.dto;

public class InvoiceDto {
    private Long id;
    private String companyName;
    private String nip;
    private String companyAddress;

    public Long getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getNip() {
        return nip;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public static InvoiceDtoBuilder builder() {
        return new InvoiceDtoBuilder();
    }

    public static final class InvoiceDtoBuilder {
        private Long id;
        private String companyName;
        private String nip;
        private String companyAddress;

        private InvoiceDtoBuilder() {
        }

        public InvoiceDtoBuilder id(final Long id) {
            this.id = id;
            return this;
        }

        public InvoiceDtoBuilder companyName(final String companyName) {
            this.companyName = companyName;
            return this;
        }

        public InvoiceDtoBuilder nip(final String nip) {
            this.nip = nip;
            return this;
        }

        public InvoiceDtoBuilder companyAddress(final String companyAddress) {
            this.companyAddress = companyAddress;
            return this;
        }

        public InvoiceDto build() {
            final InvoiceDto invoiceDto = new InvoiceDto();
            invoiceDto.companyName = this.companyName;
            invoiceDto.id = this.id;
            invoiceDto.nip = this.nip;
            invoiceDto.companyAddress = this.companyAddress;
            return invoiceDto;
        }
    }
}
