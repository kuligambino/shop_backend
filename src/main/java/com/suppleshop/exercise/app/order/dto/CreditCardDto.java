package com.suppleshop.exercise.app.order.dto;

public class CreditCardDto {
    private Long id;
    private String ownerFirstName;
    private String ownerLastName;
    private String cardNumber;
    private String expirationYear;
    private String expirationMonth;
    private String cvv;

    public Long getId() {
        return id;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public String getCvv() {
        return cvv;
    }

    public static CreditCardDtoBuilder builder() {
        return new CreditCardDtoBuilder();
    }

    public static final class CreditCardDtoBuilder {

        private Long id;
        private String ownerFirstName;
        private String ownerLastName;
        private String cardNumber;
        private String expirationYear;
        private String expirationMonth;
        private String cvv;

        public CreditCardDtoBuilder id(final Long id) {
            this.id = id;
            return this;
        }

        public CreditCardDtoBuilder ownerFirstName(final String ownerFirstName) {
            this.ownerFirstName = ownerFirstName;
            return this;
        }

        public CreditCardDtoBuilder ownerLastName(final String ownerLastName) {
            this.ownerLastName = ownerLastName;
            return this;
        }

        public CreditCardDtoBuilder cardNumber(final String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public CreditCardDtoBuilder expirationYear(final String expirationYear) {
            this.expirationYear = expirationYear;
            return this;
        }

        public CreditCardDtoBuilder expirationMonth(final String expirationMonth) {
            this.expirationMonth = expirationMonth;
            return this;
        }

        public CreditCardDtoBuilder cvv(final String cvv) {
            this.cvv = cvv;
            return this;
        }

        public CreditCardDto build() {
            final CreditCardDto creditCard = new CreditCardDto();
            creditCard.id = id;
            creditCard.ownerFirstName = ownerFirstName;
            creditCard.ownerLastName = ownerLastName;
            creditCard.cardNumber = cardNumber;
            creditCard.expirationYear = expirationYear;
            creditCard.expirationMonth = expirationMonth;
            creditCard.cvv = cvv;
            return creditCard;
        }
    }
}