package com.suppleshop.exercise.app.order.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findByUserIdAndStatus(Long id, OrderStatus status);

    @Query(value = "SELECT * FROM _order WHERE id=:id", nativeQuery = true)
    Optional<Order> findById(@Param("id") Long id);
}
