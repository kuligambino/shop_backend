package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.InvoiceDto;
import org.springframework.stereotype.Component;

@Component
public class InvoiceFacade {

    private final ValidationService<InvoiceDto> validationService;

    InvoiceFacade(ValidationService<InvoiceDto> validationService) {
        this.validationService = validationService;
    }

    public boolean isValidated(InvoiceDto invoice) {
        return validationService.isValidated(invoice);
    }
}
