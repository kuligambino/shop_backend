package com.suppleshop.exercise.app.order.domain;

import com.suppleshop.exercise.app.order.dto.AddressDto;
import com.suppleshop.exercise.app.order.dto.OrderDto;
import com.suppleshop.exercise.app.order.dto.OrderProductDTO;
import com.suppleshop.exercise.app.products.domain.Product;
import com.suppleshop.exercise.app.products.domain.ProductMapper;
import com.suppleshop.exercise.app.security.domain.EncryptionFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.suppleshop.exercise.app.order.domain.AddressMapper.toDto;
import static com.suppleshop.exercise.app.order.dto.OrderDto.builder;

@Component
class OrderMapper {

    private final ProductMapper productMapper;
    private final EncryptionFacade encryptionFacade;

    @Autowired
    public OrderMapper(ProductMapper productMapper, EncryptionFacade encryptionFacade) {
        this.productMapper = productMapper;
        this.encryptionFacade = encryptionFacade;
    }

    public OrderDto convertToOrderDTO(final Order order) {
        final List<Product> products = order.getProducts();
        final String encodedId = encryptionFacade.encodeId(order.getId());
        AddressDto addressDto = null;
        if (order.getShippingAddress() != null) {
            addressDto = toDto(order.getShippingAddress());
        }
        final List<OrderProductDTO> cartProducts = products.stream()
                .map(product ->
                        productMapper.convertToOrderProductDTO(order, product))
                .distinct()
                .collect(Collectors.toList());
        return builder()
                .id(encodedId)
                .orderProducts(cartProducts)
                .amount(order.getAmount())
                .address(addressDto)
                .build();
    }
}
