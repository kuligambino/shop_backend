package com.suppleshop.exercise.app.reset_password.domain;

import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.reset_password.dto.RequestResetPasswordDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
class RequestResetPasswordValidator implements BaseSpec<RequestResetPasswordDto> {

    private final List<BaseSpec<RequestResetPasswordDto>> specifications;

    RequestResetPasswordValidator(List<BaseSpec<RequestResetPasswordDto>> specifications) {
        this.specifications = specifications;
    }

    @Override
    public boolean isFulfilledBy(RequestResetPasswordDto requestResetPasswordDto) throws UnsatisfiedSpecificationException {
        return specifications.stream().allMatch(specification -> specification.isFulfilledBy(requestResetPasswordDto));
    }
}

@Component
class EmailSpecification implements BaseSpec<RequestResetPasswordDto> {

    private final UserRepository userRepository;
    private final Environment env;

    EmailSpecification(UserRepository userRepository, Environment env) {
        this.userRepository = userRepository;
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(RequestResetPasswordDto requestResetPasswordDto) throws UnsatisfiedSpecificationException {
        final String email = requestResetPasswordDto.getUserEmail();

        final boolean emailIsNotDefined = isBlank(email);

        if (emailIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Email is not defined!");
        }

        final boolean emailHasWrongFormat = !email.matches(requireNonNull(env.getProperty("email.regex")));

        if (emailHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Email has wrong format!");
        }

        final boolean emailNotExists = userRepository.findByEmail(email).isEmpty();

        if (emailNotExists) {
            throw new UnsatisfiedSpecificationException("Email doesn't exist!");
        }

        return true;
    }
}

class RecaptchaSpecification implements BaseSpec<RequestResetPasswordDto> {

    @Override
    public boolean isFulfilledBy(RequestResetPasswordDto requestResetPasswordDto) throws UnsatisfiedSpecificationException {
        boolean recaptchaIsNotAccepted = !requestResetPasswordDto.isRecaptchaAccepted();
        if (recaptchaIsNotAccepted) {
            throw new UnsatisfiedSpecificationException("Recaptcha must be accepted!");
        }
        return true;
    }
}
