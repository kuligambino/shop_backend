package com.suppleshop.exercise.app.reset_password.endpoints;

import com.suppleshop.exercise.app.reset_password.domain.ResetPasswordFacade;
import com.suppleshop.exercise.app.reset_password.dto.RequestResetPasswordDto;
import com.suppleshop.exercise.app.reset_password.dto.ResetPasswordDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("password")
class ResetPasswordController {

    private final ResetPasswordFacade resetPasswordFacade;

    ResetPasswordController(ResetPasswordFacade resetPasswordFacade) {
        this.resetPasswordFacade = resetPasswordFacade;
    }

    @PostMapping("forget")
    public ResponseEntity<Void> sendEmailWithResetForm(@RequestBody RequestResetPasswordDto requestResetPasswordDto) {
        resetPasswordFacade.sendEmailWithResetToken(requestResetPasswordDto);
        return status(OK).build();
    }

    @PostMapping("reset/confirm")
    public ResponseEntity<Void> confirmResettingPassword(@RequestParam String token, @RequestBody ResetPasswordDto resetPasswordModel) {
        resetPasswordFacade.resetPassword(token, resetPasswordModel);
        return status(OK).build();
    }
}
