package com.suppleshop.exercise.app.reset_password.domain;

import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.token.domain.UserTokenFacade;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static java.util.List.of;

@Component
class ResetPasswordSpecificationFactory {

    private final UserTokenFacade userTokenFacade;
    private final UserRepository userRepository;
    private final Environment env;

    ResetPasswordSpecificationFactory(UserTokenFacade userTokenFacade, UserRepository userRepository, Environment env) {
        this.userTokenFacade = userTokenFacade;
        this.userRepository = userRepository;
        this.env = env;
    }

    public ResetPasswordValidator defineResetPasswordSpecification(String token) {
        return new ResetPasswordValidator(of(
                new PasswordSpec(env),
                new TokenSpec(token, userTokenFacade)
        ));
    }

    public RequestResetPasswordValidator defineRequestResetPasswordSpecification() {
        return new RequestResetPasswordValidator(of(
                new EmailSpecification(userRepository, env),
                new RecaptchaSpecification()
        ));
    }
}
