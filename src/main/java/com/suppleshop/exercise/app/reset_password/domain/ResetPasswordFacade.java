package com.suppleshop.exercise.app.reset_password.domain;

import com.suppleshop.exercise.app.reset_password.dto.RequestResetPasswordDto;
import com.suppleshop.exercise.app.reset_password.dto.ResetPasswordDto;
import org.springframework.stereotype.Component;

@Component
public class ResetPasswordFacade {

    private final ResetPasswordService resetPasswordService;

    ResetPasswordFacade(ResetPasswordService resetPasswordService) {
        this.resetPasswordService = resetPasswordService;
    }

    public boolean sendEmailWithResetToken(RequestResetPasswordDto requestResetPasswordDto) {
        return this.resetPasswordService.sendEmailWithResetToken(requestResetPasswordDto);
    }

    public void resetPassword(String token, ResetPasswordDto resetPasswordDto) {
        this.resetPasswordService.resetPassword(token, resetPasswordDto);
    }
}
