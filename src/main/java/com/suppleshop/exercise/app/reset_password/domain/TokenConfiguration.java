package com.suppleshop.exercise.app.reset_password.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class TokenConfiguration {

    private static final String TOKEN = "default";

    @Bean
    public String token() {
        return TOKEN;
    }
}
