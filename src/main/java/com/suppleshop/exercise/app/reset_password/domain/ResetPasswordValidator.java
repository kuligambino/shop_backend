package com.suppleshop.exercise.app.reset_password.domain;

import com.suppleshop.exercise.app.reset_password.dto.ResetPasswordDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import com.suppleshop.exercise.app.token.domain.UserTokenFacade;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
class ResetPasswordValidator implements BaseSpec<ResetPasswordDto> {

    private final List<BaseSpec<ResetPasswordDto>> specifications;

    public ResetPasswordValidator(List<BaseSpec<ResetPasswordDto>> specifications) {
        this.specifications = specifications;
    }

    @Override
    public boolean isFulfilledBy(ResetPasswordDto resetPasswordDto) throws UnsatisfiedSpecificationException {
        return specifications.stream().allMatch(spec -> spec.isFulfilledBy(resetPasswordDto));
    }
}

@Component
class PasswordSpec implements BaseSpec<ResetPasswordDto> {

    private final Environment env;

    public PasswordSpec(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(ResetPasswordDto resetPasswordDto) throws UnsatisfiedSpecificationException {
        final String newPassword = resetPasswordDto.getPassword();
        final String confirmedNewPassword = resetPasswordDto.getConfirmedPassword();

        final boolean passwordsAreNotDefined = isBlank(newPassword) || isBlank(confirmedNewPassword);

        if (passwordsAreNotDefined) {
            throw new UnsatisfiedSpecificationException("Passwords must be defined!");
        }

        final String passwordRegex = requireNonNull(env.getProperty("password.regex"));

        final boolean passwordsHaveWrongFormat = !newPassword.matches(passwordRegex) || !confirmedNewPassword.matches(passwordRegex);

        if (passwordsHaveWrongFormat) {
            throw new UnsatisfiedSpecificationException("Passwords must have correct format");
        }

        final boolean passwordsAreDifferent = !newPassword.equals(confirmedNewPassword);

        if (passwordsAreDifferent) {
            throw new UnsatisfiedSpecificationException("Passwords must be same!");
        }

        return true;
    }
}

@Component
class TokenSpec implements BaseSpec<ResetPasswordDto> {

    private final String token;
    private final UserTokenFacade userTokenFacade;

    TokenSpec(String token, UserTokenFacade userTokenFacade) {
        this.token = token;
        this.userTokenFacade = userTokenFacade;
    }

    @Override
    public boolean isFulfilledBy(ResetPasswordDto resetPasswordDto) throws UnsatisfiedSpecificationException {
        boolean tokenIsInvalid = !userTokenFacade.checkIfValid1(token);
        if (tokenIsInvalid) {
            throw new UnsatisfiedSpecificationException("Token is invalid!");
        }
        return true;
    }
}
