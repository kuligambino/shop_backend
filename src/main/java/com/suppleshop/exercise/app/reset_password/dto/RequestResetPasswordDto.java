package com.suppleshop.exercise.app.reset_password.dto;

public class RequestResetPasswordDto {
    private String userEmail;
    private boolean recaptchaAccepted;

    public String getUserEmail() {
        return userEmail;
    }

    public boolean isRecaptchaAccepted() {
        return recaptchaAccepted;
    }
}
