package com.suppleshop.exercise.app.reset_password.domain;

import com.suppleshop.exercise.app.email.domain.EmailFacade;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.reset_password.dto.RequestResetPasswordDto;
import com.suppleshop.exercise.app.reset_password.dto.ResetPasswordDto;
import com.suppleshop.exercise.app.shared.exception.BusinessException;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import com.suppleshop.exercise.app.token.domain.UserToken;
import com.suppleshop.exercise.app.token.domain.UserTokenFacade;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
class ResetPasswordService {

    private static final String EMAIL_MESSAGE = "Kliknij w link, aby zresetować hasło: ";
    private static final String EMAIL_SUBJECT = "Zresetuj hasło ";
    private static final String RESET_PASSWORD_WITH_TOKEN = "http://localhost:4200/password/reset/confirm?token=";

    private final UserRepository userRepository;
    private final UserTokenFacade userTokenFacade;
    private final EmailFacade emailFacade;
    private final PasswordEncoder passwordEncoder;
    private final ResetPasswordSpecificationFactory resetPasswordSpecificationFactory;

    public ResetPasswordService(UserRepository userRepository, UserTokenFacade userTokenFacade,
                                EmailFacade emailFacade, PasswordEncoder passwordEncoder,
                                ResetPasswordSpecificationFactory resetPasswordSpecificationFactory) {
        this.userRepository = userRepository;
        this.userTokenFacade = userTokenFacade;
        this.emailFacade = emailFacade;
        this.passwordEncoder = passwordEncoder;
        this.resetPasswordSpecificationFactory = resetPasswordSpecificationFactory;
    }

    public boolean sendEmailWithResetToken(RequestResetPasswordDto requestResetPasswordDto) {
        try {
            boolean canSendToken = resetPasswordSpecificationFactory.defineRequestResetPasswordSpecification().isFulfilledBy(requestResetPasswordDto);
            User user = userRepository.findByEmail(requestResetPasswordDto.getUserEmail()).orElseThrow(UsernameNotExistsException::new);
            UserToken userToken = userTokenFacade.generateResetPasswordToken(user);
            emailFacade.sendEmail(user.getEmail(), EMAIL_SUBJECT, EMAIL_MESSAGE + RESET_PASSWORD_WITH_TOKEN + userToken.getToken());
            return canSendToken;
        } catch (UnsatisfiedSpecificationException ex) {
            throw new BusinessException(ex.getMessage(), ex.getCause());
        }
    }

    public void resetPassword(String token, ResetPasswordDto resetPasswordDto) {
        boolean isValidated = resetPasswordSpecificationFactory.defineResetPasswordSpecification(token).isFulfilledBy(resetPasswordDto);
        if (isValidated) {
            final UserToken userToken = userTokenFacade.findByToken(token);
            final User user = userToken.getUser();
            user.setPassword(passwordEncoder.encode(resetPasswordDto.getPassword()));
            userToken.setValid(false);
            userRepository.save(user);
        }
    }
}
