package com.suppleshop.exercise.app.store_data.domain;

import com.suppleshop.exercise.app.store_data.dto.StoreDataDto;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import static com.suppleshop.exercise.app.store_data.dto.StoreDataDto.builder;

@Configuration
@PropertySource("classpath:regex.properties")
class StoreConfigure {

    private final Environment env;

    public StoreConfigure(Environment env) {
        this.env = env;
    }

    public StoreDataDto initializeStoreData() {
        return builder()
                .emailPattern(env.getProperty("email.regex"))
                .phoneNumberPattern(env.getProperty("phoneNumber.regex"))
                .postCodePattern(env.getProperty("postCode.regex"))
                .passwordPattern(env.getProperty("password.regex"))
                .birthDatePattern(env.getProperty("birthDate.regex"))
                .creditCardPattern(env.getProperty("creditCard.regex"))
                .cvvPattern(env.getProperty("cvv.regex"))
                .nipPattern(env.getProperty("nip.regex"))
                .build();
    }
}
