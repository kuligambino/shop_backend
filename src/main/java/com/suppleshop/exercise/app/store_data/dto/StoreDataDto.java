package com.suppleshop.exercise.app.store_data.dto;

public class StoreDataDto {

    private String emailPattern;
    private String phoneNumberPattern;
    private String postCodePattern;
    private String passwordPattern;
    private String birthDatePattern;
    private String creditCardPattern;
    private String cvvPattern;
    private String nipPattern;

    public String getEmailPattern() {
        return emailPattern;
    }

    public void setEmailPattern(String emailPattern) {
        this.emailPattern = emailPattern;
    }

    public String getPhoneNumberPattern() {
        return phoneNumberPattern;
    }

    public void setPhoneNumberPattern(String phoneNumberPattern) {
        this.phoneNumberPattern = phoneNumberPattern;
    }

    public String getPostCodePattern() {
        return postCodePattern;
    }

    public void setPostCodePattern(String postCodePattern) {
        this.postCodePattern = postCodePattern;
    }

    public String getPasswordPattern() {
        return passwordPattern;
    }

    public void setPasswordPattern(String passwordPattern) {
        this.passwordPattern = passwordPattern;
    }

    public String getBirthDatePattern() {
        return birthDatePattern;
    }

    public void setBirthDatePattern(String birthDatePattern) {
        this.birthDatePattern = birthDatePattern;
    }

    public String getCreditCardPattern() {
        return creditCardPattern;
    }

    public void setCreditCardPattern(String creditCardPattern) {
        this.creditCardPattern = creditCardPattern;
    }

    public String getCvvPattern() {
        return cvvPattern;
    }

    public void setCvvPattern(String cvvPattern) {
        this.cvvPattern = cvvPattern;
    }

    public String getNipPattern() {
        return nipPattern;
    }

    public void setNipPattern(String nipPattern) {
        this.nipPattern = nipPattern;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String emailPattern;
        private String phoneNumberPattern;
        private String postCodePattern;
        private String passwordPattern;
        private String birthDatePattern;
        private String creditCardPattern;
        private String cvvPattern;
        private String nipPattern;

        public Builder emailPattern(final String emailPattern) {
            this.emailPattern = emailPattern;
            return this;
        }

        public Builder phoneNumberPattern(final String phoneNumberPattern) {
            this.phoneNumberPattern = phoneNumberPattern;
            return this;
        }

        public Builder postCodePattern(final String postCodePattern) {
            this.postCodePattern = postCodePattern;
            return this;
        }

        public Builder passwordPattern(final String passwordPattern) {
            this.passwordPattern = passwordPattern;
            return this;
        }

        public Builder birthDatePattern(final String birthDatePattern) {
            this.birthDatePattern = birthDatePattern;
            return this;
        }

        public Builder creditCardPattern(final String creditCardPattern) {
            this.creditCardPattern = creditCardPattern;
            return this;
        }

        public Builder cvvPattern(final String cvvPattern) {
            this.cvvPattern = cvvPattern;
            return this;
        }

        public Builder nipPattern(final String nipPattern) {
            this.nipPattern = nipPattern;
            return this;
        }

        public StoreDataDto build() {
            StoreDataDto storeDataDto = new StoreDataDto();
            storeDataDto.emailPattern = this.emailPattern;
            storeDataDto.phoneNumberPattern = this.phoneNumberPattern;
            storeDataDto.postCodePattern = this.postCodePattern;
            storeDataDto.passwordPattern = this.passwordPattern;
            storeDataDto.birthDatePattern = this.birthDatePattern;
            storeDataDto.creditCardPattern = this.creditCardPattern;
            storeDataDto.cvvPattern = this.cvvPattern;
            storeDataDto.nipPattern = this.nipPattern;
            return storeDataDto;
        }
    }
}
