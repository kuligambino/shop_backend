package com.suppleshop.exercise.app.store_data.endpoints;

import com.suppleshop.exercise.app.store_data.domain.StoreDataFacade;
import com.suppleshop.exercise.app.store_data.dto.StoreDataDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/store-data")
class StoreDataController {

    private final StoreDataFacade storeDataFacade;

    public StoreDataController(StoreDataFacade storeDataFacade) {
        this.storeDataFacade = storeDataFacade;
    }

    @GetMapping
    public ResponseEntity<StoreDataDto> getStoreData() {
        return status(OK).body(storeDataFacade.initializeStoreData());
    }
}
