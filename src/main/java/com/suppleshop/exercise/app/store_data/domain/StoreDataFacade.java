package com.suppleshop.exercise.app.store_data.domain;

import com.suppleshop.exercise.app.store_data.dto.StoreDataDto;
import org.springframework.stereotype.Component;

@Component
public class StoreDataFacade {

    private final StoreConfigure storeConfig;

    public StoreDataFacade(StoreConfigure storeConfig) {
        this.storeConfig = storeConfig;
    }

    public StoreDataDto initializeStoreData() {
        return this.storeConfig.initializeStoreData();
    }
}
