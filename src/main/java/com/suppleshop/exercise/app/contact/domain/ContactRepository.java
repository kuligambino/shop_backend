package com.suppleshop.exercise.app.contact.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for {@link Contact}
 */
interface ContactRepository extends JpaRepository<Contact, Long> {
}
