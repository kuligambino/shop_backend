package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.shared.AbstractEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import java.time.LocalDateTime;

@Entity
class Contact extends AbstractEntity {

    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String details;
    @Column(nullable = false)
    private int grade;
    @Column(nullable = false)
    private LocalDateTime sendDate;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }
}
