package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.exception.ContactException;
import org.springframework.stereotype.Service;

import static com.suppleshop.exercise.app.contact.domain.ContactMapper.toDto;
import static com.suppleshop.exercise.app.contact.domain.ContactMapper.toEntity;

/**
 * Defines methods to handling {@link Contact} operations
 */
@Service
class ContactService {

    private final ContactRepository contactRepository;
    private final ContactValidator validator;

    ContactService(ContactRepository contactRepository, ContactValidator validator) {
        this.contactRepository = contactRepository;
        this.validator = validator;
    }

    /**
     * Adds new {@link Contact} based on data transfer object
     *
     * @param contactDto data transfer object
     * @return saved Contact formatted to dto
     */
    public ContactDto addContact(final ContactDto contactDto) {
        if (validator.isFulfilledBy(contactDto)) {
            final Contact contact = toEntity(contactDto);
            return toDto(contactRepository.save(contact));
        } else {
            throw new ContactException();
        }
    }
}
