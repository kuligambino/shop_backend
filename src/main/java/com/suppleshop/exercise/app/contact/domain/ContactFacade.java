package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import org.springframework.stereotype.Component;

/**
 * Entry point for Contact Module
 */
@Component
public class ContactFacade {

    private final ContactService contactService;

    public ContactFacade(ContactService contactService) {
        this.contactService = contactService;
    }

    public ContactDto addContact(final ContactDto contact) {
        return this.contactService.addContact(contact);
    }
}
