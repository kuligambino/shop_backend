package com.suppleshop.exercise.app.contact.endpoints;

import com.suppleshop.exercise.app.contact.domain.ContactFacade;
import com.suppleshop.exercise.app.contact.dto.ContactDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.status;

/**
 * Controller handles HTTP request related to Contact
 */
@RestController
@RequestMapping("/contact")
class ContactController {

    private final ContactFacade contactFacade;

    public ContactController(ContactFacade contactFacade) {
        this.contactFacade = contactFacade;
    }

    @PostMapping
    public ResponseEntity<ContactDto> addContact(final @RequestBody ContactDto contactDto) {
        ContactDto contact = contactFacade.addContact(contactDto);
        return status(CREATED).body(contact);
    }
}
