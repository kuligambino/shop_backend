package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;

import static com.suppleshop.exercise.app.contact.dto.ContactDto.builder;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Defines all methods used to converting {@link Contact} and {@link ContactDto}
 */
class ContactMapper {

    private ContactMapper() {
    }

    /**
     * Converts contact to data transfer object (Dto)
     *
     * @param contact converted object
     * @return Dto converted from entity
     */
    public static ContactDto toDto(final Contact contact) {
        return builder()
                .email(contact.getEmail())
                .details(contact.getDetails())
                .grade(contact.getGrade())
                .recaptcha(true)
                .build();
    }

    /**
     * Converts data transfer object (Dto) to Contact entity
     *
     * @param contactDto converted object
     * @return Entity converted from dto
     */
    public static Contact toEntity(final ContactDto contactDto) {
        final Contact contact = new Contact();
        contact.setEmail(contactDto.getEmail());
        contact.setDetails(contactDto.getDetails());
        contact.setGrade(contactDto.getGrade());
        contact.setSendDate(now().truncatedTo(SECONDS));
        return contact;
    }
}
