package com.suppleshop.exercise.app.contact.dto;

public class ContactDto {

    private String email;
    private String details;
    private int grade;
    private boolean recaptcha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public boolean isRecaptcha() {
        return recaptcha;
    }

    public void setRecaptcha(boolean recaptcha) {
        this.recaptcha = recaptcha;
    }

    public static ContactDtoBuilder builder() {
        return new ContactDtoBuilder();
    }

    public static class ContactDtoBuilder {

        private String email;
        private String details;
        private int grade;
        private boolean recaptcha;

        public ContactDtoBuilder email(final String email) {
            this.email = email;
            return this;
        }

        public ContactDtoBuilder details(final String details) {
            this.details = details;
            return this;
        }

        public ContactDtoBuilder grade(final int grade) {
            this.grade = grade;
            return this;
        }

        public ContactDtoBuilder recaptcha(final boolean recaptcha) {
            this.recaptcha = recaptcha;
            return this;
        }

        public ContactDto build() {
            final ContactDto contactDto = new ContactDto();
            contactDto.email = this.email;
            contactDto.details = this.details;
            contactDto.grade = this.grade;
            contactDto.recaptcha = this.recaptcha;
            return contactDto;
        }
    }
}
