package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.contact.dto.ContactDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Validator for {@link Contact}
 */
@Component
class ContactValidator implements BaseSpec<ContactDto> {

    private final List<BaseSpec<ContactDto>> specifications;

    public ContactValidator(List<BaseSpec<ContactDto>> specifications) {
        this.specifications = specifications;
    }

    @Override
    public boolean isFulfilledBy(ContactDto contact) {
        return specifications.stream().allMatch(spec -> spec.isFulfilledBy(contact));
    }
}

/**
 * Defines all requirements for Email
 */
class EmailSpec implements BaseSpec<ContactDto> {

    private final Environment env;

    public EmailSpec(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(ContactDto contact) {

        if (contact != null) {
            String email = contact.getEmail();

            boolean emailIsNotDefined = isBlank(email);
            if (emailIsNotDefined) {
                throw new UnsatisfiedSpecificationException("Email is not defined!");
            }

            String emailRegex = env.getProperty("email.regex");
            boolean emailHasWrongFormat = !email.matches(requireNonNull(emailRegex));
            if (emailHasWrongFormat) {
                throw new UnsatisfiedSpecificationException("Email has wrong format!");
            }
            return true;
        }
        throw new UnsatisfiedSpecificationException("Contact is not defined!");
    }
}

/**
 * Defines all requirements for Details
 */
class DetailsSpec implements BaseSpec<ContactDto> {

    static final int MIN_DETAILS_LENGTH = 50;
    static final String MIN_DETAILS_MESSAGE = "Details length must be at least " + MIN_DETAILS_LENGTH + " characters long";

    @Override
    public boolean isFulfilledBy(ContactDto contact) {
        if (contact != null) {
            String details = contact.getDetails();

            boolean detailsAreNotDefined = isBlank(details);
            if (detailsAreNotDefined) {
                throw new UnsatisfiedSpecificationException("Details are not defined!");
            }

            boolean detailsLengthIsTooShort = details.length() <= MIN_DETAILS_LENGTH;
            if (detailsLengthIsTooShort) {
                throw new UnsatisfiedSpecificationException(MIN_DETAILS_MESSAGE);
            }
            return true;
        }
        throw new UnsatisfiedSpecificationException("Contact is not defined!");
    }
}

/**
 * Defines all requirements for Grade
 */
class GradeSpec implements BaseSpec<ContactDto> {

    private static final int WORST_GRADE = 1;
    private static final int BEST_GRADE = 10;

    @Override
    public boolean isFulfilledBy(ContactDto contact) {
        if (contact != null) {
            int grade = contact.getGrade();

            boolean gradeIsLowerThanWorst = grade < WORST_GRADE;
            if (gradeIsLowerThanWorst) {
                throw new UnsatisfiedSpecificationException("Grade can't be lower than " + WORST_GRADE);
            }

            boolean gradeIsHigherThanBest = grade > BEST_GRADE;
            if (gradeIsHigherThanBest) {
                throw new UnsatisfiedSpecificationException("Grade can't be higher than " + BEST_GRADE);
            }
            return true;
        }
        throw new UnsatisfiedSpecificationException("Contact is not defined!");
    }
}

/**
 * Defines all requirements for Recaptcha
 */
class RecaptchaSpec implements BaseSpec<ContactDto> {

    @Override
    public boolean isFulfilledBy(ContactDto contact) {
        if (contact != null) {

            boolean isRecaptchaNotAccepted = !contact.isRecaptcha();
            if (isRecaptchaNotAccepted) {
                throw new UnsatisfiedSpecificationException("Recaptcha must be accepted!");
            }
            return true;
        }
        throw new UnsatisfiedSpecificationException("Contact is not defined!");
    }
}
