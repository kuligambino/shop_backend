package com.suppleshop.exercise.app.contact.domain;

import com.suppleshop.exercise.app.shared.BaseSpecFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static java.util.List.of;

/**
 * Contains method which gather all contact specifications
 */
@Component
class ContactSpecificationFactory implements BaseSpecFactory<ContactValidator> {

    private final Environment env;

    public ContactSpecificationFactory(Environment env) {
        this.env = env;
    }

    @Override
    public ContactValidator defineSpecification() {
        return new ContactValidator(of(
                new EmailSpec(env),
                new DetailsSpec(),
                new GradeSpec(),
                new RecaptchaSpec()
        ));
    }
}
