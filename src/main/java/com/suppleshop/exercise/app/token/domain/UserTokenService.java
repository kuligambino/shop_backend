package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.shared.exception.TokenNotExistException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@Service
class UserTokenService {

    private static final String REMOVAL_TIME = "0 0 5 * * ?"; // 5am every day

    private final UserTokenRepository userTokenRepository;
    private final TokenRepository tokenRepository;

    public UserTokenService(UserTokenRepository userTokenRepository, TokenRepository tokenRepository) {
        this.userTokenRepository = userTokenRepository;
        this.tokenRepository = tokenRepository;
    }

    public UserToken generateTokenByType(final User user, final TokenType tokenType) {
        final UserToken userToken = TokenHelper.generateToken(user);
        final Token token = tokenRepository.findByType(tokenType);
        userToken.setTokenType(token);
        return userTokenRepository.save(userToken);
    }

    public UserToken findByToken(final String token) {
        return userTokenRepository.findByToken(token)
                .orElseThrow(TokenNotExistException::new);
    }

    public boolean checkIfValid(final UserToken userToken) {
        final boolean isValid = userToken.isValid();
        final LocalDateTime currentTime = now();
        final LocalDateTime tokenExpiryDate = userToken.getExpiryDate();
        return tokenExpiryDate.isAfter(currentTime) || isValid;
    }

    public boolean checkIfValid1(final String token) {
        final UserToken userToken = findByToken(token);
        final boolean isValid = userToken.isValid();
        final LocalDateTime currentTime = now();
        final LocalDateTime tokenExpiryDate = userToken.getExpiryDate();
        return tokenExpiryDate.isAfter(currentTime) || isValid;
    }

    /**
     * Cleans db of expired UserTokens.
     */
    @Scheduled(cron = REMOVAL_TIME)
    private void deleteExpiredTokens() {
        final LocalDateTime now = now();
        userTokenRepository.deleteByExpiryDateLessThan(now);
    }
}
