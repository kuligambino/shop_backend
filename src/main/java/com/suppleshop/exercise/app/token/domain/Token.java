package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.shared.AbstractEntity;
import com.suppleshop.exercise.app.token.domain.TokenType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;

import static jakarta.persistence.EnumType.STRING;

@Entity
class Token extends AbstractEntity {

    @Column(nullable = false)
    private String description;
    @Column(unique = true, nullable = false)
    @Enumerated(STRING)
    private TokenType type;

    public TokenType getType() {
        return type;
    }

    public void setType(TokenType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
