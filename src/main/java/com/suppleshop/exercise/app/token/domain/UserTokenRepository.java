package com.suppleshop.exercise.app.token.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

interface UserTokenRepository extends JpaRepository<UserToken, Long> {

    /**
     * Method searches UserToken by param token.
     *
     * @param token
     * @return VerificationToken assigned to User.
     */
    Optional<UserToken> findByToken(String token);

    /**
     * Deletes all expired tokens.
     *
     * @param now
     */
    void deleteByExpiryDateLessThan(LocalDateTime now);
}
