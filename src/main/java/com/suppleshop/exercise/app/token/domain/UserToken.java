package com.suppleshop.exercise.app.token.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.suppleshop.exercise.app.shared.AbstractEntity;
import com.suppleshop.exercise.app.register.domain.User;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
public class UserToken extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String token;
    @Column(nullable = false)
    private LocalDateTime expiryDate;
    @Column(nullable = false)
    private boolean isValid;

    @OneToOne
    @JoinColumn(nullable = false)
    private Token tokenType;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Token getTokenType() {
        return tokenType;
    }

    public void setTokenType(Token tokenType) {
        this.tokenType = tokenType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
