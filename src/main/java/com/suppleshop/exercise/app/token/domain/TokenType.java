package com.suppleshop.exercise.app.token.domain;

enum TokenType {

    VERIFICATION("{verification_token_description}"),
    PASSWORD_RESET("{password_reset_token_description}");

    private final String description;

    TokenType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
