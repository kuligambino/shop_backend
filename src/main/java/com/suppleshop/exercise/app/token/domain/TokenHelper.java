package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.register.domain.User;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

class TokenHelper {

    private static final long DAYS_TO_EXPIRY = 0;
    private static final boolean IS_VALID = true;

    private TokenHelper() {
    }

    public static UserToken generateToken(final User user) {
        final UserToken userToken = new UserToken();
        userToken.setToken(randomUUID().toString());
        userToken.setExpiryDate(now().plusDays(DAYS_TO_EXPIRY));
        userToken.setValid(IS_VALID);
        userToken.setUser(user);
        user.getTokens().add(userToken);
        return userToken;
    }
}
