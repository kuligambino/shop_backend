package com.suppleshop.exercise.app.token.domain;

import org.springframework.data.jpa.repository.JpaRepository;

interface TokenRepository extends JpaRepository<Token, Long> {

    /**
     * Finds requested token by type.
     *
     * @return Token depending on request.
     */
    Token findByType(TokenType type);
}
