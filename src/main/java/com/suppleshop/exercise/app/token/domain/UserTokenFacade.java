package com.suppleshop.exercise.app.token.domain;

import com.suppleshop.exercise.app.register.domain.User;
import org.springframework.stereotype.Component;

import static com.suppleshop.exercise.app.token.domain.TokenType.PASSWORD_RESET;
import static com.suppleshop.exercise.app.token.domain.TokenType.VERIFICATION;

@Component
public class UserTokenFacade {

    private final UserTokenService userTokenService;

    public UserTokenFacade(UserTokenService userTokenService) {
        this.userTokenService = userTokenService;
    }

    /**
     * Generates token by type and assign to User.
     *
     * @param user token will be assigned to user
     * @return UserToken depending on type (CONFIRM/RESET_PASSWORD)
     */
    public UserToken generateVerificationToken(final User user) {
        return userTokenService.generateTokenByType(user, VERIFICATION);
    }

    /**
     * Generates token by type and assign to User.
     *
     * @param user token will be assigned to user
     * @return UserToken depending on type (CONFIRM/RESET_PASSWORD)
     */
    public UserToken generateResetPasswordToken(final User user) {
        return userTokenService.generateTokenByType(user, PASSWORD_RESET);
    }

    /**
     * Searches UserToken by token.
     *
     * @param token base token not assigned to user
     * @return UserToken depending on the request type
     */
    public UserToken findByToken(final String token) {
        return userTokenService.findByToken(token);
    }

    /**
     * Checks if UserToken is still valid.
     *
     * @param userToken token assigned to user
     * @return if UserToken is expired
     */
    public boolean checkIfValid(final UserToken userToken) {
        return userTokenService.checkIfValid(userToken);
    }

    /**
     * Checks if Token is still valid.
     *
     * @param token token UUID
     * @return if UserToken is expired
     */
    public boolean checkIfValid1(final String token) {
        return userTokenService.checkIfValid1(token);
    }
}
