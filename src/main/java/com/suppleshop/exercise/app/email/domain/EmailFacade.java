package com.suppleshop.exercise.app.email.domain;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Shares methods related to emails
 */
@Component
public class EmailFacade {

    private final EmailService emailService;

    public EmailFacade(EmailService emailService) {
        this.emailService = emailService;
    }

    /**
     * Method sends email to indicated receiver.
     *
     * @param to      receiver
     * @param subject email subject
     * @param text    email content (only text)
     */
    public void sendEmail(final String to, final String subject, final String text) {
        this.emailService.sendEmail(to, subject, text);
    }

    /**
     * Method sends email with content (html template)
     *
     * @param to               receiver
     * @param subject          email subject
     * @param properties       email content used in html template.
     *                         Keys are used to track place for value in html template
     * @param htmlTemplateName template name stored in resources/templates
     */
    public void sendEmailWithContent(final String to, final String subject, final Map<String, Object> properties, final String htmlTemplateName) {
        this.emailService.sendEmailWithContent(to, subject, properties, htmlTemplateName);
    }
}
