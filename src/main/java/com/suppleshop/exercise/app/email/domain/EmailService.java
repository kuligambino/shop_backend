package com.suppleshop.exercise.app.email.domain;

import org.slf4j.Logger;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Defines methods related to emails like creating emails, sending etc.
 */
@Service
class EmailService {

    private static final Logger LOG = getLogger(EmailService.class.getName());
    private static final String COMPANY_EMAIL = "supply.shopkkwj@gmail.com";

    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    public EmailService(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    public void sendEmail(final String to, final String subject, final String text) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(to);
        email.setSubject(subject);
        email.setText(text);
        javaMailSender.send(email);
    }

    public void sendEmailWithContent(final String to, final String subject, final Map<String, Object> properties, final String htmlTemplateName) {
        final String content = generateMailContent(properties, htmlTemplateName);
        final MimeMessage message = javaMailSender.createMimeMessage();
        final MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(COMPANY_EMAIL);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * Method generates content for email based on properties stored in Map.
     * Mail content is a html template stored in resources/templates
     *
     * @param properties       email content used in html template.
     *                         Keys are used to track place for value in html template
     * @param htmlTemplateName template name stored in resources/templates
     * @return reprocessed html template with content
     */
    private String generateMailContent(final Map<String, Object> properties, final String htmlTemplateName) {
        final Context context = new Context();
        properties.forEach(context::setVariable);
        return templateEngine.process(htmlTemplateName, context);
    }
}
