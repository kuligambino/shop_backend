package com.suppleshop.exercise.app.login.endpoints;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
class LoginController {

    @PostMapping
    public void login() {
    }
}
