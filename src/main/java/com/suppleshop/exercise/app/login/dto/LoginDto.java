package com.suppleshop.exercise.app.login.dto;

public class LoginDto {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static LoginDtoBuilder builder() {
        return new LoginDtoBuilder();
    }

    public static class LoginDtoBuilder {

        private String email;
        private String password;

        public LoginDtoBuilder email(final String email) {
            this.email = email;
            return this;
        }

        public LoginDtoBuilder password(final String password) {
            this.password = password;
            return this;
        }

        public LoginDto build() {
            LoginDto loginDto = new LoginDto();
            loginDto.email = this.email;
            loginDto.password = this.password;
            return loginDto;
        }
    }
}
