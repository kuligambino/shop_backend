package com.suppleshop.exercise.app.security.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static com.suppleshop.exercise.app.security.domain.AppUrl.*;

@Configuration
public class SecurityConfig {

    private final HttpRequestService httpRequestService;
    private final AuthenticationService authenticationService;

    public SecurityConfig(
            HttpRequestService httpRequestService,
            AuthenticationService authenticationService) {
        this.httpRequestService = httpRequestService;
        this.authenticationService = authenticationService;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager manager, UrlBasedCorsConfigurationSource corsFilter) throws Exception {
        http
                .cors(cors -> cors.configurationSource(corsFilter))
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(authorizeHttpRequests ->
                        authorizeHttpRequests
                                .requestMatchers(REGISTER, REGISTER_EMAIL,CONFIRM_REGISTER, CONTACT, GET_ORDER_STATUS,
                                        GET_ORDER_QUANTITY, FORGET_PASSWORD_ALL, CONFIRM_RESET_PASSWORD, APPROVALS,
                                        PRODUCTS, ADD_TO_ORDER, GET_ORDER,
                                        UPDATE_ORDER, UPDATE_QUANTITY, CREDIT_CARD,
                                        STORE_DATA, ADDRESS, ADDRESSES, PLACE_ORDER, INVOICE).permitAll())
                .authorizeHttpRequests(authorizeHttpRequests ->
                        authorizeHttpRequests
                                .requestMatchers(LOGIN,
                                        GET_DATA,
                                        EDIT_DATA,
                                        CONTROL_PASSWORD,
                                        CHANGE_PASSWORD,
                                        ADD_ADDRESS,
                                        EDIT_ADDRESS,
                                        DELETE_ADDRESS).authenticated())
                .addFilter(new JwtAuthenticationFilter(manager, authenticationService))
                .addFilter(new JwtAuthorizationFilter(manager, httpRequestService))
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UrlBasedCorsConfigurationSource corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("http://localhost:4200");
        config.addAllowedHeader("*");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("PATCH");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}
