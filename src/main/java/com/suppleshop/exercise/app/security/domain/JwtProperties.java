package com.suppleshop.exercise.app.security.domain;

class JwtProperties {

    private JwtProperties() {
    }

    static final String SECRET_KEY = "SecretKeyToGenJWTs";
    static final int EXPIRATION_TIME = 100_900_000; //15 minutes
    static final String TOKEN_PREFIX = "Bearer ";
    static final String JWT_HEADER = "Authorization";
    static final String EXPOSE_HEADERS = "Access-Control-Expose-Headers";
    static final String ALLOW_HEADERS = "Access-Control-Allow-Headers";
    static final String ALLOW_HEADERS_VALUE = "Authorization, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header";
    static final String ALLOW_HEADERS_VALUE_WITH_ORDER = "Authorization, order, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header";
}
