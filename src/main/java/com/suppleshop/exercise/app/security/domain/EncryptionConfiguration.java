package com.suppleshop.exercise.app.security.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Base64;

import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;

@Configuration
class EncryptionConfiguration {

    @Bean
    public Base64.Decoder decoder() {
        return getDecoder();
    }

    @Bean
    public Base64.Encoder encoder() {
        return getEncoder();
    }
}
