package com.suppleshop.exercise.app.security.domain;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.suppleshop.exercise.app.login.dto.LoginDto;
import com.suppleshop.exercise.app.order.domain.Order;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.shared.exception.AuthenticationUserException;
import org.slf4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.System.currentTimeMillis;
import static org.slf4j.LoggerFactory.getLogger;

class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public static final String ORDER_HEADER = "order";
    private static final Logger LOG = getLogger(JwtAuthenticationFilter.class.getName());

    private final AuthenticationManager authenticationManager;
    private final AuthenticationService authenticationService;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationService authenticationService) {
        this.authenticationManager = authenticationManager;
        this.authenticationService = authenticationService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            LoginDto credentials = getCredentials(request);
            Authentication authenticationToken = getAuthToken(credentials);
            return authenticationManager.authenticate(authenticationToken);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            throw new AuthenticationUserException();
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        final UserPrincipal principal = (UserPrincipal) authResult.getPrincipal();
        final User user = principal.getUser();
        final Order order = authenticationService.decodeOrderFromRequest(request, user);
        adjustResponse(principal, response);
        if (order != null) {
            authenticationService.assignUserToOrder(user, order);
            response.addHeader(JwtProperties.ALLOW_HEADERS, JwtProperties.ALLOW_HEADERS_VALUE_WITH_ORDER);
            response.addHeader(JwtProperties.EXPOSE_HEADERS, ORDER_HEADER);
            response.addHeader(ORDER_HEADER, authenticationService.encodeOrderId(order.getId()));
        }
    }

    private void adjustResponse(UserPrincipal principal, HttpServletResponse response) {
        final String token = createJWToken(principal);
        response.addHeader(JwtProperties.EXPOSE_HEADERS, JwtProperties.JWT_HEADER);
        response.addHeader(JwtProperties.ALLOW_HEADERS, JwtProperties.ALLOW_HEADERS_VALUE);
        response.addHeader(JwtProperties.JWT_HEADER, JwtProperties.TOKEN_PREFIX + token);
    }

    private LoginDto getCredentials(HttpServletRequest request) throws IOException {
        return new ObjectMapper().readValue(request.getInputStream(), LoginDto.class);
    }

    private Authentication getAuthToken(LoginDto credentials) {
        return new UsernamePasswordAuthenticationToken(
                credentials.getEmail(),
                credentials.getPassword(),
                new ArrayList<>());
    }

    private String createJWToken(UserPrincipal principal) {
        return JWT.create()
                .withSubject(principal.getUsername())
                .withExpiresAt(new Date(currentTimeMillis() + JwtProperties.EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(JwtProperties.SECRET_KEY.getBytes()));
    }
}
