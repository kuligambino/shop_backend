package com.suppleshop.exercise.app.security.domain;

import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.shared.exception.AuthenticationUserException;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        try {
            final User user = userRepository.findByEmail(username).orElseThrow(UsernameNotExistsException::new);
            return new UserPrincipal(user);
        } catch (UsernameNotExistsException ex) {
            throw new AuthenticationUserException();
        }
    }
}
