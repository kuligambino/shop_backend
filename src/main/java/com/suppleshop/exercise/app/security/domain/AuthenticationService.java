package com.suppleshop.exercise.app.security.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.order.domain.Order;
import com.suppleshop.exercise.app.order.domain.OrderRepository;
import com.suppleshop.exercise.app.products.domain.Product;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.shared.exception.OrderException;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.suppleshop.exercise.app.order.domain.OrderStatus.OPEN;
import static java.math.BigDecimal.ZERO;

@Service
class AuthenticationService {

    private final EncryptionService encryptionService;
    private final HttpRequestService httpRequestService;
    private final OrderRepository orderRepository;

    public AuthenticationService(EncryptionService encryptionService, HttpRequestService httpRequestService, OrderRepository orderRepository) {
        this.encryptionService = encryptionService;
        this.httpRequestService = httpRequestService;
        this.orderRepository = orderRepository;
    }

    public Order decodeOrderFromRequest(final HttpServletRequest request, final User user) {
        Order order;
        try {
            order = httpRequestService.getOrder(request);
            if (checkIfUserHasOpenedOrder(user)) {
                deleteNewOrder(order);
                return mergeOrders(user, order);
            } else {
                return order;
            }
        } catch (OrderException ex) {
            final Optional<Order> openedOrder = orderRepository.findByUserIdAndStatus(user.getId(), OPEN);
            return openedOrder.flatMap(oOrder -> openedOrder).orElse(null);
        }
    }

    public void assignUserToOrder(final User user, final Order order) {
        if (user != null && order != null) {
            order.setAmount(countOrderAmount(order.getProducts()));
            order.setUser(user);
            orderRepository.save(order);
        }
    }

    public String encodeOrderId(final Long orderId) {
        return encryptionService.encodeId(orderId);
    }

    private Order mergeOrders(final User user, final Order newOrder) {
        final Optional<Order> openedOrder = user.getOrders().stream()
                .filter(order -> order.getStatus() == OPEN)
                .findFirst();
        if (openedOrder.isPresent()) {
            final Order order = openedOrder.get();
            order.getProducts().addAll(newOrder.getProducts());
            return order;
        }
        return newOrder;
    }

    private boolean checkIfUserHasOpenedOrder(final User user) {
        final Set<Order> orders = user.getOrders();
        return orders.stream().anyMatch(order -> order.getStatus() == OPEN);
    }

    private void deleteNewOrder(final Order newOrder) {
        if (newOrder != null) {
            orderRepository.delete(newOrder);
        }
    }

    private BigDecimal countOrderAmount(final List<Product> products) {
        return products.stream()
                .map(Product::getPrice)
                .reduce(ZERO, BigDecimal::add);
    }
}
