package com.suppleshop.exercise.app.security.domain;

import org.springframework.stereotype.Component;

@Component
public class EncryptionFacade {

    private final EncryptionService encryptionService;

    public EncryptionFacade(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    public Long decodeId(final String id) {
        return this.encryptionService.decodeId(id);
    }

    public String encodeId(final Long id) {
        return this.encryptionService.encodeId(id);
    }
}
