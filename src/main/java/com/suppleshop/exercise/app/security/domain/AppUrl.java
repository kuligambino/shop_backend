package com.suppleshop.exercise.app.security.domain;

class AppUrl {

    private AppUrl() {
    }

     static final String CONTACT = "/contact";
     static final String REGISTER = "/register";
     static final String REGISTER_EMAIL = "/register/email-exists";
     static final String CREDIT_CARD = "/credit-card";
     static final String CONFIRM_REGISTER = "/register/confirm";
     static final String LOGIN = "/login";
     static final String FORGET_PASSWORD_ALL = "password/forget/*";
     static final String CONFIRM_RESET_PASSWORD = "/password/reset/confirm";
     static final String APPROVALS = "/approvals";
     static final String PRODUCTS = "/products";
     static final String EDIT_DATA = "/edit-data";
     static final String GET_DATA = "/user-data";
     static final String CONTROL_PASSWORD = "/control-password";
     static final String CHANGE_PASSWORD = "/change-password";
     static final String ADD_TO_ORDER = "/order/add";
     static final String GET_ORDER = "/order";
     static final String GET_ORDER_STATUS = "/order/status";
     static final String GET_ORDER_QUANTITY = "/order/products/quantity";
     static final String UPDATE_ORDER = "/order/update";
     static final String UPDATE_QUANTITY = "/order/update-quantity";
     static final String STORE_DATA = "/store-data";
     static final String ADDRESS = "/address";
     static final String ADDRESSES = "/addresses";
     static final String ADD_ADDRESS = "/add-address";
     static final String EDIT_ADDRESS = "/edit-address";
     static final String DELETE_ADDRESS = "/delete-address";
     static final String PLACE_ORDER = "/place-order";
     static final String INVOICE = "/invoice";
}
