package com.suppleshop.exercise.app.security.domain;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import com.suppleshop.exercise.app.register.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.suppleshop.exercise.app.security.domain.JwtProperties.JWT_HEADER;
import static com.suppleshop.exercise.app.security.domain.JwtProperties.TOKEN_PREFIX;
import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.AUTHORIZATION;

class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private static final Logger LOG = LoggerFactory.getLogger(JwtAuthorizationFilter.class.getName());

    private final HttpRequestService httpRequestService;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, HttpRequestService httpRequestService) {
        super(authenticationManager);
        this.httpRequestService = httpRequestService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String jwToken = request.getHeader(JWT_HEADER);

        if (jwToken != null && jwToken.startsWith(TOKEN_PREFIX)) {
            try {
                UsernamePasswordAuthenticationToken authenticationToken = getUsernamePasswordAuthentication(request);
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } catch (JWTDecodeException | UsernameNotExistsException | TokenExpiredException ex) {
                LOG.error(AUTHORIZATION);
            }
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getUsernamePasswordAuthentication(HttpServletRequest request) {
        final UserPrincipal principal = getUsersPrincipal(request);
        return new UsernamePasswordAuthenticationToken(principal.getUser().getEmail(), null, principal.getAuthorities());
    }

    private UserPrincipal getUsersPrincipal(HttpServletRequest request) {
        final User user = httpRequestService.getUser(request);
        return new UserPrincipal(user);
    }
}

