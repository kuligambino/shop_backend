package com.suppleshop.exercise.app.security.domain;

import org.springframework.stereotype.Service;

import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import static java.lang.Long.parseLong;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
class EncryptionService {

    private final Decoder decoder;
    private final Encoder encoder;

    public EncryptionService(final Decoder decoder, final Encoder encoder) {
        this.decoder = decoder;
        this.encoder = encoder;
    }

    public Long decodeId(final String id) {
        if (isEmpty(id)) {
            return null;
        }
        final byte[] idBytes = decoder.decode(id);
        final String decodedId = new String(idBytes);
        return parseLong(decodedId);
    }

    public String encodeId(final Long id) {
        if (id == null || isEmpty(id.toString())) {
            return null;
        }
        final byte[] idBytes = id.toString().getBytes();
        return encoder.encodeToString(idBytes);
    }
}
