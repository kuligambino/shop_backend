package com.suppleshop.exercise.app.personal_data.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.user_address.dto.UserDto;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;

import static com.suppleshop.exercise.app.personal_data.domain.UserMapper.toDto;

@Service
class PersonalDataService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final HttpRequestService httpRequestService;

    public PersonalDataService(UserRepository userRepository, PasswordEncoder passwordEncoder, HttpRequestService httpRequestService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.httpRequestService = httpRequestService;
    }

    public void editPersonalData(final UserDto personalData) {
        userRepository.updateUser(
                personalData.getFirstName(),
                personalData.getLastName(),
                personalData.getBirthDate(),
                personalData.getPhoneNumber(),
                personalData.getId());
    }

    public UserDto getUserData(final HttpServletRequest request) {
        final User user = httpRequestService.getUser(request);
        return toDto(user);
    }

    public boolean compareOldPasswords(final String email, final String oldPassword) {
        final User matchedUser = userRepository.findByEmail(email)
                .orElseThrow(UsernameNotExistsException::new);
        return passwordEncoder.matches(oldPassword, matchedUser.getPassword());
    }

    public void changeUsersPassword(final HttpServletRequest request, final String newPassword) {
        final String encodedPassword = passwordEncoder.encode(newPassword);
        final User user = httpRequestService.getUser(request);
        userRepository.changePassword(encodedPassword, user.getId());
    }
}
