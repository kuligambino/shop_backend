package com.suppleshop.exercise.app.personal_data.domain;

import com.suppleshop.exercise.app.user_address.dto.UserDto;
import com.suppleshop.exercise.app.register.domain.User;

class UserMapper {

    private UserMapper() {
    }

    public static UserDto toDto(final User user) {
        return new UserDto.UserDtoBuilder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .birthDate(user.getBirthDate())
                .phoneNumber(user.getPhoneNumber())
                .build();
    }
}
