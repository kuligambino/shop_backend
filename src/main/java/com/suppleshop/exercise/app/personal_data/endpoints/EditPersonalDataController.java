package com.suppleshop.exercise.app.personal_data.endpoints;

import com.suppleshop.exercise.app.personal_data.domain.PersonalDataFacade;
import com.suppleshop.exercise.app.user_address.dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
class EditPersonalDataController {

    private final PersonalDataFacade personalDataFacade;

    public EditPersonalDataController(PersonalDataFacade personalDataFacade) {
        this.personalDataFacade = personalDataFacade;
    }

    @GetMapping("user-data")
    public ResponseEntity<UserDto> getUserPersonalData(final HttpServletRequest request) {
        final UserDto user = getUserDataFromJwToken(request);
        return status(OK).body(user);
    }

    @PatchMapping("edit-user")
    public ResponseEntity<Void> editPersonalData(@RequestBody UserDto user) {
        personalDataFacade.editPersonalData(user);
        return status(NO_CONTENT).build();
    }

    @PostMapping("/control-password")
    public ResponseEntity<Boolean> checkIfOldPasswordIsTheSame(final HttpServletRequest request, @RequestBody(required = false) String oldPassword) {
        final boolean isOldPasswordSame;
        if (oldPassword == null) {
            isOldPasswordSame = false;
        } else {
            final UserDto user = getUserDataFromJwToken(request);
            isOldPasswordSame = personalDataFacade.compareOldPasswords(user.getEmail(), oldPassword);
        }
        return status(OK).body(isOldPasswordSame);
    }

    @PatchMapping("/change-password")
    public ResponseEntity<Void> changeUsersPassword(final HttpServletRequest request, @RequestBody String newPassword) {
        personalDataFacade.changeUsersPassword(request, newPassword);
        return status(OK).build();
    }

    private UserDto getUserDataFromJwToken(final HttpServletRequest request) {
        return personalDataFacade.getUserData(request);
    }
}
