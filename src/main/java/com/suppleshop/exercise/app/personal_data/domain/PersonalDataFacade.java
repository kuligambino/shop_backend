package com.suppleshop.exercise.app.personal_data.domain;

import com.suppleshop.exercise.app.user_address.dto.UserDto;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;

@Component
public class PersonalDataFacade {

    private final PersonalDataService personalDataService;

    public PersonalDataFacade(PersonalDataService personalDataService) {
        this.personalDataService = personalDataService;
    }

    public void editPersonalData(final UserDto personalData) {
        this.personalDataService.editPersonalData(personalData);
    }

    public UserDto getUserData(final HttpServletRequest request) {
        return this.personalDataService.getUserData(request);
    }

    public boolean compareOldPasswords(final String email, final String oldPassword) {
        return this.personalDataService.compareOldPasswords(email, oldPassword);
    }

    public void changeUsersPassword(final HttpServletRequest request, final String newPassword) {
        this.personalDataService.changeUsersPassword(request, newPassword);
    }
}
