package com.suppleshop.exercise.app.user_address.endpoints;

import com.suppleshop.exercise.app.user_address.domain.UserAddressFacade;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
class UserAddressController {

    private final UserAddressFacade userAddressFacade;

    public UserAddressController(UserAddressFacade userAddressFacade) {
        this.userAddressFacade = userAddressFacade;
    }

    @GetMapping("/user-addresses")
    public ResponseEntity<Set<AddressDto>> getUserAddresses(final HttpServletRequest request) {
        final Set<AddressDto> addresses = userAddressFacade.getAddresses(request);
        return status(OK).body(addresses);
    }

    @PostMapping("/add-user-address")
    public ResponseEntity<AddressDto> addAddress(final @RequestBody AddressDto address, final HttpServletRequest request) {
        final AddressDto newAddress = userAddressFacade.addAddressToUser(address, request);
        return status(CREATED).body(newAddress);
    }
}
