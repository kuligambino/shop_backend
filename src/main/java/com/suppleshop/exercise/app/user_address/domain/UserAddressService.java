package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.http_request.impl.HttpRequestService;
import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import com.suppleshop.exercise.app.order.domain.Address;
import com.suppleshop.exercise.app.order.domain.AddressRepository;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Set;

import static com.suppleshop.exercise.app.user_address.domain.UserAddressMapper.*;
import static com.suppleshop.exercise.app.user_address.domain.UserAddressHelper.updateUserAddress;
import static java.util.Collections.emptySet;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

@Service
class UserAddressService {

    private final HttpRequestService httpRequestService;
    private final AddressRepository addressRepository;
    private final UserRepository userRepository;

    public UserAddressService(HttpRequestService httpRequestService, AddressRepository addressRepository, UserRepository userRepository) {
        this.httpRequestService = httpRequestService;
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
    }

    public Set<AddressDto> getAddresses(HttpServletRequest request) {
        try {
            final User user = httpRequestService.getUser(request);
            return toAddressUserCollectionDto(user.getAddresses());
        } catch (UsernameNotExistsException ex) {
            return emptySet();
        }
    }

    public AddressDto addAddressToUser(final AddressDto newUserAddress, final HttpServletRequest request) {
        final User user = httpRequestService.getUser(request);
        AddressDto addressUserDto;
        if (newUserAddress.getId() != null) {
            addressUserDto = editAddress(newUserAddress);
        } else {
            final Address address = addressUserToEntity(newUserAddress, user);
            addNewAddressToUser(address, user);
            addressUserDto = addressToAddressUserDto(address);
        }
        return addressUserDto;
    }

    private AddressDto editAddress(final AddressDto updatedAddress) {
        final Optional<Address> addressToUpdate = addressRepository.findById(updatedAddress.getId());
        return addressToUpdate.flatMap(address -> addressToUpdate)
                .map(addressFromDb -> updateUserAddress(addressFromDb, updatedAddress))
                .map(addressRepository::save)
                .map(UserAddressMapper::addressToAddressUserDto)
                .orElseThrow();
    }

    private void addNewAddressToUser(final Address address, final User user) {
        final Set<Address> addresses = user.getAddresses();
        if (isFalse(addresses.contains(address))) {
            addresses.add(address);
            addressRepository.save(address);
            userRepository.save(user);
        }
    }
}
