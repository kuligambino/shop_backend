package com.suppleshop.exercise.app.user_address.dto;

import java.util.Objects;

public class AddressDto {
    private Long id;
    private String street;
    private String city;
    private String postCode;
    private String country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressDto that = (AddressDto) o;
        return Objects.equals(id, that.id) && Objects.equals(street, that.street) && Objects.equals(city, that.city) && Objects.equals(postCode, that.postCode) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, street, city, postCode, country);
    }

    public static AddressUserDtoBuilder addressUserDtoBuilder() {
        return new AddressUserDtoBuilder();
    }

    public static final class AddressUserDtoBuilder {
        private Long id;
        private String street;
        private String city;
        private String postCode;
        private String country;

        public AddressUserDtoBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AddressUserDtoBuilder street(String street) {
            this.street = street;
            return this;
        }

        public AddressUserDtoBuilder city(String city) {
            this.city = city;
            return this;
        }

        public AddressUserDtoBuilder postCode(String postCode) {
            this.postCode = postCode;
            return this;
        }

        public AddressUserDtoBuilder country(String country) {
            this.country = country;
            return this;
        }

        public AddressDto build() {
            AddressDto addressUserDto = new AddressDto();
            addressUserDto.setId(id);
            addressUserDto.setStreet(street);
            addressUserDto.setCity(city);
            addressUserDto.setPostCode(postCode);
            addressUserDto.setCountry(country);
            return addressUserDto;
        }
    }
}
