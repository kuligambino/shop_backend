package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import com.suppleshop.exercise.app.order.domain.Address;

class UserAddressHelper {

    private UserAddressHelper() {
    }

    public static Address updateUserAddress(final Address address, final AddressDto addressDto) {
        address.setStreet(addressDto.getStreet());
        address.setCity(addressDto.getCity());
        address.setPostCode(addressDto.getPostCode());
        address.setCountry(addressDto.getCountry());
        return address;
    }
}
