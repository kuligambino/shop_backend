package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserAddressFacade {

    private final UserAddressService userAddressService;

    public UserAddressFacade(UserAddressService userAddressService) {
        this.userAddressService = userAddressService;
    }

    public AddressDto addAddressToUser(final AddressDto newUserAddress, final HttpServletRequest request) {
        return this.userAddressService.addAddressToUser(newUserAddress, request);
    }

    public Set<AddressDto> getAddresses(HttpServletRequest request) {
        return this.userAddressService.getAddresses(request);
    }
}
