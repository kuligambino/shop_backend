package com.suppleshop.exercise.app.user_address.domain;

import com.suppleshop.exercise.app.user_address.dto.AddressDto;
import com.suppleshop.exercise.app.order.domain.Address;
import com.suppleshop.exercise.app.register.domain.User;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

class UserAddressMapper {

    private UserAddressMapper() {
    }

    public static Set<AddressDto> toAddressUserCollectionDto(Set<Address> addresses) {
        return addresses.stream()
                .map(UserAddressMapper::addressToAddressUserDto)
                .collect(toSet());
    }

    public static Address addressUserToEntity(final AddressDto addressUserDto, final User user) {
        final Address address = new Address();
        address.setId(addressUserDto.getId());
        address.setFirstName(user.getFirstName());
        address.setLastName(user.getLastName());
        address.setEmail(user.getEmail());
        address.setPhoneNumber(user.getPhoneNumber());
        address.setStreet(addressUserDto.getStreet());
        address.setCity(addressUserDto.getCity());
        address.setPostCode(addressUserDto.getPostCode());
        address.setCountry(addressUserDto.getCountry());
        address.setMain(false);
        return address;
    }

    public static AddressDto addressToAddressUserDto(final Address address) {
        return AddressDto.addressUserDtoBuilder()
                .id(address.getId())
                .street(address.getStreet())
                .city(address.getCity())
                .postCode(address.getPostCode())
                .country(address.getCountry())
                .build();
    }
}
