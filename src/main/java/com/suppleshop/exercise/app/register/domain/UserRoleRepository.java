package com.suppleshop.exercise.app.register.domain;

import org.springframework.data.jpa.repository.JpaRepository;

interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    /**
     * Method looks for user role defined by {@link Role}.
     *
     * @param role
     * @return user role
     */
    UserRole findByRole(Role role);
}
