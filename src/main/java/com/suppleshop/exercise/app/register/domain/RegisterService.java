package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.email.domain.EmailFacade;
import com.suppleshop.exercise.app.register.dto.RegisterDto;
import com.suppleshop.exercise.app.shared.exception.TokenIsNotValidException;
import com.suppleshop.exercise.app.token.domain.UserToken;
import com.suppleshop.exercise.app.token.domain.UserTokenFacade;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
class RegisterService {

    private static final String EMAIL_MESSAGE = "Kliknij w link potwierdzający: ";
    private static final String SUBJECT = "Potwierdź rejestrację: ";
    private static final String REGISTER_WITH_TOKEN = "http://localhost:4200/register/confirm?token=";

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final RegisterMapper registerMapper;
    private final PasswordEncoder passwordEncoder;
    private final UserTokenFacade userTokenFacade;
    private final EmailFacade emailFacade;
    private final RegisterSpecificationFactory registerSpecificationFactory;

    public RegisterService(UserRepository userRepository,
                           UserRoleRepository userRoleRepository,
                           RegisterMapper registerMapper,
                           PasswordEncoder passwordEncoder,
                           UserTokenFacade userTokenFacade,
                           EmailFacade emailFacade,
                           RegisterSpecificationFactory registerSpecificationFactory) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.registerMapper = registerMapper;
        this.passwordEncoder = passwordEncoder;
        this.userTokenFacade = userTokenFacade;
        this.emailFacade = emailFacade;
        this.registerSpecificationFactory = registerSpecificationFactory;
    }

    /**
     * Method adds user to db and return register form.
     *
     * @param registerDto register
     * @return added to db register form
     */

    public RegisterDto registerUser(final RegisterDto registerDto) {
        boolean isValidated = registerSpecificationFactory.defineSpecification().isFulfilledBy(registerDto);
        if (isValidated) {
            final User user = registerMapper.convertToUser(registerDto);
            final UserRole userRole = userRoleRepository.findByRole(Role.USER);
            user.getRoles().add(userRole);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setEnabled(false);
            user.setAnonymous(false);
            userRepository.save(user);
            final UserToken userToken = userTokenFacade.generateVerificationToken(user);
            emailFacade.sendEmail(user.getEmail(), SUBJECT, EMAIL_MESSAGE + REGISTER_WITH_TOKEN + userToken.getToken());
            return registerDto;
        } else {
            return null;
        }
    }

    /**
     * Method confirms registration by changing enabled value to true.
     *
     * @param token token
     */

    public void confirmRegistration(final String token) {
        final UserToken userToken = userTokenFacade.findByToken(token);
        if (!userTokenFacade.checkIfValid(userToken)) {
            throw new TokenIsNotValidException();
        } else {
            final User user = userToken.getUser();
            user.setEnabled(true);
            userToken.setValid(false);
            userRepository.save(user);
        }
    }

    /**
     * Method search for user with the same email.
     *
     * @param email email
     * @return if user with this email exists in db
     */

    public boolean isEmailAlreadyInUse(final String email) {
        return userRepository.findByEmail(email).isPresent();
    }
}

