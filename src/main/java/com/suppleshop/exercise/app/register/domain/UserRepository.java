package com.suppleshop.exercise.app.register.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Looks for user in db using email.
     *
     * @param email
     * @return user wrapped in optional.
     */
    Optional<User> findByEmail(String email);

    /**
     * Looks for users email who approved sending newsletter every week.
     *
     * @return list of emails with approved newsletter.
     */
    @Query(value = "SELECT U.email FROM _user AS U\n" +
            "    JOIN _user_approvals AS UA\n" +
            "    ON U.id_user = UA.user_id\n" +
            "    JOIN _approval AS A\n" +
            "    ON UA.approval_id = A.id_approval\n" +
            "    WHERE A.approval_type = 'NEWSLETTER'", nativeQuery = true)
    List<String> getAllEmailsWithAcceptedNewsletter();

    /**
     * Update users personal data (first, last name, birth date, phone number)
     *
     * @param firstName
     * @param lastName
     * @param birthDate
     * @param phoneNumber
     * @param id
     */
    @Transactional
    @Modifying
    @Query("UPDATE User u " +
            "SET u.firstName = :firstName," +
            " u.lastName=:lastName," +
            " u.birthDate = :birthDate," +
            " u.phoneNumber = :phoneNumber" +
            " WHERE u.id = :userId")
    void updateUser(@Param("firstName") String firstName,
                    @Param("lastName") String lastName,
                    @Param("birthDate") LocalDate birthDate,
                    @Param("phoneNumber") String phoneNumber,
                    @Param("userId") Long id);

    /**
     * Change users password
     *
     * @param newPassword
     * @param userId
     */
    @Transactional
    @Modifying
    @Query("UPDATE User u " +
            "SET u.password = :password " +
            "WHERE u.id = :userId")
    void changePassword(@Param("password") String newPassword,
                        @Param("userId") Long userId);
}
