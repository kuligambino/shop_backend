package com.suppleshop.exercise.app.register.dto;

public class ApprovalDto {
    private Long id;
    private String type;
    private String title;
    private String description;
    private boolean required;

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }

    public static ApprovalDtoBuilder builder() {
        return new ApprovalDtoBuilder();
    }

    public static final class ApprovalDtoBuilder {
        private Long id;
        private String type;
        private String title;
        private String description;
        private boolean required;

        private ApprovalDtoBuilder() {
        }

        public ApprovalDtoBuilder id(final Long id) {
            this.id = id;
            return this;
        }

        public ApprovalDtoBuilder type(final String type) {
            this.type = type;
            return this;
        }

        public ApprovalDtoBuilder title(final String title) {
            this.title = title;
            return this;
        }

        public ApprovalDtoBuilder description(final String description) {
            this.description = description;
            return this;
        }

        public ApprovalDtoBuilder required(boolean required) {
            this.required = required;
            return this;
        }

        public ApprovalDto build() {
            final ApprovalDto approvalDto = new ApprovalDto();
            approvalDto.description = this.description;
            approvalDto.type = this.type;
            approvalDto.required = this.required;
            approvalDto.id = this.id;
            approvalDto.title = this.title;
            return approvalDto;
        }
    }
}
