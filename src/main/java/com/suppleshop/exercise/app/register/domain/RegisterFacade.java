package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.RegisterDto;
import org.springframework.stereotype.Component;

@Component
public class RegisterFacade {

    private final RegisterService registerService;

    public RegisterFacade(RegisterService registerService) {
        this.registerService = registerService;
    }

    public RegisterDto registerUser(final RegisterDto registerDto) {
        return this.registerService.registerUser(registerDto);
    }

    public void confirmRegistration(final String token) {
        this.registerService.confirmRegistration(token);
    }

    public boolean isEmailAlreadyInUse(final String email) {
        return this.registerService.isEmailAlreadyInUse(email);
    }
}
