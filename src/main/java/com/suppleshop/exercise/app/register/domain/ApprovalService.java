package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.ApprovalDto;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Service manages all operations related to {@link Approval}
 */
@Service
class ApprovalService {

    private final ApprovalRepository approvalRepository;

    public ApprovalService(ApprovalRepository approvalRepository) {
        this.approvalRepository = approvalRepository;
    }

    public List<ApprovalDto> getAllApprovals() {
        return approvalRepository.findAll().stream()
                .map(ApprovalMapper::toDto)
                .collect(toList());
    }
}
