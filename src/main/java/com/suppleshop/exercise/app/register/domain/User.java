package com.suppleshop.exercise.app.register.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.suppleshop.exercise.app.order.domain.Address;
import com.suppleshop.exercise.app.order.domain.Order;
import com.suppleshop.exercise.app.shared.AbstractEntity;
import com.suppleshop.exercise.app.token.domain.UserToken;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static jakarta.persistence.FetchType.EAGER;

@Entity
public class User extends AbstractEntity {

    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "phone_number", length = 9)
    private String phoneNumber;
    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;
    @Column(name = "is_enabled", nullable = false)
    private boolean isEnabled;
    @Column(name = "anonymous", nullable = false)
    private boolean isAnonymous;

    @JsonIgnore
    @ManyToMany(fetch = EAGER)
    private Set<Address> addresses = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = EAGER)
    private Set<UserRole> roles = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    private Set<UserToken> tokens = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = EAGER)
    private Set<Approval> approvals = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = EAGER)
    private Set<Order> orders = new HashSet<>();

    public void removeAddress(Long id) {
        this.addresses = this.addresses.stream()
                .filter(address -> !address.getId().equals(id))
                .collect(Collectors.toSet());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate bornDate) {
        this.birthDate = bornDate;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public Set<UserToken> getTokens() {
        return tokens;
    }

    public void setTokens(Set<UserToken> tokens) {
        this.tokens = tokens;
    }

    public Set<Approval> getApprovals() {
        return approvals;
    }

    public void setApprovals(Set<Approval> approvals) {
        this.approvals = approvals;
    }

    public boolean isAnonymous() {
        return isAnonymous;
    }

    public void setAnonymous(boolean anonymous) {
        isAnonymous = anonymous;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
}
