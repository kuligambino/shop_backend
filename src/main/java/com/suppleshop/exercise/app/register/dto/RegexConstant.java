package com.suppleshop.exercise.app.register.dto;

public class RegexConstant {

    public static final String PASSWORD = "(?=^.{8,}$)(?=.*[A-Z])(?=.*[!@#$%^;*()_+}{;:;\\'?;.&;,])(?!.*\\s).*$";

    private RegexConstant() {
    }
}
