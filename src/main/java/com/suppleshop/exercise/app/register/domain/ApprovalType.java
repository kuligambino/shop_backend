package com.suppleshop.exercise.app.register.domain;

/**
 * It defines all approval types
 */
enum ApprovalType {
    REGULATIONS,
    PERSONAL_DATA_PROCESSING,
    ADVERTS,
    NEWSLETTER;
}
