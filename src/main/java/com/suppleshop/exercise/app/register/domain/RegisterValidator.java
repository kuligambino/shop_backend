package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.RegisterDto;
import com.suppleshop.exercise.app.shared.BaseSpec;
import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static java.time.LocalDate.now;
import static java.time.Period.between;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
class RegisterValidator implements BaseSpec<RegisterDto> {

    private final List<BaseSpec<RegisterDto>> specifications;

    public RegisterValidator(List<BaseSpec<RegisterDto>> specifications) {
        this.specifications = specifications;
    }

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        return specifications.stream().allMatch(spec -> spec.isFulfilledBy(registerDto));
    }
}

class NameSpec implements BaseSpec<RegisterDto> {

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        final String firstName = registerDto.getFirstName();
        final String lastName = registerDto.getFirstName();

        final boolean namesAreNotDefined = isBlank(firstName) || isBlank(lastName);

        if (namesAreNotDefined) {
            throw new UnsatisfiedSpecificationException("Names must be defined!");
        }
        return true;
    }
}

@Component
class EmailSpec implements BaseSpec<RegisterDto> {

    private final UserRepository userRepository;
    private final Environment env;

    public EmailSpec(UserRepository userRepository, Environment env) {
        this.userRepository = userRepository;
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        final String email = registerDto.getEmail();

        final boolean emailIsNotDefined = isBlank(email);

        if (emailIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Email is not defined!");
        }

        final boolean emailHasWrongFormat = !email.matches(requireNonNull(env.getProperty("email.regex")));

        if (emailHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Email has wrong format!");
        }

        final boolean emailIsAlreadyUsed = userRepository.findByEmail(email).isPresent();

        if (emailIsAlreadyUsed) {
            throw new UnsatisfiedSpecificationException("Email is already used!");
        }

        return true;
    }
}

class BirthDateSpec implements BaseSpec<RegisterDto> {

    private static final int ACCEPTED_AGE = 18;

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        final LocalDate birthdate = registerDto.getBirthDate();

        final boolean birthDateIsNotDefined = birthdate == null;

        if (birthDateIsNotDefined) {
            throw new UnsatisfiedSpecificationException("Birthdate is not defined");
        }

        final boolean isBelowAcceptedAge = between(birthdate, now()).getYears() < ACCEPTED_AGE;

        if (isBelowAcceptedAge) {
            throw new UnsatisfiedSpecificationException("Birthdate must be above 18!");
        }

        return true;
    }
}

@Component
class PhoneNumberSpec implements BaseSpec<RegisterDto> {

    private final Environment env;

    public PhoneNumberSpec(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        final String phoneNumber = registerDto.getPhoneNumber();

        final boolean phoneNumberHasWrongFormat = phoneNumber != null && !phoneNumber.matches(requireNonNull(env.getProperty("phoneNumber.regex")));

        if (phoneNumberHasWrongFormat) {
            throw new UnsatisfiedSpecificationException("Phone number has wrong format!");
        }

        return true;
    }
}

class PasswordSpec implements BaseSpec<RegisterDto> {

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {

        final boolean passwordsAreNotDefined = isBlank(registerDto.getPassword()) || isBlank(registerDto.getConfirmedPassword());

        if (passwordsAreNotDefined) {
            throw new UnsatisfiedSpecificationException("Passwords are not defined");
        }

        final boolean passwordsAreDifferent = !registerDto.getPassword().equals(registerDto.getConfirmedPassword());

        if (passwordsAreDifferent) {
            throw new UnsatisfiedSpecificationException("Passwords must be same!");
        }

        return true;
    }
}

@Component
class ApprovalSpec implements BaseSpec<RegisterDto> {

    private final ApprovalRepository approvalRepository;

    public ApprovalSpec(ApprovalRepository approvalRepository) {
        this.approvalRepository = approvalRepository;
    }

    @Override
    public boolean isFulfilledBy(RegisterDto registerDto) throws UnsatisfiedSpecificationException {
        final List<String> approvals = registerDto.getApprovals();

        final boolean approvalsAreNotDefined = isEmpty(approvals);

        if (approvalsAreNotDefined) {
            throw new UnsatisfiedSpecificationException("Approvals are not defined!");
        }

        final List<String> requiredApprovals = approvalRepository.findAllByIsRequiredIsTrue().stream()
                .map(Approval::getType)
                .map(Enum::toString)
                .collect(toList());

        final boolean areApprovalsNotAccepted = !approvals.containsAll(requiredApprovals);
        if (areApprovalsNotAccepted) {
            throw new UnsatisfiedSpecificationException("Required approvals must be accepted!");
        }

        return true;
    }
}
