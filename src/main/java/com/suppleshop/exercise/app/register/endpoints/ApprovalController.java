package com.suppleshop.exercise.app.register.endpoints;

import com.suppleshop.exercise.app.register.domain.ApprovalFacade;
import com.suppleshop.exercise.app.register.dto.ApprovalDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

/**
 * Controller handles HTTP request related to Approval.
 */
@RestController
@RequestMapping("/approvals")
class ApprovalController {

    private final ApprovalFacade approvalFacade;

    public ApprovalController(ApprovalFacade approvalFacade) {
        this.approvalFacade = approvalFacade;
    }

    /**
     * Get all approvals required at Register page
     *
     * @return all approvals required at Register page
     */
    @GetMapping
    public ResponseEntity<List<ApprovalDto>> getApprovals() {
        return status(OK).body(approvalFacade.getAllApprovals());
    }
}
