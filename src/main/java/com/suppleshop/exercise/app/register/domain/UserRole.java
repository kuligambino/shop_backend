package com.suppleshop.exercise.app.register.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.suppleshop.exercise.app.shared.AbstractEntity;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static jakarta.persistence.EnumType.STRING;

@Entity
public class UserRole extends AbstractEntity {

    @Column(nullable = false)
    private String description;
    @Column(name = "role_name", nullable = false)
    @Enumerated(STRING)
    private Role role;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
