package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.RegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Component
class RegisterMapper {

    private final ApprovalRepository approvalRepository;

    @Autowired
    public RegisterMapper(ApprovalRepository approvalRepository) {
        this.approvalRepository = approvalRepository;
    }

    public User convertToUser(final RegisterDto registerDto) {
        final User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(registerDto.getPassword());
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setBirthDate(registerDto.getBirthDate());
        user.setApprovals(convertRegisterFormApprovals(registerDto));
        return user;
    }

    private Set<Approval> convertRegisterFormApprovals(final RegisterDto registerDto) {
        final List<String> approvals = registerDto.getApprovals();
        return approvalRepository.findAll().stream()
                .filter(approval -> approvals.contains(approval.getType()))
                .collect(toSet());
    }
}
