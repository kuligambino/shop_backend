package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.ApprovalDto;

import static com.suppleshop.exercise.app.register.dto.ApprovalDto.builder;

/**
 * Defines all methods used to converting {@link Approval} and {@link ApprovalDto}
 */
class ApprovalMapper {

    private ApprovalMapper() {
    }

    /**
     * Converts approval to data transfer object (Dto)
     * @param approval converted object
     * @return Dto converted from entity
     */
    public static ApprovalDto toDto(final Approval approval) {
        return builder()
                .id(approval.getId())
                .type(approval.getType().toString())
                .title(approval.getTitle())
                .description(approval.getDescription())
                .required(approval.isRequired())
                .build();
    }
}
