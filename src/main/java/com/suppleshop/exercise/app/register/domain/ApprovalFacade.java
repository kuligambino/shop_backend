package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.register.dto.ApprovalDto;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Shares methods related to approvals
 */
@Component
public class ApprovalFacade {

    private final ApprovalService approvalService;

    public ApprovalFacade(ApprovalService approvalService) {
        this.approvalService = approvalService;
    }

    /**
     * Get all existing approvals (Types defined in enum "ApprovalType") at register page
     *
     * @return list of all approvals at register page
     */
    public List<ApprovalDto> getAllApprovals() {
        return this.approvalService.getAllApprovals();
    }
}
