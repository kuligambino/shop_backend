package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.shared.AbstractEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;

import static jakarta.persistence.EnumType.STRING;

@Entity
class Approval extends AbstractEntity {

    @Enumerated(STRING)
    @Column(nullable = false, unique = true)
    private ApprovalType type;
    @Column(nullable = false, unique = true)
    private String title;
    @Column(nullable = false, unique = true)
    private String description;
    @Column(nullable = false)
    private boolean isRequired;

    public ApprovalType getType() {
        return type;
    }

    public void setType(ApprovalType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        this.isRequired = required;
    }
}
