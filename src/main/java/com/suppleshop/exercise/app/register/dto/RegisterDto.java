package com.suppleshop.exercise.app.register.dto;

import java.time.LocalDate;
import java.util.List;

public class RegisterDto {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String confirmedPassword;
    private String phoneNumber;
    private LocalDate birthDate;
    private boolean recaptcha;
    private List<String> approvals;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isRecaptcha() {
        return recaptcha;
    }

    public void setRecaptcha(boolean recaptcha) {
        this.recaptcha = recaptcha;
    }

    public List<String> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<String> approvals) {
        this.approvals = approvals;
    }

    public static RegisterDtoBuilder builder() {
        return new RegisterDtoBuilder();
    }

    public static final class RegisterDtoBuilder {

        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private String confirmedPassword;
        private String phoneNumber;
        private LocalDate birthDate;
        private boolean recaptcha;
        private List<String> approvals;

        public static RegisterDtoBuilder builder() {
            return new RegisterDtoBuilder();
        }

        public RegisterDtoBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public RegisterDtoBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public RegisterDtoBuilder email(String email) {
            this.email = email;
            return this;
        }

        public RegisterDtoBuilder password(String password) {
            this.password = password;
            return this;
        }

        public RegisterDtoBuilder confirmedPassword(String confirmedPassword) {
            this.confirmedPassword = confirmedPassword;
            return this;
        }

        public RegisterDtoBuilder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public RegisterDtoBuilder birthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public RegisterDtoBuilder recaptcha(boolean recaptcha) {
            this.recaptcha = recaptcha;
            return this;
        }

        public RegisterDtoBuilder approvals(List<String> approvals) {
            this.approvals = approvals;
            return this;
        }


        public RegisterDto build() {
            RegisterDto registerDto = new RegisterDto();
            registerDto.firstName = this.firstName;
            registerDto.lastName = this.lastName;
            registerDto.email = this.email;
            registerDto.password = this.password;
            registerDto.confirmedPassword = this.confirmedPassword;
            registerDto.phoneNumber = this.phoneNumber;
            registerDto.birthDate = this.birthDate;
            registerDto.recaptcha = this.recaptcha;
            registerDto.approvals = this.approvals;
            return registerDto;
        }
    }
}
