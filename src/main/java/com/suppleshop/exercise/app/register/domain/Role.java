package com.suppleshop.exercise.app.register.domain;

enum Role {

    ADMIN("{admin.description}"), USER("{user.description}");

    private final String description;

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
