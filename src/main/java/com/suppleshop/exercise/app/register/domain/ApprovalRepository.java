package com.suppleshop.exercise.app.register.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for {@link Approval}
 */
interface ApprovalRepository extends JpaRepository<Approval, Long> {

    /**
     * Looks for approvals which have to be approved by user during registration
     *
     * @return all required to accept approvals
     */
    List<Approval> findAllByIsRequiredIsTrue();
}
