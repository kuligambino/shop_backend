package com.suppleshop.exercise.app.register.endpoints;

import com.suppleshop.exercise.app.register.domain.RegisterFacade;
import com.suppleshop.exercise.app.register.dto.RegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/register")
class UserController {

    private final RegisterFacade registerFacade;

    @Autowired
    public UserController(RegisterFacade registerFacade) {
        this.registerFacade = registerFacade;
    }

    @PostMapping
    public ResponseEntity<Void> registerUser(final @RequestBody RegisterDto registerDto) {
        registerFacade.registerUser(registerDto);
        return new ResponseEntity<>(CREATED);
    }

    @GetMapping("/confirm")
    public ResponseEntity<Void> confirmRegistration(final @RequestParam String token) {
        try {
            registerFacade.confirmRegistration(token);
            return new ResponseEntity<>(OK);
        } catch (Exception ex) {
            throw new ResponseStatusException(CONFLICT, ex.getMessage());
        }
    }

    @PostMapping("/email-exists")
    public ResponseEntity<Boolean> checkEmailIfExist(final @RequestBody(required = false) String email) {
        final boolean emailAlreadyInUse = registerFacade.isEmailAlreadyInUse(email);
        return status(OK).body(emailAlreadyInUse);
    }
}
