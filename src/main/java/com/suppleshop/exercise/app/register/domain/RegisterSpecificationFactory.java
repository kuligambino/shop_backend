package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.shared.BaseSpecFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static java.util.List.of;

@Component
class RegisterSpecificationFactory implements BaseSpecFactory<RegisterValidator> {

    private final ApprovalRepository approvalRepository;
    private final UserRepository userRepository;
    private final Environment env;

    public RegisterSpecificationFactory(ApprovalRepository approvalRepository, UserRepository userRepository, Environment env) {
        this.approvalRepository = approvalRepository;
        this.userRepository = userRepository;
        this.env = env;
    }

    @Override
    public RegisterValidator defineSpecification() {
        return new RegisterValidator(of(
                new NameSpec(),
                new EmailSpec(userRepository, env),
                new BirthDateSpec(),
                new PhoneNumberSpec(env),
                new ApprovalSpec(approvalRepository),
                new PasswordSpec()
        ));
    }
}
