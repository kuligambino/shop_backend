package com.suppleshop.exercise.app.register.domain;

import com.suppleshop.exercise.app.email.domain.EmailFacade;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
class UserService {

    /**
     * At 08:00:00 am, on every Monday, every month
     */
    private static final String NEWSLETTER_SEND_TIME = "0 0 8 ? * MON";

    private final UserRepository userRepository;
    private final EmailFacade emailFacade;

    public UserService(UserRepository userRepository, EmailFacade emailFacade) {
        this.userRepository = userRepository;
        this.emailFacade = emailFacade;
    }

    /**
     * Send newsletters at specific date (defined using Cron expression) to users who have approved sending newsletter
     */
    @Scheduled(cron = NEWSLETTER_SEND_TIME)
    public void sendNewsletters() {
        userRepository.getAllEmailsWithAcceptedNewsletter()
                .forEach(email -> emailFacade.sendEmail(email, "Newsletter", "Cotygodniowy newsletter"));
    }
}
