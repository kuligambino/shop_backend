package com.suppleshop.exercise.app.products.domain;

import com.suppleshop.exercise.app.products.dto.ProductDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ProductFacade {

    private final ProductService productService;

    public ProductFacade(ProductService productService) {
        this.productService = productService;
    }

    public Map<String, List<ProductDTO>> getProducts() {
        return this.productService.getProducts();
    }

    public ProductDTO getProduct(final Long id) {
        return this.productService.getProduct(id);
    }
}
