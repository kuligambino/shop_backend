package com.suppleshop.exercise.app.products.endpoints;

import com.suppleshop.exercise.app.products.domain.ProductFacade;
import com.suppleshop.exercise.app.products.dto.ProductDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/products")
class ProductController {

    private final ProductFacade productFacade;

    public ProductController(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    @GetMapping
    public ResponseEntity<Map<String, List<ProductDTO>>> getHomePageProducts() {
        Map<String, List<ProductDTO>> products = productFacade.getProducts();
        return status(OK).body(products);
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ProductDTO> getProduct(final @PathVariable Long id) {
        return status(OK).body(productFacade.getProduct(id));
    }
}
