package com.suppleshop.exercise.app.products.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT * FROM product ORDER BY RAND() LIMIT 10", nativeQuery = true)
    List<Product> get10SpecialProducts();

    List<Product> findTop10ByOrderByPriceAsc();

    List<Product> findTop10ByOrderByPriceDesc();

    Optional<Product> findById(Long id);
}
