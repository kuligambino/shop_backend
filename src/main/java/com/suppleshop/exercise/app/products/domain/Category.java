package com.suppleshop.exercise.app.products.domain;

import com.suppleshop.exercise.app.shared.AbstractEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity
class Category extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String name;
    @Column(nullable = false, length = 1000)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
