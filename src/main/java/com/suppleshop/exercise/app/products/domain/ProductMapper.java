package com.suppleshop.exercise.app.products.domain;

import com.suppleshop.exercise.app.order.domain.Order;
import com.suppleshop.exercise.app.order.dto.OrderProductDTO;
import com.suppleshop.exercise.app.products.dto.ProductDTO;
import com.suppleshop.exercise.app.shared.exception.CategoryNotExistsException;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    private final CategoryRepository categoryRepository;

    public ProductMapper(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Product> convertToProducts(List<OrderProductDTO> orderProducts) {
        final List<Product> products = new ArrayList<>();
        orderProducts
                .forEach(orderProduct -> products.addAll(addToList(convertToProduct(orderProduct), orderProduct.getQuantity())));
        return products;
    }

    public OrderProductDTO convertToOrderProductDTO(Order order, Product product) {
        return OrderProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .weight(product.getWeight())
                .taste(product.getTaste())
                .price(product.getPrice())
                .categoryName(product.getCategory().getName())
                .img(product.getImg())
                .quantity(countQuantityOfProductInOrder(order.getProducts(), product.getId()))
                .build();
    }

    public List<ProductDTO> convertToDTO(final List<Product> products) {
        return products.stream().map(this::toDto).collect(Collectors.toList());
    }

    public ProductDTO toDto(Product product) {
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .weight(product.getWeight())
                .price(product.getPrice())
                .taste(product.getTaste())
                .categoryName(product.getCategory().getName())
                .img(product.getImg())
                .build();
    }

    private List<Product> addToList(Product product, int quantity) {
        return Collections.nCopies(quantity, product);
    }

    private Product convertToProduct(OrderProductDTO orderProduct) {
        final Category category = getCategory(orderProduct);
        Product product = new Product();
        product.setId(orderProduct.getId());
        product.setName(orderProduct.getName());
        product.setWeight(orderProduct.getWeight());
        product.setTaste(orderProduct.getTaste());
        product.setPrice(orderProduct.getPrice());
        product.setCategory(category);
        product.setImg(orderProduct.getImg());
        return product;
    }

    private Category getCategory(OrderProductDTO orderProduct) {
        final Optional<Category> categoryOptional = categoryRepository.findByName(orderProduct.getCategoryName());
        return categoryOptional
                .flatMap(c -> categoryOptional)
                .orElseThrow(CategoryNotExistsException::new);
    }

    private int countQuantityOfProductInOrder(List<Product> products, Long productId) {
        final long quantity = products.stream()
                .filter(product -> product.getId().equals(productId))
                .count();
        return (int) quantity;
    }
}
