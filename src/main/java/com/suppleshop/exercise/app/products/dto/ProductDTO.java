package com.suppleshop.exercise.app.products.dto;

import java.math.BigDecimal;

public class ProductDTO {

    private Long id;
    private String name;
    private int weight;
    private BigDecimal price;
    private String taste;
    private String categoryName;
    private String img;

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getTaste() {
        return taste;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getImg() {
        return img;
    }

    public Long getId() {
        return id;
    }

    public static ProductDtoBuilder builder(){
        return new ProductDtoBuilder();
    }

    public static final class ProductDtoBuilder {
        private Long id;
        private String name;
        private int weight;
        private BigDecimal price;
        private String taste;
        private String categoryName;
        private String img;

        public ProductDtoBuilder id(final Long id) {
            this.id = id;
            return this;
        }

        public ProductDtoBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public ProductDtoBuilder weight(final int weight) {
            this.weight = weight;
            return this;
        }

        public ProductDtoBuilder price(final BigDecimal price) {
            this.price = price;
            return this;
        }

        public ProductDtoBuilder taste(final String taste) {
            this.taste = taste;
            return this;
        }

        public ProductDtoBuilder categoryName(final String categoryName) {
            this.categoryName = categoryName;
            return this;
        }

        public ProductDtoBuilder img(final String img) {
            this.img = img;
            return this;
        }

        public ProductDTO build() {
            final ProductDTO product = new ProductDTO();
            product.id = id;
            product.name = name;
            product.categoryName = categoryName;
            product.price = price;
            product.taste = taste;
            product.weight = weight;
            product.img = img;
            return product;
        }
    }
}
