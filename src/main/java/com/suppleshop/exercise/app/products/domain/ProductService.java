package com.suppleshop.exercise.app.products.domain;

import com.suppleshop.exercise.app.products.dto.ProductDTO;
import com.suppleshop.exercise.app.shared.exception.ProductNotExistsException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
class ProductService {

    private static final String SPECIALS = "Nasze specjały";
    private static final String CHEAPEST = "Najlepsza cena";
    private static final String MOST_EXPENSIVE = "Najwyższa jakość";
    private static final String TASTY = "Najsmaczniejsze";

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public Map<String, List<ProductDTO>> getProducts() {
        final Map<String, List<ProductDTO>> products = new HashMap<>();
        final List<Product> specialProducts = productRepository.get10SpecialProducts();
        final List<Product> deliciousProduct = productRepository.get10SpecialProducts();
        final List<Product> cheapestProducts = productRepository.findTop10ByOrderByPriceAsc();
        final List<Product> highestQualityProducts = productRepository.findTop10ByOrderByPriceDesc();
        products.put(SPECIALS, productMapper.convertToDTO(specialProducts));
        products.put(CHEAPEST, productMapper.convertToDTO(cheapestProducts));
        products.put(MOST_EXPENSIVE, productMapper.convertToDTO(highestQualityProducts));
        products.put(TASTY, productMapper.convertToDTO(deliciousProduct));
        return products;
    }

    public ProductDTO getProduct(final Long id) {
        return productRepository.findById(id)
                .map(productMapper::toDto)
                .orElseThrow(ProductNotExistsException::new);
    }
}
