package com.suppleshop.exercise.app.shared.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static java.time.LocalDateTime.now;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
            BusinessException.class,
            ContactException.class,
            UserAlreadyExistsException.class,
            TokenNotExistException.class,
            ConfirmedPasswordNotEqualsException.class,
            OrderException.class})
    public ResponseEntity<CustomErrorResponse> handleException(Exception ex) {
        return buildResponse(ex, BAD_REQUEST);
    }

    private ResponseEntity<CustomErrorResponse> buildResponse(Exception ex, HttpStatus responseStatus) {
        final CustomErrorResponse response = new CustomErrorResponse();
        response.setMessage(ex.getMessage());
        response.setTimestamp(now().toString());
        response.setHttpStatus(responseStatus.toString());
        return new ResponseEntity<>(response, responseStatus);
    }
}
