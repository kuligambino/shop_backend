package com.suppleshop.exercise.app.shared;

import jakarta.persistence.*;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static jakarta.persistence.GenerationType.IDENTITY;

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    @Version
    private int version;
    @Column(nullable = false)
    private LocalDateTime createTime;
    @Column
    private LocalDateTime updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @PrePersist
    void setCreateTime() {
        createTime = now();
    }

    @PreUpdate
    void setUpdateTime() {
        updateTime = now();
    }
}
