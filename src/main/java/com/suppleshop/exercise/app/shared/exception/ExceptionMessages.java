package com.suppleshop.exercise.app.shared.exception;

public class ExceptionMessages {

    private ExceptionMessages() {
    }

    public static final String BAD_CREDENTIALS = "Niepoprawny login lub hasło!";
    public static final String AUTHORIZATION = "Problem z autoryzacją!";
    public static final String PASSWORDS_NOT_EQUAL = "Hasła muszą być takie same!";
    public static final String TOKEN_NOT_ACTIVE = "Token jest nieaktywny!";
    public static final String TOKEN_NOT_EXISTS = "Taki token nie istnieję!";
    public static final String USER_NOT_EXISTS = "Nie ma takiego użytkownika!";
    public static final String USER_ALREADY_EXISTS = "Taki użytkownik już istnieję!";
    public static final String RECAPTCHA_TRUE = "Recaptcha musi być zaznaczona!";
    public static final String APPROVALS_NOT_ACCEPTED = "Trzeba zaakceptować wszystkie wymagane zgody!";
}
