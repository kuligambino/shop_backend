package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.TOKEN_NOT_ACTIVE;

public class TokenIsNotValidException extends RuntimeException {

    public TokenIsNotValidException() {
        super(TOKEN_NOT_ACTIVE);
    }
}
