package com.suppleshop.exercise.app.shared.exception;

public class CustomErrorResponse {

    private String message;
    private String timestamp;
    private String httpStatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private String message;
        private String timestamp;
        private String httpStatus;

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder timestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder httpStatus(String httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        public CustomErrorResponse build() {
            CustomErrorResponse response = new CustomErrorResponse();
            response.message = this.message;
            response.timestamp = this.timestamp;
            response.httpStatus = this.httpStatus;
            return response;
        }
    }
}
