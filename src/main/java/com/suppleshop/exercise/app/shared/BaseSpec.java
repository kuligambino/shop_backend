package com.suppleshop.exercise.app.shared;

import com.suppleshop.exercise.app.shared.exception.UnsatisfiedSpecificationException;

/**
 * Contains all methods used to validate
 * All Validators has to implement this interface
 *
 * @param <T> object class to validate
 */
public interface BaseSpec<T> {

    boolean isFulfilledBy(T t) throws UnsatisfiedSpecificationException;
}
