package com.suppleshop.exercise.app.shared.exception;

public class BusinessException extends RuntimeException{

    public BusinessException() {
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
