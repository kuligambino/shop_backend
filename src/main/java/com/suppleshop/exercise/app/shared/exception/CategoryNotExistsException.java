package com.suppleshop.exercise.app.shared.exception;

public class CategoryNotExistsException extends RuntimeException {

    public CategoryNotExistsException() {
        super("Kategoria nie istnieję!");
    }
}
