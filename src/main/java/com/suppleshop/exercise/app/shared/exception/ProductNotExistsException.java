package com.suppleshop.exercise.app.shared.exception;

public class ProductNotExistsException extends RuntimeException {

    public ProductNotExistsException() {
        super("Product doesn't exists!");
    }
}
