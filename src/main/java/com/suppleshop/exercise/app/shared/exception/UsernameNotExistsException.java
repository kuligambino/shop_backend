package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.USER_NOT_EXISTS;

public class UsernameNotExistsException extends RuntimeException {

    public UsernameNotExistsException() {
        super(USER_NOT_EXISTS);
    }
}
