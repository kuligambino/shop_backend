package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.USER_ALREADY_EXISTS;

public class UserAlreadyExistsException extends RuntimeException {

    public UserAlreadyExistsException() {
        super(USER_ALREADY_EXISTS);
    }
}
