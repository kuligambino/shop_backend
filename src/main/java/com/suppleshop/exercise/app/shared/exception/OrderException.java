package com.suppleshop.exercise.app.shared.exception;

public class OrderException extends RuntimeException {
    public OrderException() {
        super("Problem z koszykiem!");
    }
}
