package com.suppleshop.exercise.app.shared.exception;

public class ContactException extends RuntimeException{
    public ContactException() {
        super("There are problems with form!");
    }
}
