package com.suppleshop.exercise.app.shared.exception;

public class AuthorizationException extends RuntimeException {

    public AuthorizationException() {
        super(ExceptionMessages.AUTHORIZATION);
    }
}
