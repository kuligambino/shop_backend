package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.BAD_CREDENTIALS;

public class AuthenticationUserException extends RuntimeException {
    public AuthenticationUserException() {
        super(BAD_CREDENTIALS);
    }
}
