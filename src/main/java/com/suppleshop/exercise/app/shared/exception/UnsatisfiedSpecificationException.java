package com.suppleshop.exercise.app.shared.exception;

public class UnsatisfiedSpecificationException extends RuntimeException {

    public UnsatisfiedSpecificationException(String message) {
        super(message);
    }
}
