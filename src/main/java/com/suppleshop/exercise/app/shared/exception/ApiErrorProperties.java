package com.suppleshop.exercise.app.shared.exception;

public class ApiErrorProperties {

    private ApiErrorProperties() {
    }

    public static final String RECAPTCHA_FIELD = "Recaptcha";
    public static final String RECAPTCHA_CODE = "RecaptchaNotAccepted";
    public static final String EMAIL_FIELD = "Email";
    public static final String EMAIL_NOT_EXIST_CODE = "UserNotExists";
    public static final String EMAIL_ALREADY_EXIST_CODE = "UserAlreadyExists";
    public static final String CONF_PASSWORD_FIELD = "ConfirmedPassword";
    public static final String CONF_PASSWORD_CODE = "NotEquals";
    public static final String TOKEN_FIELD = "Token";
    public static final String TOKEN_CODE = "TokenNotExists";
    public static final String APPROVAL_FIELD = "Approvals";
    public static final String APPROVAL_CODE = "ApprovalsNotAccepted";
}
