package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.TOKEN_NOT_EXISTS;

public class TokenNotExistException extends RuntimeException {

    public TokenNotExistException() {
        super(TOKEN_NOT_EXISTS);
    }
}
