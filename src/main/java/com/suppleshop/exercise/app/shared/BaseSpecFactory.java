package com.suppleshop.exercise.app.shared;

/**
 * Contains all common methods in Specification Factory
 * All Specification Factories have to implement it
 *
 * @param <T> the type of Spec
 */
public interface BaseSpecFactory<T> {

    /**
     * Defines all specifications for given object
     *
     * @return all specifications for given object
     */
    T defineSpecification();
}
