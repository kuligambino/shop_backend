package com.suppleshop.exercise.app.shared.exception;

import static com.suppleshop.exercise.app.shared.exception.ExceptionMessages.PASSWORDS_NOT_EQUAL;

public class ConfirmedPasswordNotEqualsException extends RuntimeException {

    public ConfirmedPasswordNotEqualsException() {
        super(PASSWORDS_NOT_EQUAL);
    }
}
