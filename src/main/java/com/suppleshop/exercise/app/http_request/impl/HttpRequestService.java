package com.suppleshop.exercise.app.http_request.impl;

import com.suppleshop.exercise.app.order.domain.Order;
import com.suppleshop.exercise.app.order.domain.OrderRepository;
import com.suppleshop.exercise.app.register.domain.User;
import com.suppleshop.exercise.app.register.domain.UserRepository;
import com.suppleshop.exercise.app.security.domain.EncryptionFacade;
import com.suppleshop.exercise.app.shared.exception.OrderException;
import com.suppleshop.exercise.app.shared.exception.UsernameNotExistsException;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;

import static com.auth0.jwt.JWT.require;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class HttpRequestService {

    private static final String EMPTY_STRING = "";
    private static final String ORDER_HEADER = "order";
    private static final Long NOT_EXISTED_ORDER_ID = 0L;
    private static final String SECRET_KEY = "SecretKeyToGenJWTs";
    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String JWT_HEADER = "Authorization";
    private static final String BLANK = "";

    private final EncryptionFacade encryptionFacade;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;

    public HttpRequestService(EncryptionFacade encryptionFacade, UserRepository userRepository, OrderRepository orderRepository) {
        this.encryptionFacade = encryptionFacade;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
    }

    public User getUser(final HttpServletRequest request) {
        final String email = decodeEmailFromRequest(request);
        return userRepository.findByEmail(email).orElseThrow(UsernameNotExistsException::new);
    }

    public Order getOrder(final HttpServletRequest request) {
        final Long orderId = getOrderId(request);
        return orderRepository.findById(orderId).orElseThrow(OrderException::new);
    }

    private String decodeEmailFromRequest(final HttpServletRequest request) {
        final String jwToken = readHeader(request, JWT_HEADER);
        return isNotBlank(jwToken) ?
                require(HMAC512(SECRET_KEY.getBytes()))
                        .build()
                        .verify(jwToken.replace(TOKEN_PREFIX, BLANK))
                        .getSubject() : EMPTY_STRING;
    }

    private Long getOrderId(final HttpServletRequest request) {
        final String encodedOrderId = readHeader(request, ORDER_HEADER);
        return isNotBlank(encodedOrderId) ? encryptionFacade.decodeId(encodedOrderId) : NOT_EXISTED_ORDER_ID;
    }

    private String readHeader(final HttpServletRequest request, final String headerName) {
        return request != null && isNotBlank(headerName) ? request.getHeader(headerName) : EMPTY_STRING;
    }
}
