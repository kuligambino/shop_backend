-- INSERTING APPROVALS --
INSERT INTO approval(id,description,title,type,is_required,create_time, version) VALUES (1,'Oświadczam, iż zapoznałem się z treścią Regulaminu.','Regulamin','REGULATIONS',1, '2016-01-05',0);
INSERT INTO approval(id,description,title,type,is_required,create_time, version) VALUES (2,'Dane osobowe będą przetwarzane na podstawie zgody wyłącznie przez czas trwania udzielonej zgody.','Przetwarzanie danych osobowych','PERSONAL_DATA_PROCESSING',1,'2016-01-05',0);
INSERT INTO approval(id,description,title,type,is_required,create_time, version) VALUES (3,'Wyrażam zgodę na przetwarzanie przez KKWJ moich danych osobowych w postaci imienia, nazwiska i adresu do korespondencji.','Wysyłanie materiałów reklamowych','ADVERTS',0,'2016-01-05',0);
INSERT INTO approval(id,description,title,type,is_required,create_time, version) VALUES (4,'Wyrażam zgodę na przesyłanie informacji handlowych za pomocą środków komunikacji elektronicznej w rozumieniu.','Newsletter','NEWSLETTER',0,'2016-01-05',0);

-- INSERTING ROLES --
INSERT INTO user_role(id,role_name,description,create_time, version) VALUES (1,'ADMIN','Admin ma nieograniczone prawa.','2016-01-05',0);
INSERT INTO user_role(id,role_name,description,create_time, version) VALUES (2,'USER','Użytkownik ma ograniczone prawa.','2016-01-05',0);

-- INSERTING TOKENS --
INSERT INTO token(id,description,type,create_time, version) VALUES (1,"Token przypisywany do potwierdzenia rejestracji nowego użytkownika","VERIFICATION",'2016-01-05',0);
INSERT INTO token(id,description,type,create_time, version) VALUES (2,"Token przypisywany podczas zmiany hasła","PASSWORD_RESET",'2016-01-05',0);

-- INSERTING CATEGORIES --
INSERT INTO category(id,name, description,create_time, version) VALUES (1,'BIAŁKO',
'Z obserwacji naszych klientów wynika, że odżywki białkowe
odgrywają kluczową rolę w diecie większości osób uprawiających
sporty siłowe, siłowo-wytrzymałościowe oraz osób dążących do
redukcji wagi ciała. W opinii naszych klientów, wraz z intensywnym
wysiłkiem fizycznym dochodzi do zwiększonego zużycia białek ustrojowych,
dlatego kluczową kwestią pozostaje uzupełnienie protein w codziennej diecie,
ponieważ to aminokwasy w nich zawarte, stanowią budulec mięśni.','2016-01-05',0);

INSERT INTO category(id,name, description,create_time, version) VALUES (2,'KREATYNA',
'Kreatyna jest prawdopodobnie najlepiej przebadanym suplementem diety
dla sportowców o potwierdzonym działaniu. Jej szerokie spektrum aktywności
pozwala cieszyć się z jej dobrodziejstw zarówno sportowcom dyscyplin sylwetkowo-siłowych
jak i wytrzymałościowych. Jej niegasnąca popularność wynika głównie z tego, że suplementacja
tym preparatem pozwala poprawić wyniki sportowe w stosunkowo szybkim czasie. Na rynku
istnieje wiele form kreatyny, od najpopularniejszego i najlepiej przebadanego monohydratu,
poprzez jabłczan, chlorowodorek na chelacie magnezowym skończywszy.','2016-01-05',0);

INSERT INTO category(id,name, description,create_time, version) VALUES (3,'SPALACZ TŁUSZCZU','
Istotne jest to, by zredukować zbyt dużą ilość tkanki tłuszczowej jaką posiadamy.
Przeróżne badania wskazują na to, że zbyt duża jej ilość w organizmie może być dla
nas bardzo szkodliwa i doprowadzić do przeróżnych chorób, jak nowotwory, cukrzyca typu 2,
choroby serca,','2016-01-05',0);

INSERT INTO category(id,name, description,create_time, version) VALUES (4,'GAINER','
Gainery, powszechnie znane jako odżywki na masę, to wysokokaloryczne preparaty,
które mają uzupełnić naszą dietę w potrzebne kalorie do wzrostu masy mięśniowej.
W skład produktów zazwyczaj wchodzą: węglowodany, które stanowią zazwyczaj 60-70%
ich zawartości oraz białko w ilości 20-30%. Gainery mogą być dodatkowo wzbogacone o
tłuszcze (np. olej MCT), aminokwasy, witaminy, a nawet wyciągi ziołowe.','2016-01-05',0);

INSERT INTO category(id,name, description,create_time, version) VALUES (5,'AMINOKWASY BCAA','
Aminokwasy BCAA od ponad dwóch dekad są stałym elementem suplementacji
sportowców na całym świecie. Pomimo licznych kontrowersji i debat na temat
 zasadności suplementacją aminokwasami rozgałęzionymi, wciąż produkty
 zawierające BCAA cieszą się dużą popularnością wśród sportowców.','2016-01-05',0);

 -- INSERTING CREATINES --
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (1,'JaskoKREATOR',300,30,'Cytryna',2,'assets/kreatyna1.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (2,'JaskoKREATOR',500,50,'Ananas',2,'assets/kreatyna2.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (3,'JaskoKREATOR',700,70,'Pomarańcza',2,'assets/kreatyna3.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (4,'JaskoKREATOR',1000,100,'Pomarańcza',2,'assets/kreatyna4.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (5,'JaskoKREATOR',700,70,'Cytryna',2,'assets/kreatyna5.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (6,'JaskoKREATOR',300,30,'Malina',2,'assets/kreatyna6.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (7,'JaskoKREATOR',500,50,'Kokos',2,'assets/kreatyna7.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (8,'JaskoKREATOR',700,70,'Mars',2,'assets/kreatyna8.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (9,'JaskoKREATOR',1000,100,'Karmel',2,'assets/kreatyna9.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (10,'JaskoKREATOR',700,70,'Banan',2,'assets/kreatyna10.jpg','2016-01-05',0);

 -- INSERTING PROTEINS --
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (11,'Kuligoprotein3000',300,30,'Czekolada',1,'assets/bialko1.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (12,'Kuligoprotein3000',1000,100,'Czekolada',1,'assets/bialko2.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (13,'Kuligoprotein3000',700,70,'Wanilia',1,'assets/bialko3.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (14,'Kuligoprotein3000',1000,100,'Truskawka',1,'assets/bialko4.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (15,'Kuligoprotein3000',1000,100,'Orzech',1,'assets/bialko5.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (16,'Kuligoprotein3000',500,50,'Ananas',1,'assets/bialko6.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (17,'Kuligoprotein3000',1000,100,'Snickers',1,'assets/bialko7.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (18,'Kuligoprotein3000',700,70,'Banan',1,'assets/bialko8.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (19,'Kuligoprotein3000',1000,100,'Guma balonowa',1,'assets/bialko9.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (20,'Kuligoprotein3000',1000,100,'Lody waniliowe',1,'assets/bialko10.jpg','2016-01-05',0);

 -- INSERTING BCCAS --
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (21,'BccaBest',100,10,'Cytryna',5,'assets/bcca1.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (22,'BccaBest',200,20,'Cytryna',5,'assets/bcca2.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (23,'BccaBest',300,30,'Granat',5,'assets/bcca3.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (24,'BccaBest',500,50,'Orzech',5,'assets/bcca4.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (25,'BccaBest',1000,100,'Cytryna',5,'assets/bcca5.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (26,'BccaBest',100,10,'Malina',5,'assets/bcca6.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (27,'BccaBest',200,20,'Kokos',5,'assets/bcca7.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (28,'BccaBest',300,30,'Snickers',5,'assets/bcca8.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (29,'BccaBest',500,50,'Karmel',5,'assets/bcca9.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (30,'BccaBest',1000,100,'Masło orzechowe',5,'assets/bcca10.jpg','2016-01-05',0);

 -- INSERTING FATBURNERS --
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (31,'YouAreFAT!',700,70,'Wanilia',3,'assets/fat1.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (32,'YouAreFAT!',1000,100,'Wanilia',3,'assets/fat2.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (33,'YouAreFAT!',300,30,'Truskawka',3,'assets/fat3.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (34,'YouAreFAT!',500,50,'Czekolada',3,'assets/fat4.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (35,'YouAreFAT!',1000,100,'Cytryna',3,'assets/fat5.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (36,'YouAreFAT!',700,70,'Banan',3,'assets/fat6.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (37,'YouAreFAT!',1000,100,'Karmel',3,'assets/fat7.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (38,'YouAreFAT!',300,30,'Mars',3,'assets/fat8.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (39,'YouAreFAT!',500,50,'Kokos',3,'assets/fat9.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (40,'YouAreFAT!',1000,100,'Malina',3,'assets/fat10.jpg','2016-01-05',0);

 -- INSERTING GAINERS --
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (41,'GainPower',300,30,'Orzech',4,'assets/gainer1.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (42,'GainPower',500,50,'Orzech',4,'assets/gainer2.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (43,'GainPower',700,70,'Ananas',4,'assets/gainer3.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (44,'GainPower',500,50,'Snickers',4,'assets/gainer4.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (45,'GainPower',1000,100,'Ciasteczka',4,'assets/gainer5.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (46,'GainPower',300,30,'Wanilia',4,'assets/gainer6.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (47,'GainPower',500,50,'Truskawka',4,'assets/gainer7.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (48,'GainPower',700,70,'Kokos',4,'assets/gainer8.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (49,'GainPower',500,50,'Karmel',4,'assets/gainer9.jpg','2016-01-05',0);
INSERT INTO product(id,name,weight,price,taste,category_id,img,create_time, version) VALUES (50,'GainPower',1000,100,'Masło orzechowe',4,'assets/gainer10.jpg','2016-01-05',0);

 -- INSERTING USERS --
INSERT INTO user(id,birth_date, email, first_name,is_enabled, last_name,password,phone_number,anonymous,create_time, version) VALUES
(1,'2020-10-10','krystian.kulig94@gmail.com','Krystian',1,'Kulig','$2a$10$qBYv4NnU6eSknpycxLNhmehO8.U/YGOZZ0qzXCUd/8vaNFrczC1tC','507586135',false,'2016-01-05',0);